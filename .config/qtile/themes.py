from typing import Dict


class _Theme:
    _colors: Dict[str, str] = {}

    def theme(self):
        return self._colors

    def fg(self):
        return self._colors["foreground"]

    def bg(self):
        return self._colors["background"]

    def color0(self):
        return self._colors["color0"]

    def color1(self):
        return self._colors["color1"]

    def color2(self):
        return self._colors["color2"]

    def color3(self):
        return self._colors["color3"]

    def color4(self):
        return self._colors["color4"]

    def color5(self):
        return self._colors["color5"]

    def color6(self):
        return self._colors["color6"]

    def color7(self):
        return self._colors["color7"]

    def color8(self):
        return self._colors["color8"]

    def color9(self):
        return self._colors["color9"]

    def color10(self):
        return self._colors["color10"]

    def color11(self):
        return self._colors["color11"]

    def color12(self):
        return self._colors["color12"]

    def color13(self):
        return self._colors["color13"]

    def color14(self):
        return self._colors["color14"]

    def color15(self):
        return self._colors["color15"]

    def black(self):
        return self.color0()

    def red(self):
        return self.color1()

    def green(self):
        return self.color2()

    def yellow(self):
        return self.color3()

    def blue(self):
        return self.color4()

    def magenta(self):
        return self.color5()

    def cyan(self):
        return self.color6()

    def white(self):
        return self.color7()

    def black_bold(self):
        return self.color8()

    def red_bold(self):
        return self.color9()

    def green_bold(self):
        return self.color10()

    def yellow_bold(self):
        return self.color11()

    def blue_bold(self):
        return self.color12()

    def magenta_bold(self):
        return self.color13()

    def cyan_bold(self):
        return self.color14()

    def white_bold(self):
        return self.color15()


class CatppuccinMocha(_Theme):
    def __init__(self):
        self._colors = {
            "background": "#1E1E2E",
            "foreground": "#CDD6F4",
            "color0":  "#45475A",
            "color8":  "#585B70",
            "color1":  "#F38BA8",
            "color9":  "#F38BA8",
            "color2":  "#A6E3A1",
            "color10": "#A6E3A1",
            "color3":  "#F9E2AF",
            "color11": "#F9E2AF",
            "color4":  "#89B4FA",
            "color12": "#89B4FA",
            "color5":  "#F5C2E7",
            "color13": "#F5C2E7",
            "color6":  "#94E2D5",
            "color14": "#94E2D5",
            "color7":  "#BAC2DE",
            "color15": "#A6ADC8",
        }

    def muave(self):
        return "CBA6F7"

    def muave_bold(self):
        return "CBA6F7"

class Dracula(_Theme):
    def __init__(self):
        self._colors = {
            "foreground": "#F8F8F2",
            "background": "#282A36",
            "color0":     "#000000",
            "color1":     "#FF5555",
            "color2":     "#50FA7B",
            "color3":     "#F1FA8C",
            "color4":     "#BD93F9",
            "color5":     "#FF79C6",
            "color6":     "#8BE9FD",
            "color7":     "#BFBFBF",
            "color8":     "#4D4D4D",
            "color9":     "#FF6E67",
            "color10":    "#5AF78E",
            "color11":    "#F4F99D",
            "color12":    "#CAA9FA",
            "color13":    "#FF92D0",
            "color14":    "#9AEDFE",
            "color15":    "#E6E6E6",
        }
