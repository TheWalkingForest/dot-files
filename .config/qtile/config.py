# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import math
import os
import shutil
import subprocess

from libqtile import bar, hook, layout, qtile, widget
from libqtile.config import Click, Drag, DropDown, Group, Key, Match, ScratchPad, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

import themes

IS_WAYLAND: bool = qtile.core.name == "wayland"


def exists(name: str):
    return shutil.which(name) is not None


def disk_usage(path):
    total, used, _ = shutil.disk_usage(path)
    total = total // math.pow(2, 30)
    used = used // math.pow(2, 30)
    return f"{used}GiB/{total}GiB"


mod = "mod1"
terminal = "wezterm" if exists("wezterm") else guess_terminal()

theme = themes.CatppuccinMocha()

keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    # Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    # Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    # Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    # Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    # Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    # Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    # Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    # Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    # Key(
    #     [mod, "shift"],
    #     "Return",
    #     lazy.layout.toggle_split(),
    #     desc="Toggle between split and unsplit sides of stack",
    # ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key(
        [mod],
        "f",
        lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen on the focused window",
    ),
    Key([mod], "t", lazy.window.toggle_floating(), desc="Toggle floating on the focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "shift"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    # Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    Key([mod], "space", lazy.spawn("rofi -show drun"), desc="Launch rofi w/ drun"),
    Key([], "F10", lazy.group["scratchpad"].dropdown_toggle("term")),
    # Key([], "F12", lazy.group["scratchpad"].dropdown_toggle("qtile shell")),
]

groups = [Group(name=n, label=lbl) for n, lbl in zip("123456789", "")]

groups.extend(
    [
        ScratchPad(
            "scratchpad",
            [
                # define a drop down terminal.
                # it is placed in the upper third of screen by default.
                DropDown("term", terminal, opacity=0.8),
                # define another terminal exclusively for ``qtile shell` at different position
                # DropDown(
                #     "qtile shell",
                #     f"{terminal} 'qtile shell'",
                #     x=0.05,
                #     y=0.4,
                #     width=0.9,
                #     height=0.6,
                #     opacity=0.9,
                #     on_focus_lost_hide=True,
                # ),
            ],
        ),
    ],
)

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name),
                desc="move focused window to group {}".format(i.name),
            ),
            Key(
                [mod, "control"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
        ],
    )

layouts = [
    layout.Tile(margin=5, margin_on_single=True, ratio=0.55),
    layout.Max(),
    layout.TreeTab(),
]

widget_defaults = {
    "font": "FiraCode Nerd Font",
    "fontsize": 14,
    "padding": 3,
}
extension_defaults = widget_defaults.copy()

sep = {
    "linewidth": 2,
    "size_percent": 70,
    "padding": 7,
    "foreground": theme.bg(),
}

clock = {
    "fmt": " {} ",
    "background": theme.black(),
    "foreground": theme.cyan(),
    "format": "%a %F %R",
}

window_name = {
    "background": theme.black(),
    # "foreground": theme.muave(),
    "foreground": theme.magenta(),
}

check_updates = {
    "distro": "Void",
    "fmt": " {} ",
    "background": theme.black(),
    "foreground": theme.magenta(),
    "display_format": "{updates}",
    "no_update_string": "0",
}

cpu = {
    "fmt": " {} ",
    "background": theme.black(),
    "foreground": theme.blue(),
    "format": "{load_percent}%",
}

memory = {
    "fmt": " {} ",
    "background": theme.black(),
    "foreground": theme.green(),
    "format": "{MemPercent}%",
}

status_bar = {
    "widgets": [
        widget.GroupBox(highlight_method="line"),
        widget.CurrentLayoutIcon(),
        widget.WindowName(**window_name),
        widget.Prompt(),
        widget.Sep(**sep),
        # 
        widget.TextBox(
            fmt="  ",
            background=theme.cyan(),
            foreground=theme.black(),
        ),
        widget.Mpd2(
            fmt=" {} ",
            background=theme.black(),
            foreground=theme.cyan(),
            status_format="{play_status} {title} - [{artist}]",
            idle_format="{play_status} {title} - [{artist}]",
        ),
        widget.Sep(**sep),
        widget.TextBox(
            fmt="  ",
            background=theme.yellow(),
            foreground=theme.black(),
        ),
        widget.TextBox(
            fmt=f" {disk_usage('/')} ",
            background=theme.black(),
            foreground=theme.yellow(),
        ),
        widget.Sep(**sep),
        widget.TextBox(
            fmt="  ",
            foreground=theme.black(),
            background=theme.blue(),
        ),
        widget.CPU(**cpu),
        widget.Sep(**sep),
        widget.TextBox(
            fmt="  ",
            foreground=theme.black(),
            background=theme.green(),
        ),
        widget.Memory(**memory),
        widget.Sep(**sep),
        widget.TextBox(
            fmt="  ",
            foreground=theme.black(),
            background=theme.magenta(),
        ),
        widget.CheckUpdates(**check_updates),
        widget.Sep(**sep),
        widget.TextBox(
            fmt="  ",
            foreground=theme.black(),
            background=theme.cyan(),
        ),
        widget.Clock(**clock),
        widget.Sep(**sep),
        widget.StatusNotifier() if IS_WAYLAND else widget.Systray(),
    ],
    "size": 28,
    "margin": [10, 10, 5, 10],
    "background": theme.bg(),
}

screens = [
    Screen(
        top=bar.Bar(**status_bar),
        x=0,
        y=0,
        width=3840,
        height=2160,
        x11_drag_polling_rate=60 if not IS_WAYLAND else None,
    ),
    Screen(
        top=bar.Bar(**status_bar),
        x=3840,
        y=0,
        width=2160,
        height=1440,
        x11_drag_polling_rate=60 if not IS_WAYLAND else None,
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
floats_kept_above = True
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ],
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"


@hook.subscribe.startup_once
def autostart():
    home = os.getenv("HOME", os.path.expanduser("~"))
    config_dir = os.getenv("XDG_CONFIG_HOME", home + "/.config")
    if IS_WAYLAND:
        subprocess.Popen([config_dir + "/qtile/autostart-wayland.sh"])
    else:
        subprocess.Popen([config_dir + "/qtile/autostart.sh"])
