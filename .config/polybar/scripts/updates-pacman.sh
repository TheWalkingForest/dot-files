#!/bin/sh

if ! updates=$(checkupdates 2> /dev/null | wc -l ); then
    updates=0
fi

if [ "$updates" -ge 0 ]; then
    echo "$updates"
else
    echo "ERROR: updates-pacman.sh not working"
fi
