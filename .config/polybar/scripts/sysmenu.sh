#!/usr/bin/env sh

myKernel=$(uname -r)
term=kitty
fm=nemo
mail=thunderbird
web=firefox

/usr/bin/env cat << EOF | xmenu -p 0x40 | sh &
Void - $myKernel

IMG:/home/fwalk/.cache/xdg-xmenu/icons/kitty.png	Terminal	$term
IMG:/home/fwalk/.cache/xdg-xmenu/icons/system-file-manager.png	File Manager	$fm
IMG:/home/fwalk/.cache/xdg-xmenu/icons/thunderbird.png	Mail Reader	$mail
IMG:/home/fwalk/.cache/xdg-xmenu/icons/firefox.png	Web Browser	$web

IMG:/home/fwalk/.cache/xdg-xmenu/icons/applications-accessories.png	Accessories
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/org.gnome.clocks.png	Clocks (Clocks)	gnome-clocks
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/compton.png	compton (X compositor)	compton
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/system-file-manager.png	Files - Nemo	nemo
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/org.gnome.FontManager.png	Font Manager (Font Manager)	font-manager
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/org.gnome.FontViewer.png	Font Viewer (Font Viewer)	/usr/libexec/font-manager/font-viewer
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/galculator.png	Galculator	galculator
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/keepassxc.png	KeePassXC (Password Manager)	keepassxc
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/kvantum.png	Kvantum Manager	kvantummanager
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/nvim.png	Neovim (Text Editor)	xterm -e nvim
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/nitrogen.png	nitrogen (Wallpaper Setter)	nitrogen
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/application-x-executable.png	picom (X compositor)	picom
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/com.github.alainm23.planner.png	Planner (Planner)	com.github.alainm23.planner
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/solaar.png	Solaar	solaar
IMG:/home/fwalk/.cache/xdg-xmenu/icons/applications-development.png	Development
IMG:/home/fwalk/.cache/xdg-xmenu/icons/applications-education.png	Education
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/libreoffice-math.png	LibreOffice Math (Formula Editor)	libreoffice --math
IMG:/home/fwalk/.cache/xdg-xmenu/icons/applications-games.png	Games
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/application-x-executable.png	Minecraft	/home/fwalk/.bin/minecraft-launcher
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/steam.png	Steam	/usr/bin/steam
IMG:/home/fwalk/.cache/xdg-xmenu/icons/applications-graphics.png	Graphics
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/org.gnome.Evince.png	Document Viewer	evince
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/org.flameshot.Flameshot.png	Flameshot (Screenshot tool)	/usr/bin/flameshot
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/gimp.png	GNU Image Manipulation Program (Image Editor)	gimp-2.10
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/org.inkscape.Inkscape.png	Inkscape (Vector Graphics Editor)	inkscape
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/libreoffice-draw.png	LibreOffice Draw (Drawing Program)	libreoffice --draw
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/org.gnome.Shotwell.png	Shotwell (Photo Manager)	shotwell
IMG:/home/fwalk/.cache/xdg-xmenu/icons/applications-internet.png	Internet
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/chromium.png	Chromium (Web Browser)	chromium
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/discord.png	Discord (Internet Messenger)	/usr/lib/discord/Discord
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/element.png	Element	element-desktop
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/firefox.png	Firefox Web Browser (Web Browser)	firefox
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/application-x-executable.png	FreeTube	/home/fwalk/Applications/FreeTube.AppImage
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/kdeconnect.png	KDE Connect (Device Synchronization)	kdeconnect-app
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/kdeconnect.png	KDE Connect Indicator	kdeconnect-indicator
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/kdeconnect.png	KDE Connect SMS (SMS)	kdeconnect-sms
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/com.gitlab.newsflash.png	NewsFlash (RSS Reader)	/usr/bin/flatpak run --branch=stable --arch=x86_64 --command=com.gitlab.newsflash com.gitlab.newsflash
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/kdeconnect.png	Open on connected device via KDE Connect (Open on connected device via KDE Connect)	kdeconnect-handler --open
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/org.remmina.Remmina.png	Remmina (Remote Desktop Client)	remmina-file-wrapper
	IMG:/home/fwalk/.local/share/icons/session_icon.png	Session	/home/fwalk/Applications/session-desktop.AppImage
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/syncthing.png	Start Syncthing (File synchronization)	/usr/bin/syncthing serve --no-browser --logfile=default
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/steam.png	Steam	/usr/bin/steam
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/syncthing.png	Syncthing Web UI (File synchronization UI)	/usr/bin/syncthing -browser-only
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/re.sonny.Tangram.png	Tangram	/usr/bin/flatpak run --branch=stable --arch=x86_64 --command=re.sonny.Tangram re.sonny.Tangram
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/thunderbird.png	Thunderbird (Mail Client)	thunderbird
	IMG:/home/fwalk/.local/share/Steam/steamapps/common/War Thunder/launcher.ico	War Thunder	/home/fwalk/.local/share/Steam/steamapps/common/War Thunder/launcher
IMG:/home/fwalk/.cache/xdg-xmenu/icons/applications-multimedia.png	Multimedia
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/multimedia-volume-control.png	PulseAudio Volume Control (Volume Control)	pavucontrol
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/com.spotify.Client.png	Spotify (Online music streaming service)	/usr/bin/flatpak run --branch=stable --arch=x86_64 --command=spotify --file-forwarding com.spotify.Client @@u @@
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/vlc.png	VLC media player (Media player)	/usr/bin/vlc --started-from-file
IMG:/home/fwalk/.cache/xdg-xmenu/icons/applications-office.png	Office
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/org.gnome.Evince.png	Document Viewer	evince
	IMG:/home/fwalk/.local/share/icons/session_icon.png	Joplin	/home/fwalk/Applications/Joplin.AppImage
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/libreoffice-startcenter.png	LibreOffice (Office)	libreoffice
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/libreoffice-base.png	LibreOffice Base (Database Development)	libreoffice --base
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/libreoffice-calc.png	LibreOffice Calc (Spreadsheet)	libreoffice --calc
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/libreoffice-draw.png	LibreOffice Draw (Drawing Program)	libreoffice --draw
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/libreoffice-impress.png	LibreOffice Impress (Presentation)	libreoffice --impress
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/libreoffice-math.png	LibreOffice Math (Formula Editor)	libreoffice --math
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/libreoffice-writer.png	LibreOffice Writer (Word Processor)	libreoffice --writer
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/com.github.alainm23.planner.png	Planner (Planner)	com.github.alainm23.planner
IMG:/home/fwalk/.cache/xdg-xmenu/icons/applications-science.png	Science
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/libreoffice-math.png	LibreOffice Math (Formula Editor)	libreoffice --math
IMG:/home/fwalk/.cache/xdg-xmenu/icons/preferences-desktop.png	Settings
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/preferences-system-network.png	Advanced Network Configuration	nm-connection-editor
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/preferences-desktop-theme.png	Customize Look and Feel (Customize Look and Feel)	lxappearance
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/kdeconnect.png	KDE Connect Settings (Connect and sync your devices)	kdeconnect-settings
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/kvantum.png	Kvantum Manager	kvantummanager
	IMG:/usr/share/pixmaps/nvidia-settings.png	NVIDIA X Server Settings	/usr/bin/nvidia-settings
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/multimedia-volume-control.png	PulseAudio Volume Control (Volume Control)	pavucontrol
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/preferences-desktop-theme.png	Qt5 Settings	qt5ct
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/input-tablet.png	Wacom Tablet finder (Helper tool to detect Wacom Tablets)	kde_wacom_tabletfinder
IMG:/home/fwalk/.cache/xdg-xmenu/icons/applications-system.png	System
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/Alacritty.png	Alacritty (Terminal)	alacritty
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/conky-logomark-violet.png	conky	conky --daemonize --pause=1
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/fish.png	fish	xterm -e fish
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/gparted.png	GParted (Partition Editor)	/usr/bin/gparted-pkexec
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/grub-customizer.png	Grub Customizer	grub-customizer
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/htop.png	Htop (Process Viewer)	xterm -e htop
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/kitty.png	kitty (Terminal emulator)	kitty
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/virtualbox.png	Oracle VM VirtualBox (Virtual Machine)	VirtualBox
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/org.gnome.Terminal.png	Terminal	gnome-terminal
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/xterm-color_48x48.png	UXTerm	uxterm
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/virt-manager.png	Virtual Machine Manager	virt-manager
	IMG:/home/fwalk/.cache/xdg-xmenu/icons/xterm-color_48x48.png	XTerm	xterm
IMG:/home/fwalk/.cache/xdg-xmenu/icons/applications-other.png	Others

Reload		herbstclient reload
Shutdown	poweroff
Reboot		reboot
Logout		logout.sh
EOF
