#!/usr/bin/env bash

rofi -show p -modi p:"${HOME}/.local/bin/rofi-power-menu" \
    -theme "${HOME}/.config/polybar/menu/powermenu.rasi" \
    -font "FiraCode Nerd Font 12" \
    -width 20 \
    -lines 6 \
