#!/bin/sh

updates=$(apt list --upgradable 2> /dev/null | grep -c upgradable);

if [ "$updates" -ge 0 ]; then
    echo "$updates"
else
    echo "ERROR: updates-apt.sh not working"
fi
