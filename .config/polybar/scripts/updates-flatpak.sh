#!/bin/sh

updates=$(echo 'n' | flatpak update 2> /dev/null | tail -n +5 | head -2 | wc -l)

if [ "$updates" -ge 0 ]; then
    echo "$updates"
else
    echo "ERROR: updates-flatpak.sh not working"
fi
