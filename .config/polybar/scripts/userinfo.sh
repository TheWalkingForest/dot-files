#!/usr/bin/env bash

HOSTNAME=$(echo $HOSTNAME 2> /dev/null)
USER=$(echo $USER 2> /dev/null)

echo "$USER"@"$HOSTNAME"
