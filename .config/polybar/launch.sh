#!/usr/bin/env bash

# Add this script to your wm startup file.

XDG_CONFIG_HOME=${XDG_CONFIG_HOME:-"$HOME"/.config}

DIR="$XDG_CONFIG_HOME/polybar"
POLYBAR_LOGFILE="$HOME/.local/log/polybar.log"

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Create local logfile
rm "$POLYBAR_LOGFILE" 2> /dev/null

# Launch the bar
echo "=====Base=====" >> "$POLYBAR_LOGFILE"
polybar -q base -c "$DIR"/config.ini 2>> "$POLYBAR_LOGFILE" &
echo "=====test=====" >> "$POLYBAR_LOGFILE"
polybar -q test -c "$DIR"/config.ini 2>> "$POLYBAR_LOGFILE" &
