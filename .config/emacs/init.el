;; -*- lexical-binding: t; -*-

;;; Code:
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name
	"straight/repos/straight.el/bootstrap.el"
	(or (bound-and-true-p straight-base-dir)
	    user-emacs-directory)))
      (bootstrap-version 7))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(setq straight-use-package-by-default 1)
(setq package-enable-at-startup nil)

;; * Emacs
(use-package emacs
  :custom
  ;(tab-always-indent 'complete)
  ;(read-extended-command-predicate #'command-completion-default-include-p)
  (inhibit-startup-message t)
  (visible-bell nil)
  (scroll-step 1)
  (warning-minimum-level :error)
  (backup-directory-alist '(("." . "~/.local/state/emacs/saves")) )
  (backup-by-copying t)

  (ide-enable-flex-matching t)
  (ide-everywhere t)

  (recentf-max-menu-items 25)
  (recentf-max-saved-items 25)
  (history-length 30)
  (org-return-follows-link  t)
  (delete-selection-mode 1)
  (electric-pair-mode 1)
  (x-stretch-cursor 1)
  (display-line-numbers-type 'relative)

  :config
  (add-to-list 'exec-path "~/.nix-profile/bin")
  (set-face-attribute
   'default nil
   :font "ComicShannsMono Nerd font"
   :height 120)

  (add-hook 'org-mode-hook (lambda ()
			     (setq-local electric-pair-inhibit-predicate
					 (lambda (c)
					   (if (char-equal c ?<)
					       t
					     (,electric-pair-inhibit-predicate c))))))
  (add-hook 'emacs-startup-hook (lambda ()
				  (message "Emacs loaded in %s with %d garbage collections."
					   (format "%.2f seconds"
						   (float-time
						    (time-subtract after-init-time before-init-time)))
					   gcs-done)))

  :bind
  ("\C-x\ \C-r" . recentf-open-files)
  ("C-=" . text-scale-increase)
  ("C--" . text-scale-decrease)
  ("<C-wheel-up>" . text-scale-increase)
  ("<C-wheel-down>" . text-scale-decrease)
  ("<escape>" . keyboard-escape-quit)
  ("\C-x\ K". kill-buffer-and-window)

  :init
  (fido-mode 1)
  (icomplete-vertical-mode t)
  (savehist-mode 1)
  (recentf-mode 1)
  (scroll-bar-mode -1)
  (tool-bar-mode -1)
  (menu-bar-mode -1)
  (global-hl-line-mode 1)
  (global-display-line-numbers-mode 1)
  (global-visual-line-mode 1)) ; emacs

;; * Ui
;; ** Page Break Lines
(use-package page-break-lines
  :config
  (page-break-lines-mode))

;; ** Dashboard
(use-package dashboard
  :ensure t
  :after nerd-icons
  :custom
  (initial-buffer-choice 'dashboard-open)
  (dashboard-set-heading-icons t)
  (dashboard-set-file-icons t)
  (dashboard-startup-banner "/usr/share/icons/hicolor/128x128/apps/emacs.png")
  (dashboard-center-content t)
  (dashboard-vertically-center-content t)
  (dashboard-items '((recents . 5)
                     (agenda . 5)
                     (bookmarks . 3)
                     (projects . 3)
                     (registers . 3)))
  (dashboard-display-icons-p t)
  (dashboard-icon-type 'nerd-icons)
  :config
  (dashboard-modify-heading-icons '((recents . "nf-oct-log")
  (bookmarks . "nf-oct-book")))
  (dashboard-setup-startup-hook))

;; (use-package projectile
;;   :config (projectile-mode 1))

;; ** Dirvish
(use-package dirvish
  :custom (dirvish-use-header-line 'global)
  :init (dirvish-override-dired-mode))

;;(use-package dired-open
;;  :custom
;;  (dired-open-experssions '(("gif" . "sxiv")
;;			    ("jpg" . "sxiv")
;;			    ("png" . "sxiv")
;;			    ("mkv" . "mpv")
;;			    ("mp4" . "mpv"))))
;;(use-package peep-dired
;;  :after dired
;;  ;:hook (peep-dired . evil-normalize-keymaps)
;;  :hook (dired-mode . peep-dired)
;;  :config
;;  (evil-define-key 'normal dired-mode-map (kbd "h") 'dired-up-directory)
;;  (evil-define-key 'normal dired-mode-map (kbd "l") 'dired-open-file)
;;  (evil-define-key 'normal peep-dired-mode-map (kbd "j") 'peep-dired-next-file)
;;  (evil-define-key 'normal peep-dired-mode-map (kbd "k") 'peep-dired-prev-file))

;; ** Which Key
(use-package which-key
  :init
  (which-key-mode 1)
  :custom
  (which-key-sort-order #'which-key-key-order-alpha)
  (which-key-sort-uppercase-first nil)
  (which-key-add-column-padding 1)
  (which-key-max-display-columns nil)
  (which-key-min-display-lines 6)
  (which-key-side-window-slot -10)
  (which-key-side-window-max-height 0.25)
  (which-key-idle-delay 0.5)
  (which-key-max-description-length 25)
  (which-key-allow-imprecise-window-fit t))

;; ** Nerd Icons
(use-package nerd-icons
  :demand t)
;;(use-package nerd-icons-corfu
;;  :after nerd-icons
;;  :after corfu
;;  :demand t
;;  :init (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))
;;(use-package nerd-icons-completion
;;  :after nerd-icons
;;  :demand t
;;  :config
;;  (nerd-icons-completion-mode))
(use-package nerd-icons-dired
  :after nerd-icons
  :demand t
  :hook (dired-mode . nerd-icons-dired-mode))
(use-package nerd-icons-ibuffer
  :after nerd-icons
  :demand t
  :hook (ibuffer-mode . nerd-icons-ibuffer-mode))

;; ** Rainbow Delimeters
(use-package rainbow-delimiters
  :hook ((prog-mode text-mode) . rainbow-delimiters-mode))
(use-package rainbow-mode
  :diminish
  :hook org-mode prog-mode)

;; * App Launcher
(use-package app-launcher
  :straight '(app-launcher :host github :repo "SebastienWae/app-launcher"))

(defun emacs-run-launcher ()
  (interactive)
  (with-selected-frame
      (make-frame '((name . "emacs-run-launcher")
		    (minibuffer . only)
		    (fullscreen . 0)
                    (undecorated . t)
                    ;;(auto-raise . t)
                    (internal-border-width . 10)
                    (width . 80)
                    (height . 11)))
    (unwind-protect
	(app-launcher-run-app)
      (delete-frame))))

(use-package hl-todo
  :hook ((org-mode . hl-todo-mode)
         (prog-mode . hl-todo-mode))
  :config
  (setq hl-todo-highlight-punctuation ":"
        hl-todo-keyword-faces
        `(("TODO"       warning bold)
          ("FIXME"      error bold)
          ("XXX"        error bold)
          ("HACK"       font-lock-constant-face bold)
          ("REVIEW"     font-lock-keyword-face bold)
          ("NOTE"       success bold)
          ("DEPRECATED" font-lock-doc-face bold))))

;; * Completion
;; ** Orderless
;;(use-package orderless
;;  :demand t
;;  ;;:bind (:map vertico-map
;;  ;;	  ("?" . minibuffer-completion-help)
;;  ;;	  ("M-RET" . minibuffer-force-complete-and-exit)
;;  ;;	  ("M-TAB" . minibuffer-complete))
;;  :custom
;;  (completion-styles '(orderless basic))
;;  (completion-category-defaults nil)
;;  (completion-category-overrides '(file (styles partial-completions))))
;;
;;;; ** Vertico
;;(use-package vertico
;;  :init
;;  (vertico-mode))
;;
;;  ;; ** Dabbrev
;;(use-package dabbrev
;;  :bind (("M-/" . dabbrev-completion)
;;  ("C-M-/" . dabbre-expand))
;;  :config
;;  (add-to-list 'dabbrev-ignored-buffer-regexps "\\` ")
;;  ;; Since 29.1, use 'dabbrev-ignored-buffer-regexps' on older.
;;  (add-to-list 'dabbrev-ignored-buffer-modes 'doc-view-mode)
;;  (add-to-list 'dabbrev-ignored-buffer-modes 'pdf-view-mode)
;;  (add-to-list 'dabbrev-ignored-buffer-modes 'tags-table-mode)
;;  )
;;
;;;; ** Corfu
;;(use-package corfu
;;  :custom
;;  (corfu-auto t)
;;  (corfu-auto-delay 0)
;;  (corfu-auto-prefix 1)
;;  (corfu-cycle t)
;;  :init
;;  (global-corfu-mode)
;;  :config
;;  (keymap-set corfu-map "RET" `( menu-item "" nil :filter
;;  ,(lambda (&optional _)
;;  (and (derived-mode-p 'eshell-mode 'comint-mode)
;;  #'corfu-send))))
;;  ) ; corfu
;;
;;  ;; ** Cape
;;(use-package cape
;;  :bind
;;  (("C-c p" . completion-at-point))
;;  :init
;;  (add-hook 'completion-at-point-functions #'cape-dabbrev)
;;  (add-hook 'completion-at-point-functions #'cape-file)
;;  (add-hook 'completion-at-point-functions #'cape-elisp-block)
;;  ) ; cape
;;
;;;; ** Consult
;;(use-package consult
;;  :hook (completion-list-mode . consult-preview-at-point-mode)
;;  :custom
;;  (xref-show-xrefs-function #'consult-xref)
;;  (xref-show-definitions-function #'consult-xref)
;;  :init
;;  (setq register-preview-delay 0.5
;;  register-preview-function #'consult-register-format)
;;  (advice-add #'register-preview :override #'consult-register-window)
;;
;;  :config
;;  ;; Optionally configure preview. The default value
;;  ;; is 'any, such that any key triggers the preview.
;;  ;; (setq consult-preview-key 'any)
;;  ;; (setq consult-preview-key "M-.")
;;  ;; (setq consult-preview-key '("S-<down>" "S-<up>"))
;;  ;; For some commands and buffer sources it is useful to configure the
;;  ;; :preview-key on a per-command basis using the 'consult-customize' macro.
;;  (consult-customize
;;   consult-theme :preview-key '(:debounce 0.2 any)
;;   consult-ripgrep consult-git-grep consult-grep
;;   consult-bookmark consult-recent-file consult-xref
;;   consult--source-bookmark consult--source-file-register
;;   consult--source-recent-file consult--source-project-recent-file
;;   ;; :preview-key "M-."
;;   :preview-key '(:debounce 0.4 any))
;;
;;  ;; Optionally configure the narrowing key.
;;  ;; Both < and C-+ work reasonably well.
;;  (setq consult-narrow-key "<") ;; "C-+"
;;
;;  ;; Optionally make narrowing help available in the minibuffer.
;;  ;; You may want to use 'embark-prefix-help-command' or which-key instead.
;;  ;; (keymap-set consult-narrow-map (concat consult-narrow-key " ?") #'consult-narrow-help)
;;  ) ; consult

;(use-package embark)

;(use-package eglot)

;; * Theming
(add-to-list 'custom-theme-load-path (concat user-emacs-directory "themes/"))
;; ** Catppuccin
(use-package catppuccin-theme
  :custom
  (catppuccin-flavor 'mocha)
  :config
  (load-theme 'catppuccin :no-confirm))

;; ** Doom Themes
(use-package doom-themes
  :custom
  (doom-themes-enable-bold t)
  (doom-themes-enable-italic t))

;; Evil Mode
(straight-use-package 'undo-fu)
(use-package evil
  :custom
  (evil-undo-system 'undo-fu)
  (evil-want-integration t)
  (evil-want-keybinding nil)
  (evil-vsplit-window-right t)
  (evil-vsplit-window-below t)
  (evil-set-leader nil (kbd "<SPC>"))
  :init
  (evil-mode 1)
  :config
  ;; Using RETURN to follow links in Org/Evil
  ;; Unmap keys in 'evil-maps if not done, (setq org-return-follows-link t) will not work
  (with-eval-after-load 'evil-maps
    (define-key evil-motion-state-map (kbd "SPC") nil)
    (define-key evil-motion-state-map (kbd "RET") nil)
    (define-key evil-motion-state-map (kbd "TAB") nil))
  ;; Setting RETURN key in org-mode to follow links
  ) ; evil
(use-package evil-collection
  :after evil
  :custom (evil-collection-mode-list '(dashboard dired ibuffer))
  :config (evil-collection-init)) ; evil-collection
(use-package evil-tutor
  :after evil)
(use-package general
  :after evil
  :after which-key
  :config
  (general-evil-setup)

  (general-create-definer fwalk/leader-keys
                          :states '(normal insert visual emacs)
                          :keymaps 'override
                          :prefix "SPC"
                          :global-prefix "M-SPC")
  (fwalk/leader-keys
    "a"     '(:ignore t :wk "Applications")
    "a d"   '(dashboard-open :wk "Dashboard")
    "a m"   '(magit :wk "Magit")
    "a e"   '(elfeed :wk "Elfeed RSS")
    "a f"   '(make-frame :wk "Open buffer in new frame")
    "a F"   '(select-frame-by-name :wk "Select frame by name"))
  (fwalk/leader-keys
    "b"     '(:ignore t :wk "buffer")
    "b b"   '(switch-to-buffer :wk "Switch buffer")
    "b i"   '(ibuffer :wk "IBuffer")
    "b k"   '(kill-this-buffer :wk "Kill buffer")
    "b n"   '(next-buffer :wk "Next buffer")
    "b p"   '(previous-buffer :wk "Previous buffer")
    "b r"   '(revert-buffer :wk "Reload buffer"))
  (fwalk/leader-keys
    "s"     '(:ignore t :wk "Split")
    "s v"   '(evil-window-vsplit :wk "Split vertical")
    "s h"   '(evil-window-split :wk "Split horizontal"))
  (fwalk/leader-keys
    "f"     '(:ignore t :wk "File")
    "f f"   '(find-file :wk "Find file")
    "f e"   '(dired :wk "Dired")
    "f r"   '(consult-recent-file :wk "Find recent file")
    "f s"   '(save-buffer :wk "Save file")
    "f q"   '(evil-quit :wk "Quit")
    "f c"   '((lambda () (interactive) (find-file "~/.config/emacs/init.el")) :wk "Open emacs config"))
  (fwalk/leader-keys
    "e"     '(:ignore t :wk "Evaluate")
    "e b"   '(eval-buffer :wk "Evaluate elisp buffer")
    "e d"   '(eval-defun :wk "Evaluate defun containing or after point")
    "e e"   '(eval-expression :wk "Evaluate elisp expression")
    "e l"   '(eval-last-sexp :wk "Evaluate elisp expression before point")
    "e r"   '(eval-region :wk "Evaluate elisp in region")
    "e s"   '(eshell :wk "Eshell"))
  (fwalk/leader-keys
    "h"     '(:ignore t :wk "Help")
    "h f"   '(describe-function :wk "Describe function")
    "h v"   '(describe-variable :wk "Describe variable")
    "h t"   '(load-theme :wk "Load theme"))
  (fwalk/leader-keys
    "g c"   '(comment-line :wk "Comment line"))
  (fwalk/leader-keys
    "w"     '(:ignore t :wk "Windows")
    "w c"   '(evil-window-delete :wk "Close window")
    "w n"   '(evil-window-new :wk "New window")
    "w s"   '(evil-window-split :wk "Horizontal split window")
    "w v"   '(evil-window-vsplit :wk "Vertical split window")

    "w h"   '(evil-window-left :wk "Window left")
    "w j"   '(evil-window-left :wk "Window down")
    "w k"   '(evil-window-left :wk "Window up")
    "w l"   '(evil-window-left :wk "Window right")
    "w w"   '(evil-window-next :wk "Next window"))
    ;;"w H"   '(buf-move-left :wk "Buffer move left")
    ;;"w J"   '(buf-move-down :wk "Buffer move down")
    ;;"w K"   '(buf-move-up :wk "Buffer move up")
    ;;"w L"   '(buf-move-right :wk "Buffer move right")
  (fwalk/leader-keys
    "l"     '(:ignore t :wk "Lsp")
    "l f"   '(flycheck-first-error :wk "First error")
    "l n"   '(flycheck-next-error :wk "Next error")
    "l p"   '(flycheck-next-error :wk "Previous error")
    "l l"   '(flycheck-list-errors :wk "List errors"))
  (fwalk/leader-keys
    "t"     '(:ignore t :wk "Toggle")
    "t t"   '(vterm-toggle :wk "Toggle vterm"))

  ;; (general-create-definer fwalk/visual-keys :states '(visual) :keymaps 'override)
  ;; (fwalk/visual-keys
  ;;   "g c" '(comment-line :wk "Comment line"))

  ;; (general-create-definer fwalk/normal-keys :states '(visual) :keymaps 'override)
  ;; (fwalk/normal-keys
  ;;  "g c"   '(:ignore t :wk "Comment")
  ;;  "g c c" '(comment-line :wk "Comment line")
  ;;  )

  ) ; general

;; * Org Mode
(use-package org
  ;; :bind (("C-c l" . org-store-link)
  ;;        ("C-c a" . org-agenda)
  ;;        ("C-c c" . org-capture))
  :hook (org-mode . org-indent-mode)
  :custom
  (org-startup-with-inline-images t)
  (org-agenda-files '("~/Notes/agenda.org"))
  (org-agenda-block-separator 45)
  (org-fancy-priorities-list '("[A]" "[B]" "C"))
  (org-priority-faces '((?A :forground "#f38ba8")
  			(?B :forground "#a6e3a1")
  			(?C :forground "#89b4fa")))
  (org-agenda-custom-commands
   '(("v" "A Better agenda view"
      ((tags "PRIORITY=\"A\""
	     ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
	      (org-agenda-overriding-header "High-priority unfinished tasks:")))
       (tags "PRIORITY=\"B\""
	     ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
	      (org-agenda-overriding-header "Medium-priority unfinished tasks:")))
       (tags "PRIORITY=\"C\""
	     ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
	      (org-agenda-overriding-header "Low-priority unfinished tasks:")))
       (agenda "")
       (alltodo "")))))
  :config
  (fwalk/leader-keys
    "o"     '(:ignore t :wk "Org")
    "o a"   '((lambda () (interactive) (find-file "~/Notes/agenda.org")) :wk "Open agenda file")
    "o A"   '(org-agenda :wk "Org agenda")
    "o e"   '(org-export-dispatch :wk "Org export dispatch")
    "o i"   '(org-toggle-item :wk "Org toggle item")
    "o t"   '(org-todo :wk "Org todo")
    "o T"   '(org-todo-list :wk "Org todo list"))
  (fwalk/leader-keys
    "o b"   '(:ignore t :wk "Tables")
    "o b -" '(org-table-insert-hline :wk "Insert hline in table"))
  (fwalk/leader-keys
    "o d"   '(:ignore t :wk "Date/deadline")
    "o d t" '(org-time-stamp :wk "Org time stamp"))
  (fwalk/leader-keys
    "o p"   '(:ignore t :wk "Org Priority")
    "o p u" '(org-priority-up :wk "Increase priority")
    "o p d" '(org-priority-down :wk "Decrease priority"))

  (custom-set-faces
   '(org-level-1 ((t (:inherit outline-1 :height 1.7))))
   '(org-level-2 ((t (:inherit outline-2 :height 1.6))))
   '(org-level-3 ((t (:inherit outline-3 :height 1.5))))
   '(org-level-4 ((t (:inherit outline-4 :height 1.4))))
   '(org-level-5 ((t (:inherit outline-5 :height 1.3))))
   '(org-level-6 ((t (:inherit outline-5 :height 1.2))))
   '(org-level-7 ((t (:inherit outline-5 :height 1.1))))))

(require 'org-tempo)
(use-package toc-org
  :after org
  :commands toc-org-enable
  :hook (org-mode . toc-org-enable))
(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode))

;; * Programming
;; ** lsp
(use-package lsp-mode
  :ensure t
  :custom
  (lsp-inlay-hint-enable 1)
  (lsp-keymap-prefix "C-c l")
  :init
  :hook
  (lsp-mode . lsp-enable-which-key-integration)
  (rust-mode . lsp)
  ;(ron-mode . lsp)
  ;(python-mode . lsp)
  ;(lua-mode . lsp)
  ;(zig-mode . lsp)
  ;(fish-mode . lsp)
  ;(hy-mode . lsp)
  ;(lfe-mode . lsp)
  ;(gdscript-mode . lsp)
  :commands lsp)

;; ** Flycheck
(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))
(straight-use-package 'dash) ;; required for flycheck-rust
(use-package flycheck-rust
  :ensure t
  :hook
  (flycheck-mode . flycheck-rust-setup))
(use-package flycheck-inline
  :hook
  (flycheck-mode . flycheck-inline-mode)) ; end flycheck-inline

;; ** Treesitter
(use-package treesit-auto
  :custom
  (treesit-auto-install 'prompt)
  :config
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))

;; ** Languages
(straight-use-package 'ron-mode)
(straight-use-package 'lua-mode)
(straight-use-package 'zig-mode)
(straight-use-package 'fish-mode)
(straight-use-package 'python-mode)
(straight-use-package 'hy-mode)
(straight-use-package 'lfe-mode)
(straight-use-package 'rust-mode)
(straight-use-package 'gdscript-mode)

;; ** Magit
(straight-use-package 'magit)

;; * Shells and Terminals
;; ** EShell
(use-package eshell-syntax-highlighting
  :after esh-mode
  :custom
  (eshell-rc-script (concat user-emacs-directory "eshell/profile"))
  (eshell-aliases-file (concat user-emacs-directory "eshell/aliases"))
  (eshell-history-size 5000)
  (eshell-buffer-maximum-lines 5000)
  (eshell-hist-ignore-dupes t)
  (eshell-scroll-to-bottom-on-input t)
  (eshell-destroy-buffer-when-proccess-dies t)
  (eshell-visual-commands '("bash" "fish" "htop" "ssh" "top" "zsh"))
  :config
  (eshell-syntax-highlighting-global-mode +1))

;; ** VTerm
(use-package vterm
  :custom
  (shell-file-name "/bin/fish")
  (vterm-max-scrollback 5000))

;; ** VTerm-toggle
(use-package vterm-toggle
  :after vterm
  :custom
  (vterm-toggle-fullscreen-p nil)
  (vterm-toggle-scope 'project)
  :config
  (add-to-list 'display-buffer-alist
	       '((lambda (buffer-or-name _)
		   (let ((buffer (get-buffer buffer-or-name)))
		     (with-current-buffer buffer
		       (or (equal major-mode 'vterm-mode)
			   (string-prefix-p vterm-buffer-name (buffer-name buffer))))))
		 (display-buffer-reuse-window display-buffer-at-bottom)
		 (reusable-frames . visible)
		 (window-height . 0.3)))
  ) ; vterm-toggle

;; * Applications
;; ** ElFeed
(use-package elfeed
  :after evil
  :custom
  (elfeed-search-feed-face ":foreground #ffffff :weight bold")
  (elfeed-feeds (quote
		 (("https://www.reddit.com/r/linux.rss" reddit linux)
                  ("https://www.reddit.com/r/emacs.rss" reddit linux)
                  ;;("https://voidlinux.org/atom.xml" void linux)
                  ("https://www.reddit.com/r/voidlinux.rss" reddit void linux))))
  :config
  (evil-define-key 'normal elfeed-show-mode-map
    (kbd "J") 'elfeed-goodies/split-show-next
    (kbd "K") 'elfeed-goodies/split-show-prev)
  (evil-define-key 'normal elfeed-search-mode-map
    (kbd "J") 'elfeed-goodies/split-show-next
    (kbd "K") 'elfeed-goodies/split-show-prev))
(use-package elfeed-goodies
  :after elfeed
  :init (elfeed-goodies/setup)
  :custom (elfeed-goodies/entry-pane-size 0.5))
;; (use-package elfeed-protocol
;;   :after elfeed
;;   :custom (elfeed-use-curl t))
;;; init.el ends here
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files '("/home/fwalk/Notes/agenda.org") nil nil "Customized with use-package org"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-level-1 ((t (:inherit outline-1 :height 1.7))))
 '(org-level-2 ((t (:inherit outline-2 :height 1.6))))
 '(org-level-3 ((t (:inherit outline-3 :height 1.5))))
 '(org-level-4 ((t (:inherit outline-4 :height 1.4))))
 '(org-level-5 ((t (:inherit outline-5 :height 1.3))))
 '(org-level-6 ((t (:inherit outline-5 :height 1.2))))
 '(org-level-7 ((t (:inherit outline-5 :height 1.1)))))
