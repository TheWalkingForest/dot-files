#!/usr/bin/env bash

# python
alias py='python3'

# git
alias add='git add'
alias cln='git clone'
alias commit='git commit'
alias push='git push'
alias st='git status'
alias lg='git log'
alias init='git init'
alias pull='git pull'
alias checkout='git checkout'
alias fetch='git fetch'
alias tag='git tag'

# cd
alias ..='cd ..'
alias ...='cd ../..'
alias cdRust='cd ~/Projects/Rust'
alias cdDocker='cd ~/Projects/Docker'
alias cdDockData='cd /mnt/misc/docker'
alias cdpy='cd ~/Projects/Python'
function ws() {
    cd "$HOME"/Projects/workspace-csc"$1"
}

# mv
alias mv='mv -i'

# rm
alias rm='rm -i'

# mkdir
alias mkdir='mkdir -v'

# sudo
alias sui='/usr/bin/sudo -i'

# exa
# alias l='exa --color=always --group-directories-first --icons'
# alias ls='exa -lBF --git --color=always --group-directories-first --icons'
# alias la='exa -laBF --git --color=always --group-directories-first --icons'
# alias ll='exa -lmhgBF --git --color=always --group-directories-first --icons'
# alias l.='exa -laBF --git --group-directories-first --icons | rg "^\."'

# bat
# alias cat='bat --color=always --decorations=always --theme="Dracula"'

# grep
alias grep='grep --color=always'
alias egrep='egrep --color=always'
alias fgrep='fgrep --color=always'

# systemctl
alias start='sudo systemctl start'
alias stop='sudo systemctl stop'
alias sysenable='sudo systemctl enable'
alias sysdisable='sudo systemctl disable'


### Init system alias
#### systemctl
if command -v systemctl &> /dev/null
then
    if command -v doas &> /dev/null
    then
        alias sysstart='doas systemctl start'
        alias sysstop='doas systemctl stop'
        alias sysenable='doas systemctl enable'
        alias sysdisable='doas systemctl disable'
    else
        alias sysstart='sudo systemctl start'
        alias sysstop='sudo systemctl stop'
        alias sysenable='sudo systemctl enable'
        alias sysdisable='sudo systemctl disable'
    fi
fi
#### runit
if command -v sv &> /dev/null
then
    if command -v doas &> /dev/null
    then
        alias svup='doas sv up'
        alias svdown='doas sv down'
        alias svstatus='doas sv status'
        alias svrestart='doas sv restart'
        alias svruning='ls /var/service'
        alias svlist='ls /etc/sv'
        function svlink() {
            doas ln -s /etc/sv/"$1" /var/service/"$1"
        }
        function svunlink() {
            sudo rm /var/service/"$1"
        }
    else
        alias svup='sudo sv up'
        alias svdown='sudo sv down'
        alias svstatus='sudo sv status'
        alias svrestart='sudo sv restart'
        alias svruning='ls /var/service'
        alias svlist='ls /etc/sv'

        function svlink() {
            sudo ln -s /etc/sv/"$1" /var/service/"$1"

        }
        function svunlink() {
            sudo rm /var/service/"$1"

        }
    fi
fi

# tar
alias targz='tar -xvf'
alias tarxz='tar -xvf'

# moc
alias mocp='mocp --theme nightly_enhanced'

# mdcat - view markdown files in terminal
alias md='mdcat'

# ip
alias ip='ip --color=auto'

# shellcheck
alias sc='shellcheck'

# vpsm
alias vps-up='vpsm upr && vpsm bu && vpsm ups'

# youtube-dlc
alias ytdl='youtube-dlc'
alias ytdl-aac="youtube-dlc --extract-audio --audio-format aac "
alias ytdl-best="youtube-dlc --extract-audio --audio-format best "
alias ytdl-flac="youtube-dlc --extract-audio --audio-format flac "
alias ytdl-m4a="youtube-dlc --extract-audio --audio-format m4a "
alias ytdl-mp3="youtube-dlc --extract-audio --audio-format mp3 "
alias ytdl-opus="youtube-dlc --extract-audio --audio-format opus "
alias ytdl-vorbis="youtube-dlc --extract-audio --audio-format vorbis "
alias ytdl-wav="youtube-dlc --extract-audio --audio-format wav "
alias ytdl-best="youtube-dlc -f bestvideo+bestaudio "

## get top process eating memory
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'

## get top process eating cpu ##
alias pscpu='ps auxf | sort -nr -k 3'
alias pscpu10='ps auxf | sort -nr -k 3 | head -10'

## Docker
alias dcomp='docker-compose'
alias dcup='docker-compose up'
alias dcdown='docker-compose down'
