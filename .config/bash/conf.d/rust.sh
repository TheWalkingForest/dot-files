export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export CARGO_HOME="$XDG_DATA_HOME/cargo"

CARGO_BIN="$CARGO_HOME/bin"
if test -d "$CARGO_BIN"; then
    bash_add_path "$CARGO_BIN"
fi
