# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    source /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    source /etc/bash_completion
  fi
fi

export BASH_SILENCE_DEPRECATION_WARNING=1
export TERM="xterm-256color"

export HISTCONTROL=ignoredups:erasedups
export EDITOR=nvim                            # Sets vim as default terminal editor
export VISUAL=mousepad                        # Sets gedit as default gui editor
#export MANPAGER="nvim -c 'set ft=man' -"      # "nvim" as manpager
export XBPS_DISTDIR="$HOME"/void-packages     # location of void-packages

export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:-$HOME/.config}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:-$HOME/.cache}
export XDG_STATE_HOME=${XDG_STATE_HOME:-$HOME/.local/state}
export XDG_DATA_HOME=${XDG_DATA_HOME:-$HOME/.local/share}

HISTSIZE=1000
SAVEHIST=1000
export HISTFILE="${XDG_STATE_HOME}/bash/history"

# sets command line commands to vim
set -o vi

# append to the history file, don't overwrite it
shopt -s histappend

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize
