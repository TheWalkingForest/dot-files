# export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
bash_add_path() {
    if [ -d "$1" ]; then
        export PATH="$PATH:$1"
    fi
}
bash_add_path_front() {
    if [ -d "$1" ]; then
        export PATH="$1:$PATH"
    fi
}

bash_add_path "$HOME/.bin"                # Adds private bin to path
bash_add_path "$HOME/.local/bin"          # Adds .local/bin to path
bash_add_path "$HOME/.scripts"            # Adds scripts directory to path
