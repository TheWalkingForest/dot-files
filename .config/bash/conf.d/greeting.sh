fish_greeting() {
    if command -v  starfetch; then
        starfetch
    elif command -v  neofetch; then
        neofetch
    elif command -v  pfetch; then
        pfetch
    fi
}
