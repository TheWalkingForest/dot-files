# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color)
        color_prompt=yes
        source "$XDG_CONFIG_HOME/bash/lib/color.sh"
        ;;
    *) return ;;
esac


## git branch function
get_git_branch() {
    readonly GIT_PROMPT_MACOS="/Library/Developer/CommandLineTools/usr/share/git-core/git-prompt.sh"
    readonly GIT_PROMPT_LINUX="/Library/Developer/CommandLineTools/usr/share/git-core/git-prompt.sh"

    if [ -f "$GIT_PROMPT_LINUX" ]; then
        source "$GIT_PROMPT_LINUX"
    elif [ -f "$GIT_PROMPT_MACOS" ]; then
        source "$GIT_PROMPT_MACOS"
    fi

    branch="$(__git_ps1 '%s')"
    if [ -n "$branch" ]; then
        printf "%b(%s)%b " "${BOLD}${FG_GREEN}" "${branch}" "${RESET}"
    fi
}

prompt_zig() {
    ZIG="zig::[$(zig version)]"
    printf " %b%s%b" "${BOLD}${FG_YELLOW}" "$ZIG" "${RESET}"
}
prompt_rustc() {
    RUSTC="rust::[$(rustc --version 2> /dev/null | awk '{print $2}')]"
    printf " %b%s%b" "${BOLD}${FG_RED}" "$RUSTC" "${RESET}"
}
prompt_python() {
    PYTHON="python::[$(python3 --version 2> /dev/null | awk '{print $2}')]"
    printf " %b%s%b" "${BOLD}${FG_RED}" "$PYTHON" "${RESET}"
}
prompt_nim() {
    NIM="nim::[$(nim --version | /usr/bin/grep Nim | awk '{print $4}')]"
    printf " %b%s%b" "${BOLD}${FG_YELLOW}" "$NIM" "${RESET}"
}

prompt() {
    check_zig=
    check_rustc=
    check_python=
    check_nim=
    for file in ./*; do
        ext="${file##*.}"
        if [ -z "$check_zig" ] && [ "$ext" = zig ]; then
            prompt_zig
            check_zig="done"
        elif [ -z "$check_rustc" ] && [ "$ext" = rs ] || [ "$file" = "Cargo.toml" ]; then
            prompt_rustc
            check_rustc="done"
        elif [ -z "$check_python" ] && [ "$ext" = py ]; then
            prompt_python
            check_python="done"
        elif [ -z "$check_nim" ] && [ "$ext" = nim ] || [ "$ext" = nimble ]; then
            prompt_nim
            check_nim="done"
        fi
    done
}

if [ "$color_prompt" = "no" ]; then
    export PS1="${debian_chroot:+($debian_chroot)}\u@\h \w\n> "
elif [ "$COLORS_SH" = "true" ]; then
    #export PS1="${debian_chroot:+($debian_chroot)}${FG_GREEN}\u${RESET}@\h in ${FG_GREEN}\w${RESET}\$(prompt)\n\$(get_git_branch)> "
    export PS1="${debian_chroot:+($debian_chroot)}${FG_GREEN}\u${RESET}@\h in ${FG_GREEN}\w${RESET}\$(prompt)\n> "
else
    RESET="\[\033[0m\]"
    BLUE="\[\033[34m\]"
    export PS1="${debian_chroot:+($debian_chroot)}${BLUE}\u${RESET}@\h ${BLUE}\w${RESET}\n> "
fi
