nv() {
    if [ $# -lt 2 ]; then
        nvim .
    else
        nvim "$@"
    fi
}
