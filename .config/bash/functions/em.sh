em() {
    command emacsclient --socket-name=/tmp/emacs"$(id -u $USER)"/"$USER"-emacsd -c -a 'emacs' $@; or \
        zenity --error --no-wrap --text 'Failed to connect to the Emacs daemon!' ^/dev/null
}
