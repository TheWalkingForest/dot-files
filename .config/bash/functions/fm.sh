fm() {
    if [ $# -lt 2 ]; then
        vifm . .
    else
        vifm "$@"
    fi
}
