fnv() {
    if set -q dir; and not test -n "$dir"; then
        set dir '.'
    else
        echo "'$dir'"
    fi

    res=$(find . -type f | fzf --preview 'bat --color always {}')

    if set -q res; and not test -n "$res"; then
        echo "$res"
    else
        nv $res
    fi
}
