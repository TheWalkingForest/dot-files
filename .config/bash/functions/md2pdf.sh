md2pdf() {
    pdf=$(echo "$1" | sed s/.md/.pdf/)
    pandoc "$1" -s -o "$pdf"
}
