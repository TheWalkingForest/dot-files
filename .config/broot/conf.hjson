###############################################################
# This file's format is Hjson ( https://hjson.github.io/ ). Some
# properties are commented out. To enable them, remove the `#`.
###############################################################

default_flags: '-gIh'

modal: true
initial_mode: command

show_selection_mark: true

cols_order: [
    mark
    git
    branch
    size
    permission
    date
    count
    name
]

icon_theme: nerdfont

###############################################################
# Special paths
# If some paths must be handled specially, uncomment (and change
# this section as per the examples)
# Setting "list":"never" on a dir prevents broot from looking at its
#  children when searching, unless the dir is the selected root.
# Setting "sum":"never" on a dir prevents broot from looking at its
#  children when computing the total size and count of files.
# Setting "show":"always" makes a file visible even if its name
#  starts with a dot.
# Setting "list":"always" may be useful on a link to a directory
#  (they're otherwise not entered by broot unless selected)
#
special_paths: {
    "**/.git": {"list": "never" }
    "~/.config": { "show": "always" }
    "~/temp": { "show": "never" }
}

content_search_max_file_size: 10MB

# max_panels_count: 2

enable_kitty_keyboard: false

lines_before_match_in_preview: 1
lines_after_match_in_preview: 1

preview_transformers: [
    {
        input_extensions: [ "pdf" ] // case doesn't matter
        output_extension: png
        mode: image
        command: [ "mutool", "draw", "-w", "1000", "-o", "{output-path}", "{input-path}" ]
    }

    {
        input_extensions: [ "xls", "xlsx", "doc", "docx", "ppt", "pptx", "ods", "odt", "odp" ]
        output_extension: png
        mode: image
        command: [
            "libreoffice", "--headless",
            "--convert-to", "png",
            "--outdir", "{output-dir}",
            "{input-path}"
        ]
    }

    {
        input_extensions: [ "json" ]
        output_extension: json
        mode: text
        command: [ "jq" ]
    }
]

imports: [

    verbs.hjson

    {
        luma: [
            dark
            unknown
        ]
        file: skins/catppuccin-mocha.hjson
    }

    {
        luma: light
        file: skins/white.hjson
    }
]
