return {
    ['Python main setup']= {
        prefix = 'pymain',
        body = {
            '#!/usr/bin/env python3',
            '',
            'def main():',
            '    pass',
            '',
            'if __name__ == \'___main___\':',
            '    main()'
        },
        description = 'Setup a python script for python main'
    }
}
