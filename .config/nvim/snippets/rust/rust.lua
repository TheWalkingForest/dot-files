return {
    ['Color Eyre Setup'] = {
        prefix = 'coleyre',
        body = {
            'if std::env::var("RUST_BACKTRACE").is_err() {',
            '    std::env::set_var("RUST_BACKTRACE", "1");',
            '}',
            'color_eyre::install().unwrap();'
        }
    },
    ['Env Logger'] = {
        prefix = 'envlog',
        body = {
            'if std::env::var("RUST_LOG").is_err() {',
            '    std::env::set_var("RUST_LOG", "debug");',
            '}',
            'env_logger::init();'
        }
    },
    ['test'] = {
        prefix = 'test',
        body = {
            '#[test]',
            'fn ${1}() {',
            '}'
        }
    },
}
