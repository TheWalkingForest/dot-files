return {
    Ocaml_Format = {
        prefix = "ocfmt",
        body = {
            "profile = janestreet",
            "version = 0.26.0"
        },
    },
    Git_Email = {
        prefix = "gitemail",
        body = {
            "4783673-TheWalkingForest@users.noreply.gitlab.com"
        },
    },
    Lorem1P = {
        prefix = "lorem1p",
        body = {
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean tempus a sem nec mattis. Nulla id dolor quis sapien egestas imperdiet quis quis diam. Proin nulla ante, varius a vulputate ut, rutrum eget sapien. Morbi vitae erat sapien. Donec et vehicula ante. Pellentesque ac dignissim mauris. Etiam mattis risus vel laoreet eleifend. Donec eu massa sit amet enim sagittis viverra. Cras ultrices tempus odio."
        },
    },
    Lorem3P = {
        prefix = "lorem3p",
        body = {
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean tempus a sem nec mattis. Nulla id dolor quis sapien egestas imperdiet quis quis diam. Proin nulla ante, varius a vulputate ut, rutrum eget sapien. Morbi vitae erat sapien. Donec et vehicula ante. Pellentesque ac dignissim mauris. Etiam mattis risus vel laoreet eleifend. Donec eu massa sit amet enim sagittis viverra. Cras ultrices tempus odio.",
            "",
            "Pellentesque ac velit et nisi finibus tempus. Phasellus aliquam, neque id molestie gravida, mi arcu gravida diam, et bibendum quam neque vitae sapien. Sed quis arcu sit amet nunc maximus viverra eu et lectus. Vestibulum vestibulum porta magna, non volutpat velit. Suspendisse potenti. Nam imperdiet tellus condimentum, condimentum eros quis, tempus sem. Nullam volutpat, libero ut vehicula ullamcorper, nunc lorem molestie turpis, consectetur condimentum nulla dolor eu dolor. Pellentesque dictum lectus euismod quam semper molestie. Vivamus id tortor tellus. Integer eu placerat purus. Nulla facilisi. Nunc efficitur ullamcorper est eget porttitor. Nulla porttitor finibus nibh, vitae aliquet nisl gravida nec.",
            "",
            "In a sodales urna. Sed dapibus quis lorem in tristique. Duis in massa massa. Aliquam ullamcorper nulla non ipsum cursus luctus. Nam laoreet eros sed leo commodo maximus. Proin condimentum pellentesque mauris, sed tempus ex. Etiam pharetra ullamcorper leo ut venenatis. Duis ipsum magna, hendrerit non fringilla in, faucibus tincidunt nulla. Etiam sagittis tellus nulla, sed molestie neque rhoncus id. In consequat rutrum erat. Nulla dui nulla, gravida ut lectus vel, lobortis convallis tortor. Morbi semper arcu at sodales tristique. Suspendisse ut risus ullamcorper, accumsan velit vel, rutrum ex. Nam in facilisis ante, nec mattis ligula."
        },
    },
    Lorem5P = {
        prefix = "lorem5p",
        body = {
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean tempus a sem nec mattis. Nulla id dolor quis sapien egestas imperdiet quis quis diam. Proin nulla ante, varius a vulputate ut, rutrum eget sapien. Morbi vitae erat sapien. Donec et vehicula ante. Pellentesque ac dignissim mauris. Etiam mattis risus vel laoreet eleifend. Donec eu massa sit amet enim sagittis viverra. Cras ultrices tempus odio.",
            "",
            "Pellentesque ac velit et nisi finibus tempus. Phasellus aliquam, neque id molestie gravida, mi arcu gravida diam, et bibendum quam neque vitae sapien. Sed quis arcu sit amet nunc maximus viverra eu et lectus. Vestibulum vestibulum porta magna, non volutpat velit. Suspendisse potenti. Nam imperdiet tellus condimentum, condimentum eros quis, tempus sem. Nullam volutpat, libero ut vehicula ullamcorper, nunc lorem molestie turpis, consectetur condimentum nulla dolor eu dolor. Pellentesque dictum lectus euismod quam semper molestie. Vivamus id tortor tellus. Integer eu placerat purus. Nulla facilisi. Nunc efficitur ullamcorper est eget porttitor. Nulla porttitor finibus nibh, vitae aliquet nisl gravida nec.",
            "",
            "In a sodales urna. Sed dapibus quis lorem in tristique. Duis in massa massa. Aliquam ullamcorper nulla non ipsum cursus luctus. Nam laoreet eros sed leo commodo maximus. Proin condimentum pellentesque mauris, sed tempus ex. Etiam pharetra ullamcorper leo ut venenatis. Duis ipsum magna, hendrerit non fringilla in, faucibus tincidunt nulla. Etiam sagittis tellus nulla, sed molestie neque rhoncus id. In consequat rutrum erat. Nulla dui nulla, gravida ut lectus vel, lobortis convallis tortor. Morbi semper arcu at sodales tristique. Suspendisse ut risus ullamcorper, accumsan velit vel, rutrum ex. Nam in facilisis ante, nec mattis ligula.",
            "",
            "Praesent cursus ligula quis diam vestibulum accumsan. Cras cursus ipsum quis dui dictum scelerisque. Mauris turpis odio, posuere a elit ut, luctus pretium est. Donec sagittis fermentum eros quis rutrum. Proin eu ante quis odio tincidunt luctus non nec massa. Suspendisse potenti. Nunc erat magna, lacinia sed ultricies ac, lacinia quis erat. Nullam pulvinar nec nunc in varius. Suspendisse potenti. Nunc nisl nisl, dapibus vel orci eu, consectetur pretium sapien. Proin tempor, nisi sed lobortis porttitor, sem est rutrum velit, nec malesuada augue elit euismod mauris. Quisque nec sodales neque. Vivamus vitae erat eu justo vulputate placerat in et nulla. Sed et feugiat lorem. Nullam non purus eget urna rhoncus suscipit sed in nibh. Curabitur aliquet vel nisi sit amet congue.",
            "",
            "Aenean non mattis tortor. Vivamus porttitor semper diam quis tincidunt. Sed cursus ex nec urna laoreet semper. Proin consectetur non velit eu pulvinar. Etiam finibus neque eget odio euismod imperdiet. Fusce egestas cursus velit, aliquet finibus mauris iaculis nec. Nunc sollicitudin sem eget magna maximus, in scelerisque metus luctus. Mauris eleifend luctus leo et venenatis. Cras porta sit amet ex a tempor. Sed sit amet dolor erat. Proin nec erat urna. Vestibulum vel consequat sem, a bibendum lorem. Cras dictum fermentum dui mattis tristique. Donec sagittis ipsum vitae diam euismod aliquam. Pellentesque non ex ullamcorper, malesuada libero eget, euismod neque. Aliquam efficitur non augue vel ornare."
        },
    },
    Lorem10P = {
        prefix = "lorem10p",
        body = {
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris dictum bibendum ipsum, eget dictum ante volutpat eget. Sed eu lacus facilisis, tristique tortor et, condimentum justo. Praesent sed lectus sapien. Quisque ligula arcu, egestas at ultricies nec, euismod gravida mi. Curabitur malesuada dolor id nibh hendrerit, eget gravida enim venenatis. Etiam nec porta purus. Sed tristique, tellus eu faucibus tempor, nunc nisl tristique risus, at ultrices quam massa et diam. Curabitur fringilla a nunc eget dapibus. Donec dictum ligula libero, in condimentum dui pharetra non. Duis nunc metus, tempus eu elit vitae, sodales fermentum lacus. Duis tempus justo quis lorem facilisis, eu fringilla ante porttitor. Donec ac pulvinar tortor. Morbi ante massa, lobortis sit amet mi laoreet, sagittis vestibulum libero. Cras molestie lectus ipsum, vel hendrerit nisl vestibulum in. Morbi viverra, justo vitae luctus scelerisque, dui risus laoreet neque, tempor venenatis est tellus a risus.",
            "",
            "Duis id purus dapibus, ornare ex id, faucibus metus. Cras congue consectetur orci ut vestibulum. Pellentesque ut interdum odio, non interdum risus. Donec sagittis in nisi id efficitur. Proin semper mauris ut nulla porta, ut cursus purus dictum. Praesent sodales in nisi ut gravida. Sed nisl neque, maximus eleifend purus ut, varius mattis lectus. Duis ut enim semper, porttitor ipsum sed, semper tortor. Mauris ut elit vitae eros rhoncus volutpat quis a felis.",
            "",
            "Aliquam et ipsum ac arcu lacinia porttitor. Sed ac pretium ligula, sed dictum nulla. Vivamus tempor sed lacus in bibendum. Mauris in tellus at velit tincidunt posuere vel eget nisl. Fusce consectetur tincidunt mi. Aliquam pharetra ipsum non ornare convallis. Aenean ornare sit amet dolor nec convallis. Sed efficitur semper mauris non bibendum. Vestibulum eget nibh et neque convallis ornare. Curabitur vel faucibus erat. In auctor tincidunt turpis, quis aliquet augue laoreet id.",
            "",
            "Vestibulum ut porta dui. Cras elementum diam ut lorem dictum molestie. Proin porta ornare eros, eu gravida ipsum aliquet sed. Ut vulputate at risus luctus sodales. Suspendisse pretium neque in ullamcorper vehicula. Donec ut lobortis ex. Donec vitae tellus dignissim, dignissim purus nec, mattis magna. In neque risus, posuere non justo vitae, pulvinar viverra ex.",
            "",
            "Fusce quis porta nunc. Phasellus in est vel magna hendrerit rutrum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur ut luctus felis, ac dapibus odio. Nullam iaculis nibh vel justo iaculis molestie. Aliquam bibendum magna et eleifend mollis. Nulla facilisi. Proin nibh ipsum, sagittis non dolor vel, molestie consectetur felis. Aliquam elementum dapibus sagittis. Maecenas arcu magna, maximus sed molestie eu, mattis quis tortor. Sed pulvinar, metus vel dictum consequat, tellus metus lacinia urna, in tristique nisi mi sed libero.",
            "",
            "Nulla rhoncus interdum mattis. Aliquam imperdiet luctus fringilla. Cras a pretium arcu. Donec sed efficitur mauris. Etiam efficitur turpis turpis, vitae auctor libero vulputate ut. Sed mollis vitae purus vestibulum viverra. Morbi quis leo in urna consectetur consequat. Donec bibendum quam eu congue imperdiet. Vivamus dictum risus molestie, aliquet tellus id, accumsan tellus.",
            "",
            "Sed eget viverra purus. Quisque imperdiet leo nunc, at tempor dui eleifend posuere. Cras dapibus eget leo nec placerat. Fusce consectetur pretium dui a eleifend. Etiam rutrum vitae libero at elementum. Curabitur tempor ullamcorper mauris. Nulla hendrerit vel neque sit amet accumsan.",
            "",
            "Integer non tortor vitae nisi maximus ullamcorper. Maecenas aliquam diam eu velit blandit ultricies et nec dui. Donec tortor est, varius et ultricies ac, varius id felis. Nullam ullamcorper, ipsum non bibendum efficitur, arcu eros molestie tortor, nec scelerisque enim neque vel tortor. Etiam hendrerit euismod massa, eget sollicitudin nisi sodales sed. Donec eu augue sed nisi dictum egestas a nec mauris. Aliquam erat volutpat. Proin ac odio mollis, malesuada neque sit amet, vehicula dui. Fusce luctus elit et dolor feugiat ornare. Mauris semper enim sit amet lacus mollis scelerisque. Curabitur lobortis, neque et vestibulum lacinia, enim urna faucibus mauris, in hendrerit velit risus id ante.",
            "",
            "Nullam nec sagittis tortor. Aliquam quis nisi vulputate purus elementum sodales. Nullam placerat, leo eu placerat cursus, nunc sapien ornare metus, quis sollicitudin mauris libero et felis. Curabitur quis lectus ac neque varius condimentum. Proin consequat, nibh at fermentum molestie, nunc nibh aliquet sapien, mollis cursus dolor erat at ante. Donec interdum eros non tortor ornare pretium. Nulla rutrum fermentum dolor, at ullamcorper mi congue eu. Sed id fermentum ex. Proin finibus ligula in purus suscipit varius. Sed dictum euismod viverra.",
            "",
            "Pellentesque bibendum sagittis interdum. Integer bibendum pharetra est, vitae dignissim massa porta eget. Morbi iaculis leo in metus feugiat, ut scelerisque ex euismod. Nullam tempus ut lectus et finibus. Vestibulum bibendum posuere mauris vel dapibus. Phasellus dolor lacus, tincidunt vitae risus nec, suscipit luctus mi. Ut bibendum malesuada lectus, eget dignissim massa luctus ut. Aliquam justo libero, malesuada eu varius quis, malesuada a massa."
        },
    }
}
