return {
    ['Ocaml Format'] = {
        prefix = 'ocfmt',
        body = {
            'profile = janestreet',
            'version = 0.26.0',
        },
    }
}
