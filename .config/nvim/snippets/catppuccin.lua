return {
    ['Cattppuccin Frappe Rosewater Hex'] = {
        prefix = 'cat-frappe-Rosewater-hex',
        body = {
            '#f2d5cf'
        }
    },
    ['Cattppuccin Frappe Rosewater rgb'] = {
        prefix = 'cat-frappe-Rosewater-rgb',
        body = {
            'rgb(242, 213, 207)'
        }
    },
    ['Cattppuccin Frappe Rosewater hsl'] = {
        prefix = 'cat-frappe-Rosewater-hsl',
        body = {
            'hsl(10, 57%, 88%)'
        }
    },
    ['Cattppuccin Frappe Flamingo Hex'] = {
        prefix = 'cat-frappe-Flamingo-hex',
        body = {
            '#eebebe'
        }
    },
    ['Cattppuccin Frappe Flamingo rgb'] = {
        prefix = 'cat-frappe-Flamingo-rgb',
        body = {
            'rgb(238, 190, 190)'
        }
    },
    ['Cattppuccin Frappe Flamingo hsl'] = {
        prefix = 'cat-frappe-Flamingo-hsl',
        body = {
            'hsl(0, 59%, 84%)'
        }
    },
    ['Cattppuccin Frappe Pink Hex'] = {
        prefix = 'cat-frappe-Pink-hex',
        body = {
            '#f48e4'
        }
    },
    ['Cattppuccin Frappe Pink rgb'] = {
        prefix = 'cat-frappe-Pink-rgb',
        body = {
            'rgb(244, 184, 228)'
        }
    },
    ['Cattppuccin Frappe Pink hsl'] = {
        prefix = 'cat-frappe-Pink-hsl',
        body = {
            'hsl(316, 73%, 84%)'
        }
    },
    ['Cattppuccin Frappe Mauve Hex'] = {
        prefix = 'cat-frappe-Mauve-hex',
        body = {
            '#ca9ee6'
        }
    },
    ['Cattppuccin Frappe Mauve rgb'] = {
        prefix = 'cat-frappe-Mauve-rgb',
        body = {
            'rgb(202, 158, 230)'
        }
    },
    ['Cattppuccin Frappe Mauve hsl'] = {
        prefix = 'cat-frappe-Mauve-hsl',
        body = {
            'hsl(277, 59%, 76%)'
        }
    },
    ['Cattppuccin Frappe Red Hex'] = {
        prefix = 'cat-frappe-Red-hex',
        body = {
            '#e78284'
        }
    },
    ['Cattppuccin Frappe Red rgb'] = {
        prefix = 'cat-frappe-Red-rgb',
        body = {
            'rgb(231, 130, 132)'
        }
    },
    ['Cattppuccin Frappe Red hsl'] = {
        prefix = 'cat-frappe-Red-hsl',
        body = {
            'hsl(359, 68%, 71%)'
        }
    },
    ['Cattppuccin Frappe Maroon Hex'] = {
        prefix = 'cat-frappe-Maroon-hex',
        body = {
            '#ea999c'
        }
    },
    ['Cattppuccin Frappe Maroon rgb'] = {
        prefix = 'cat-frappe-Maroon-rgb',
        body = {
            'rgb(234, 153, 156)'
        }
    },
    ['Cattppuccin Frappe Maroon hsl'] = {
        prefix = 'cat-frappe-Maroon-hsl',
        body = {
            'hsl(358, 66%, 76%)'
        }
    },
    ['Cattppuccin Frappe Peach Hex'] = {
        prefix = 'cat-frappe-Peach-hex',
        body = {
            '#ef9f76'
        }
    },
    ['Cattppuccin Frappe Peach rgb'] = {
        prefix = 'cat-frappe-Peach-rgb',
        body = {
            'rgb(239, 159, 118)'
        }
    },
    ['Cattppuccin Frappe Peach hsl'] = {
        prefix = 'cat-frappe-Peach-hsl',
        body = {
            'hsl(20, 79%, 70%)'
        }
    },
    ['Cattppuccin Frappe Yellow Hex'] = {
        prefix = 'cat-frappe-Yellow-hex',
        body = {
            '#e5c890'
        }
    },
    ['Cattppuccin Frappe Yellow rgb'] = {
        prefix = 'cat-frappe-Yellow-rgb',
        body = {
            'rgb(229, 200, 144)'
        }
    },
    ['Cattppuccin Frappe Yellow hsl'] = {
        prefix = 'cat-frappe-Yellow-hsl',
        body = {
            'hsl(40, 62%, 73%)'
        }
    },
    ['Cattppuccin Frappe Green Hex'] = {
        prefix = 'cat-frappe-Green-hex',
        body = {
            '#a6d189'
        }
    },
    ['Cattppuccin Frappe Green rgb'] = {
        prefix = 'cat-frappe-Green-rgb',
        body = {
            'rgb(166, 209, 137)'
        }
    },
    ['Cattppuccin Frappe Green hsl'] = {
        prefix = 'cat-frappe-Green-hsl',
        body = {
            'hsl(96, 44%, 68%)'
        }
    },
    ['Cattppuccin Frappe Teal Hex'] = {
        prefix = 'cat-frappe-Teal-hex',
        body = {
            '#81c8be'
        }
    },
    ['Cattppuccin Frappe Teal rgb'] = {
        prefix = 'cat-frappe-Teal-rgb',
        body = {
            'rgb(129, 200, 190)'
        }
    },
    ['Cattppuccin Frappe Teal hsl'] = {
        prefix = 'cat-frappe-Teal-hsl',
        body = {
            'hsl(172, 39%, 65%)'
        }
    },
    ['Cattppuccin Frappe Sky Hex'] = {
        prefix = 'cat-frappe-Sky-hex',
        body = {
            '#99d1db'
        }
    },
    ['Cattppuccin Frappe Sky rgb'] = {
        prefix = 'cat-frappe-Sky-rgb',
        body = {
            'rgb(153, 209, 219)'
        }
    },
    ['Cattppuccin Frappe Sky hsl'] = {
        prefix = 'cat-frappe-Sky-hsl',
        body = {
            'hsl(189, 48%, 73%)'
        }
    },
    ['Cattppuccin Frappe Sapphire Hex'] = {
        prefix = 'cat-frappe-Sapphire-hex',
        body = {
            '#85c1dc'
        }
    },
    ['Cattppuccin Frappe Sapphire rgb'] = {
        prefix = 'cat-frappe-Sapphire-rgb',
        body = {
            'rgb(133, 193, 220)'
        }
    },
    ['Cattppuccin Frappe Sapphire hsl'] = {
        prefix = 'cat-frappe-Sapphire-hsl',
        body = {
            'hsl(199, 55%, 69%)'
        }
    },
    ['Cattppuccin Frappe Blue Hex'] = {
        prefix = 'cat-frappe-Blue-hex',
        body = {
            '#8caaee'
        }
    },
    ['Cattppuccin Frappe Blue rgb'] = {
        prefix = 'cat-frappe-Blue-rgb',
        body = {
            'rgb(140, 170, 238)'
        }
    },
    ['Cattppuccin Frappe Blue hsl'] = {
        prefix = 'cat-frappe-Blue-hsl',
        body = {
            'hsl(222, 74%, 74%)'
        }
    },
    ['Cattppuccin Frappe Lavender Hex'] = {
        prefix = 'cat-frappe-Lavender-hex',
        body = {
            '#babbf1'
        }
    },
    ['Cattppuccin Frappe Lavender rgb'] = {
        prefix = 'cat-frappe-Lavender-rgb',
        body = {
            'rgb(186, 187, 241)'
        }
    },
    ['Cattppuccin Frappe Lavender hsl'] = {
        prefix = 'cat-frappe-Lavender-hsl',
        body = {
            'hsl(239, 66%, 84%)'
        }
    },
    ['Cattppuccin Frappe Text Hex'] = {
        prefix = 'cat-frappe-Text-hex',
        body = {
            '#c6d0f5'
        }
    },
    ['Cattppuccin Frappe Text rgb'] = {
        prefix = 'cat-frappe-Text-rgb',
        body = {
            'rgb(198, 208, 245)'
        }
    },
    ['Cattppuccin Frappe Text hsl'] = {
        prefix = 'cat-frappe-Text-hsl',
        body = {
            'hsl(227, 70%, 87%)'
        }
    },
    ['Cattppuccin Frappe Subtext1 Hex'] = {
        prefix = 'cat-frappe-Subtext1-hex',
        body = {
            '#b5bfe2'
        }
    },
    ['Cattppuccin Frappe Subtext1 rgb'] = {
        prefix = 'cat-frappe-Subtext1-rgb',
        body = {
            'rgb(181, 191, 226)'
        }
    },
    ['Cattppuccin Frappe Subtext1 hsl'] = {
        prefix = 'cat-frappe-Subtext1-hsl',
        body = {
            'hsl(227, 44%, 80%)'
        }
    },
    ['Cattppuccin Frappe Subtext0 Hex'] = {
        prefix = 'cat-frappe-Subtext0-hex',
        body = {
            '#a5adce'
        }
    },
    ['Cattppuccin Frappe Subtext0 rgb'] = {
        prefix = 'cat-frappe-Subtext0-rgb',
        body = {
            'rgb(165, 173, 206)'
        }
    },
    ['Cattppuccin Frappe Subtext0 hsl'] = {
        prefix = 'cat-frappe-Subtext0-hsl',
        body = {
            'hsl(228, 29%, 73%)'
        }
    },
    ['Cattppuccin Frappe Overlay2 Hex'] = {
        prefix = 'cat-frappe-Overlay2-hex',
        body = {
            '#949cbb'
        }
    },
    ['Cattppuccin Frappe Overlay2 rgb'] = {
        prefix = 'cat-frappe-Overlay2-rgb',
        body = {
            'rgb(148, 156, 187)'
        }
    },
    ['Cattppuccin Frappe Overlay2 hsl'] = {
        prefix = 'cat-frappe-Overlay2-hsl',
        body = {
            'hsl(228, 22%, 66%)'
        }
    },
    ['Cattppuccin Frappe Overlay1 Hex'] = {
        prefix = 'cat-frappe-Overlay1-hex',
        body = {
            '#838ba7'
        }
    },
    ['Cattppuccin Frappe Overlay1 rgb'] = {
        prefix = 'cat-frappe-Overlay1-rgb',
        body = {
            'rgb(131, 139, 167)'
        }
    },
    ['Cattppuccin Frappe Overlay1 hsl'] = {
        prefix = 'cat-frappe-Overlay1-hsl',
        body = {
            'hsl(227, 17%, 58%)'
        }
    },
    ['Cattppuccin Frappe Overlay0 Hex'] = {
        prefix = 'cat-frappe-Overlay0-hex',
        body = {
            '#737994'
        }
    },
    ['Cattppuccin Frappe Overlay0 rgb'] = {
        prefix = 'cat-frappe-Overlay0-rgb',
        body = {
            'rgb(115, 121, 148)'
        }
    },
    ['Cattppuccin Frappe Overlay0 hsl'] = {
        prefix = 'cat-frappe-Overlay0-hsl',
        body = {
            'hsl(229, 13%, 52%)'
        }
    },
    ['Cattppuccin Frappe Surface2 Hex'] = {
        prefix = 'cat-frappe-Surface2-hex',
        body = {
            '#626880'
        }
    },
    ['Cattppuccin Frappe Surface2 rgb'] = {
        prefix = 'cat-frappe-Surface2-rgb',
        body = {
            'rgb(98, 104, 128)'
        }
    },
    ['Cattppuccin Frappe Surface2 hsl'] = {
        prefix = 'cat-frappe-Surface2-hsl',
        body = {
            'hsl(228, 13%, 44%)'
        }
    },
    ['Cattppuccin Frappe Surface1 Hex'] = {
        prefix = 'cat-frappe-Surface1-hex',
        body = {
            '#51576d'
        }
    },
    ['Cattppuccin Frappe Surface1 rgb'] = {
        prefix = 'cat-frappe-Surface1-rgb',
        body = {
            'rgb(81, 87, 109)'
        }
    },
    ['Cattppuccin Frappe Surface1 hsl'] = {
        prefix = 'cat-frappe-Surface1-hsl',
        body = {
            'hsl(227, 15%, 37%)'
        }
    },
    ['Cattppuccin Frappe Surface0 Hex'] = {
        prefix = 'cat-frappe-Surface0-hex',
        body = {
            '#414559'
        }
    },
    ['Cattppuccin Frappe Surface0 rgb'] = {
        prefix = 'cat-frappe-Surface0-rgb',
        body = {
            'rgb(65, 69, 89)'
        }
    },
    ['Cattppuccin Frappe Surface0 hsl'] = {
        prefix = 'cat-frappe-Surface0-hsl',
        body = {
            'hsl(230, 16%, 30%)'
        }
    },
    ['Cattppuccin Frappe Base Hex'] = {
        prefix = 'cat-frappe-Base-hex',
        body = {
            '#303446'
        }
    },
    ['Cattppuccin Frappe Base rgb'] = {
        prefix = 'cat-frappe-Base-rgb',
        body = {
            'rgb(48, 52, 70)'
        }
    },
    ['Cattppuccin Frappe Base hsl'] = {
        prefix = 'cat-frappe-Base-hsl',
        body = {
            'hsl(229, 19%, 23%)'
        }
    },
    ['Cattppuccin Frappe Mantle Hex'] = {
        prefix = 'cat-frappe-Mantle-hex',
        body = {
            '#292c3c'
        }
    },
    ['Cattppuccin Frappe Mantle rgb'] = {
        prefix = 'cat-frappe-Mantle-rgb',
        body = {
            'rgb(41, 44, 60)'
        }
    },
    ['Cattppuccin Frappe Mantle hsl'] = {
        prefix = 'cat-frappe-Mantle-hsl',
        body = {
            'hsl(231, 19%, 20%)'
        }
    },
    ['Cattppuccin Frappe Crust Hex'] = {
        prefix = 'cat-frappe-Crust-hex',
        body = {
            '#232634'
        }
    },
    ['Cattppuccin Frappe Crust rgb'] = {
        prefix = 'cat-frappe-Crust-rgb',
        body = {
            'rgb(35, 38, 52)'
        }
    },
    ['Cattppuccin Frappe Crust hsl'] = {
        prefix = 'cat-frappe-Crust-hsl',
        body = {
            'hsl(229, 20%, 17%)'
        }
    },
    ['Catppuccin Latte Rosewater hex'] = {
        prefix = 'cat-latte-Rosewater-hex',
        body = {
            '#dc8a78'
        }
    },
    ['Catppuccin Latte Rosewater rgb'] = {
        prefix = 'cat-latte-Rosewater-rgb',
        body = {
            'rgb(220, 138, 120)'
        }
    },
    ['Catppuccin Latte Rosewater hsl'] = {
        prefix = 'cat-latte-Rosewater-hsl',
        body = {
            'hsl(11, 59%, 67%)'
        }
    },
    ['Catppuccin Latte Flamingo hex'] = {
        prefix = 'cat-latte-Flamingo-hex',
        body = {
            '#dd7878'
        }
    },
    ['Catppuccin Latte Flamingo rgb'] = {
        prefix = 'cat-latte-Flamingo-rgb',
        body = {
            'rgb(221, 120, 120)'
        }
    },
    ['Catppuccin Latte Flamingo hsl'] = {
        prefix = 'cat-latte-Flamingo-hsl',
        body = {
            'hsl(0, 60%, 67%)'
        }
    },
    ['Catppuccin Latte Pink hex'] = {
        prefix = 'cat-latte-pinkk-hex',
        body = {
            '#ea76cb'
        }
    },
    ['Catppuccin Latte Pink hex'] = {
        prefix = 'cat-latte-pink-rgb',
        body = {
            'rgb(234, 118, 203)'
        }
    },
    ['Catppuccin Latte Pink hex'] = {
        prefix = 'cat-latte-pink-hsl',
        body = {
            'hsl(316, 73%, 69%)'
        }
    },
    ['Catppuccin Latte Mauve hex'] = {
        prefix = 'cat-latte-Mauve-hex',
        body = {
            '#8839ef'
        }
    },
    ['Catppuccin Latte Mauve rgb'] = {
        prefix = 'cat-latte-Mauve-rgb',
        body = {
            'rgb(136, 57, 239)'
        }
    },
    ['Catppuccin Latte Mauve hsl'] = {
        prefix = 'cat-latte-Mauve-hsl',
        body = {
            'hsl(266, 85%, 58%)'
        }
    },
    ['Catppuccin Latte Red hex'] = {
        prefix = 'cat-latte-Red-hex',
        body = {
            '#d20f39'
        }
    },
    ['Catppuccin Latte Red rgb'] = {
        prefix = 'cat-latte-Red-rgb',
        body = {
            'rgb(210, 15, 57)'
        }
    },
    ['Catppuccin Latte Red hsl'] = {
        prefix = 'cat-latte-Red-hsl',
        body = {
            'hsl(347, 87%, 44%)'
        }
    },
    ['Catppuccin Latte Maroon hex'] = {
        prefix = 'cat-latte-Maroon-hex',
        body = {
            '#e64553'
        }
    },
    ['Catppuccin Latte Maroon rgb'] = {
        prefix = 'cat-latte-Maroon-rgb',
        body = {
            'rgb(230, 69, 83)'
        }
    },
    ['Catppuccin Latte Maroon hsl'] = {
        prefix = 'cat-latte-Maroon-hsl',
        body = {
            'hsl(355, 76%, 59%)'
        }
    },
    ['Catppuccin Latte Peach hex'] = {
        prefix = 'cat-latte-Peach-hex',
        body = {
            '#fe640b'
        }
    },
    ['Catppuccin Latte Peach rgb'] = {
        prefix = 'cat-latte-Peach-rgb',
        body = {
            'rgb(254, 100, 11)'
        }
    },
    ['Catppuccin Latte Peach hsl'] = {
        prefix = 'cat-latte-Peach-hsl',
        body = {
            'hsl(22, 99%, 52%)'
        }
    },
    ['Catppuccin Latte Yellow hex'] = {
        prefix = 'cat-latte-Yellow-hex',
        body = {
            '#df8e1d'
        }
    },
    ['Catppuccin Latte Yellow rgb'] = {
        prefix = 'cat-latte-Yellow-rgb',
        body = {
            'rgb(223, 142, 29)'
        }
    },
    ['Catppuccin Latte Yellow hsl'] = {
        prefix = 'cat-latte-Yellow-hsl',
        body = {
            'hsl(35, 77%, 49%)'
        }
    },
    ['Catppuccin Latte Green hex'] = {
        prefix = 'cat-latte-Green-hex',
        body = {
            '#40a02b'
        }
    },
    ['Catppuccin Latte Green rgb'] = {
        prefix = 'cat-latte-Green-rgb',
        body = {
            'rgb(64, 160, 43)'
        }
    },
    ['Catppuccin Latte Green hsl'] = {
        prefix = 'cat-latte-Green-hsl',
        body = {
            'hsl(109, 58%, 40%)'
        }
    },
    ['Catppuccin Latte Teal hex'] = {
        prefix = 'cat-latte-Teal-hex',
        body = {
            '#179299'
        }
    },
    ['Catppuccin Latte Teal rgb'] = {
        prefix = 'cat-latte-Teal-rgb',
        body = {
            'rgb(23, 146, 153)'
        }
    },
    ['Catppuccin Latte Teal hsl'] = {
        prefix = 'cat-latte-Teal-hsl',
        body = {
            'hsl(183, 74%, 35%)'
        }
    },
    ['Catppuccin Latte Sky hex'] = {
        prefix = 'cat-latte-Sky-hex',
        body = {
            '#04a5e5'
        }
    },
    ['Catppuccin Latte Sky rgb'] = {
        prefix = 'cat-latte-Sky-rgb',
        body = {
            'rgb(4, 165, 229)'
        }
    },
    ['Catppuccin Latte Sky hsl'] = {
        prefix = 'cat-latte-Sky-hsl',
        body = {
            'hsl(197, 97%, 46%)'
        }
    },
    ['Catppuccin Latte Sapphire hex'] = {
        prefix = 'cat-latte-Sapphire-hex',
        body = {
            '#209fb5'
        }
    },
    ['Catppuccin Latte Sapphire rgb'] = {
        prefix = 'cat-latte-Sapphire-rgb',
        body = {
            'rgb(32, 159, 181)'
        }
    },
    ['Catppuccin Latte Sapphire hsl'] = {
        prefix = 'cat-latte-Sapphire-hsl',
        body = {
            'hsl(189, 70%, 42%)'
        }
    },
    ['Catppuccin Latte Blue hex'] = {
        prefix = 'cat-latte-Blue-hex',
        body = {
            '#1e66f5'
        }
    },
    ['Catppuccin Latte Blue rgb'] = {
        prefix = 'cat-latte-Blue-rgb',
        body = {
            'rgb(30, 102, 245)'
        }
    },
    ['Catppuccin Latte Blue hsl'] = {
        prefix = 'cat-latte-Blue-hsl',
        body = {
            'hsl(220, 91%, 54%)'
        }
    },
    ['Catppuccin Latte Lavender hex'] = {
        prefix = 'cat-latte-Lavender-hex',
        body = {
            '#7287fd'
        }
    },
    ['Catppuccin Latte Lavender rgb'] = {
        prefix = 'cat-latte-Lavender-rgb',
        body = {
            'rgb(114, 135, 253)'
        }
    },
    ['Catppuccin Latte Lavender hsl'] = {
        prefix = 'cat-latte-Lavender-hsl',
        body = {
            'hsl(231, 97%, 72%)'
        }
    },
    ['Catppuccin Latte Text hex'] = {
        prefix = 'cat-latte-Text-hex',
        body = {
            '#4c4f69'
        }
    },
    ['Catppuccin Latte Text rgb'] = {
        prefix = 'cat-latte-Text-rgb',
        body = {
            'rgb(76, 79, 105)'
        }
    },
    ['Catppuccin Latte Text hsl'] = {
        prefix = 'cat-latte-Text-hsl',
        body = {
            'hsl(234, 16%, 35%)'
        }
    },
    ['Catppuccin Latte Subtext1 hex'] = {
        prefix = 'cat-latte-Subtext1-hex',
        body = {
            '#5c5f77'
        }
    },
    ['Catppuccin Latte Subtext1 rgb'] = {
        prefix = 'cat-latte-Subtext1-rgb',
        body = {
            'rgb(92, 95, 119)'
        }
    },
    ['Catppuccin Latte Subtext1 hsl'] = {
        prefix = 'cat-latte-Subtext1-hsl',
        body = {
            'hsl(233, 13%, 41%)'
        }
    },
    ['Catppuccin Latte Subtext0 hex'] = {
        prefix = 'cat-latte-Subtext0-hex',
        body = {
            '#6c6f85'
        }
    },
    ['Catppuccin Latte Subtext0 rgb'] = {
        prefix = 'cat-latte-Subtext0-rgb',
        body = {
            'rgb(108, 111, 133)'
        }
    },
    ['Catppuccin Latte Subtext0 hsl'] = {
        prefix = 'cat-latte-Subtext0-hsl',
        body = {
            'hsl(233, 10%, 47%)'
        }
    },
    ['Catppuccin Latte Overlay2 hex'] = {
        prefix = 'cat-latte-Overlay2-hex',
        body = {
            '#7c7f93'
        }
    },
    ['Catppuccin Latte Overlay2 rgb'] = {
        prefix = 'cat-latte-Overlay2-rgb',
        body = {
            'rgb(124, 127, 147)'
        }
    },
    ['Catppuccin Latte Overlay2 hsl'] = {
        prefix = 'cat-latte-Overlay2-hsl',
        body = {
            'hsl(232, 10%, 53%)'
        }
    },
    ['Catppuccin Latte Overlay1 hex'] = {
        prefix = 'cat-latte-Overlay1-hex',
        body = {
            '#8c8fa1'
        }
    },
    ['Catppuccin Latte Overlay1 rgb'] = {
        prefix = 'cat-latte-Overlay1-rgb',
        body = {
            'rgb(140, 143, 161)'
        }
    },
    ['Catppuccin Latte Overlay1 hsl'] = {
        prefix = 'cat-latte-Overlay1-hsl',
        body = {
            'hsl(231, 10%, 59%)'
        }
    },
    ['Catppuccin Latte Overlay0 hex'] = {
        prefix = 'cat-latte-Overlay0-hex',
        body = {
            '#9ca0b0'
        }
    },
    ['Catppuccin Latte Overlay0 rgb'] = {
        prefix = 'cat-latte-Overlay0-rgb',
        body = {
            'rgb(156, 160, 176)'
        }
    },
    ['Catppuccin Latte Overlay0 hsl'] = {
        prefix = 'cat-latte-Overlay0-hsl',
        body = {
            'hsl(228, 11%, 65%)'
        }
    },
    ['Catppuccin Latte Surface2 hex'] = {
        prefix = 'cat-latte-Surface2-hex',
        body = {
            '#acb0be'
        }
    },
    ['Catppuccin Latte Surface2 rgb'] = {
        prefix = 'cat-latte-Surface2-rgb',
        body = {
            'rgb(172, 176, 190)'
        }
    },
    ['Catppuccin Latte Surface2 hsl'] = {
        prefix = 'cat-latte-Surface2-hsl',
        body = {
            'hsl(227, 12%, 71%)'
        }
    },
    ['Catppuccin Latte Surface1 hex'] = {
        prefix = 'cat-latte-Surface1-hex',
        body = {
            '#bcc0cc'
        }
    },
    ['Catppuccin Latte Surface1 rgb'] = {
        prefix = 'cat-latte-Surface1-rgb',
        body = {
            'rgb(188, 192, 204)'
        }
    },
    ['Catppuccin Latte Surface1 hsl'] = {
        prefix = 'cat-latte-Surface1-hsl',
        body = {
            'hsl(225, 14%, 77%)'
        }
    },
    ['Catppuccin Latte Surface0 hex'] = {
        prefix = 'cat-latte-Surface0-hex',
        body = {
            '#ccd0da'
        }
    },
    ['Catppuccin Latte Surface0 rgb'] = {
        prefix = 'cat-latte-Surface0-rgb',
        body = {
            'rgb(204, 208, 218)'
        }
    },
    ['Catppuccin Latte Surface0 hsl'] = {
        prefix = 'cat-latte-Surface0-hsl',
        body = {
            'hsl(223, 16%, 83%)'
        }
    },
    ['Catppuccin Latte Base hex'] = {
        prefix = 'cat-latte-Base-hex',
        body = {
            '#eff1f5'
        }
    },
    ['Catppuccin Latte Base rgb'] = {
        prefix = 'cat-latte-Base-rgb',
        body = {
            'rgb(239, 241, 245)'
        }
    },
    ['Catppuccin Latte Base hsl'] = {
        prefix = 'cat-latte-Base-hsl',
        body = {
            'hsl(220, 23%, 95%)'
        }
    },
    ['Catppuccin Latte Mantle hex'] = {
        prefix = 'cat-latte-Mantle-hex',
        body = {
            '#e6e9ef'
        }
    },
    ['Catppuccin Latte Mantle rgb'] = {
        prefix = 'cat-latte-Mantle-rgb',
        body = {
            'rgb(230, 233, 239)'
        }
    },
    ['Catppuccin Latte Mantle hsl'] = {
        prefix = 'cat-latte-Mantle-hsl',
        body = {
            'hsl(220, 22%, 92%)'
        }
    },
    ['Catppuccin Latte Crust hex'] = {
        prefix = 'cat-latte-Crust-hex',
        body = {
            '#dce0e8'
        }
    },
    ['Catppuccin Latte Crust rgb'] = {
        prefix = 'cat-latte-Crust-rgb',
        body = {
            'rgb(220, 224, 232)'
        }
    },
    ['Catppuccin Latte Crust hsl'] = {
        prefix = 'cat-latte-Crust-hsl',
        body = {
            'hsl(220, 21%, 89%)'
        }
    },
    ['Catppuccin Mocha Rosewater hex'] = {
        prefix = 'cat-mocha-Rosewater-hex',
        body = {
            '#f5e0dc'
        }
    },
    ['Catppuccin Mocha Rosewater rgb'] = {
        prefix = 'cat-mocha-Rosewater-rgb',
        body = {
            'rgb(245, 224, 220)'
        }
    },
    ['Catppuccin Mocha Rosewater hsl'] = {
        prefix = 'cat-mocha-Rosewater-hsl',
        body = {
            'hsl(10, 56%, 91%)'
        }
    },
    ['Catppuccin Mocha Flamingo hex'] = {
        prefix = 'cat-mocha-Flamingo-hex',
        body = {
            '#f2cdcd'
        }
    },
    ['Catppuccin Mocha Flamingo rgb'] = {
        prefix = 'cat-mocha-Flamingo-rgb',
        body = {
            'rgb(242, 205, 205)'
        }
    },
    ['Catppuccin Mocha Flamingo hsl'] = {
        prefix = 'cat-mocha-Flamingo-hsl',
        body = {
            'hsl(0, 59%, 88%)'
        }
    },
    ['Catppuccin Mocha Pink hex'] = {
        prefix = 'cat-mocha-Pink-hex',
        body = {
            '#f5c2e7'
        }
    },
    ['Catppuccin Mocha Pink rgb'] = {
        prefix = 'cat-mocha-Pink-rgb',
        body = {
            'rgb(245, 194, 231)'
        }
    },
    ['Catppuccin Mocha Pink hsl'] = {
        prefix = 'cat-mocha-Pink-hsl',
        body = {
            'hsl(316, 72%, 86%)'
        }
    },
    ['Catppuccin Mocha Mauve hex'] = {
        prefix = 'cat-mocha-Mauve-hex',
        body = {
            '#cba6f7'
        }
    },
    ['Catppuccin Mocha Mauve rgb'] = {
        prefix = 'cat-mocha-Mauve-rgb',
        body = {
            'rgb(203, 166, 247)'
        }
    },
    ['Catppuccin Mocha Mauve hsl'] = {
        prefix = 'cat-mocha-Mauve-hsl',
        body = {
            'hsl(267, 84%, 81%)'
        }
    },
    ['Catppuccin Mocha Red hex'] = {
        prefix = 'cat-mocha-Red-hex',
        body = {
            '#f38ba8'
        }
    },
    ['Catppuccin Mocha Red rgb'] = {
        prefix = 'cat-mocha-Red-rgb',
        body = {
            'rgb(243, 139, 168)'
        }
    },
    ['Catppuccin Mocha Red hsl'] = {
        prefix = 'cat-mocha-Red-hsl',
        body = {
            'hsl(343, 81%, 75%)'
        }
    },
    ['Catppuccin Mocha Maroon hex'] = {
        prefix = 'cat-mocha-Maroon-hex',
        body = {
            '#eba0ac'
        }
    },
    ['Catppuccin Mocha Maroon rgb'] = {
        prefix = 'cat-mocha-Maroon-rgb',
        body = {
            'rgb(235, 160, 172)'
        }
    },
    ['Catppuccin Mocha Maroon hsl'] = {
        prefix = 'cat-mocha-Maroon-hsl',
        body = {
            'hsl(350, 65%, 77%)'
        }
    },
    ['Catppuccin Mocha Peach hex'] = {
        prefix = 'cat-mocha-Peach-hex',
        body = {
            '#fab387'
        }
    },
    ['Catppuccin Mocha Peach rgb'] = {
        prefix = 'cat-mocha-Peach-rgb',
        body = {
            'rgb(250, 179, 135)'
        }
    },
    ['Catppuccin Mocha Peach hsl'] = {
        prefix = 'cat-mocha-Peach-hsl',
        body = {
            'hsl(23, 92%, 75%)'
        }
    },
    ['Catppuccin Mocha Yellow hex'] = {
        prefix = 'cat-mocha-Yellow-hex',
        body = {
            '#f9e2af'
        }
    },
    ['Catppuccin Mocha Yellow rgb'] = {
        prefix = 'cat-mocha-Yellow-rgb',
        body = {
            'rgb(249, 226, 175)'
        }
    },
    ['Catppuccin Mocha Yellow hsl'] = {
        prefix = 'cat-mocha-Yellow-hsl',
        body = {
            'hsl(41, 86%, 83%)'
        }
    },
    ['Catppuccin Mocha Green hex'] = {
        prefix = 'cat-mocha-Green-hex',
        body = {
            '#a6e3a1'
        }
    },
    ['Catppuccin Mocha Green rgb'] = {
        prefix = 'cat-mocha-Green-rgb',
        body = {
            'rgb(166, 227, 161)'
        }
    },
    ['Catppuccin Mocha Green hsl'] = {
        prefix = 'cat-mocha-Green-hsl',
        body = {
            'hsl(115, 54%, 76%)'
        }
    },
    ['Catppuccin Mocha Teal hex'] = {
        prefix = 'cat-mocha-Teal-hex',
        body = {
            '#94e2d5'
        }
    },
    ['Catppuccin Mocha Teal rgb'] = {
        prefix = 'cat-mocha-Teal-rgb',
        body = {
            'rgb(148, 226, 213)'
        }
    },
    ['Catppuccin Mocha Teal hsl'] = {
        prefix = 'cat-mocha-Teal-hsl',
        body = {
            'hsl(170, 57%, 73%)'
        }
    },
    ['Catppuccin Mocha Sky hex'] = {
        prefix = 'cat-mocha-Sky-hex',
        body = {
            '#89dceb'
        }
    },
    ['Catppuccin Mocha Sky rgb'] = {
        prefix = 'cat-mocha-Sky-rgb',
        body = {
            'rgb(137, 220, 235)'
        }
    },
    ['Catppuccin Mocha Sky hsl'] = {
        prefix = 'cat-mocha-Sky-hsl',
        body = {
            'hsl(189, 71%, 73%)'
        }
    },
    ['Catppuccin Mocha Sapphire hex'] = {
        prefix = 'cat-mocha-Sapphire-hex',
        body = {
            '#74c7ec'
        }
    },
    ['Catppuccin Mocha Sapphire rgb'] = {
        prefix = 'cat-mocha-Sapphire-rgb',
        body = {
            'rgb(116, 199, 236)'
        }
    },
    ['Catppuccin Mocha Sapphire hsl'] = {
        prefix = 'cat-mocha-Sapphire-hsl',
        body = {
            'hsl(199, 76%, 69%)'
        }
    },
    ['Catppuccin Mocha Blue hex'] = {
        prefix = 'cat-mocha-Blue-hex',
        body = {
            '#89b4fa'
        }
    },
    ['Catppuccin Mocha Blue rgb'] = {
        prefix = 'cat-mocha-Blue-rgb',
        body = {
            'rgb(137, 180, 250)'
        }
    },
    ['Catppuccin Mocha Blue hsl'] = {
        prefix = 'cat-mocha-Blue-hsl',
        body = {
            'hsl(217, 92%, 76%)'
        }
    },
    ['Catppuccin Mocha Lavender hex'] = {
        prefix = 'cat-mocha-Lavender-hex',
        body = {
            '#b4befe'
        }
    },
    ['Catppuccin Mocha Lavender rgb'] = {
        prefix = 'cat-mocha-Lavender-rgb',
        body = {
            'rgb(180, 190, 254)'
        }
    },
    ['Catppuccin Mocha Lavender hsl'] = {
        prefix = 'cat-mocha-Lavender-hsl',
        body = {
            'hsl(232, 97%, 85%)'
        }
    },
    ['Catppuccin Mocha Text hex'] = {
        prefix = 'cat-mocha-Text-hex',
        body = {
            '#cdd6f4'
        }
    },
    ['Catppuccin Mocha Text rgb'] = {
        prefix = 'cat-mocha-Text-rgb',
        body = {
            'rgb(205, 214, 244)'
        }
    },
    ['Catppuccin Mocha Text hsl'] = {
        prefix = 'cat-mocha-Text-hsl',
        body = {
            'hsl(226, 64%, 88%)'
        }
    },
    ['Catppuccin Mocha Subtext1 hex'] = {
        prefix = 'cat-mocha-Subtext1-hex',
        body = {
            '#bac2de'
        }
    },
    ['Catppuccin Mocha Subtext1 rgb'] = {
        prefix = 'cat-mocha-Subtext1-rgb',
        body = {
            'rgb(186, 194, 222)'
        }
    },
    ['Catppuccin Mocha Subtext1 hsl'] = {
        prefix = 'cat-mocha-Subtext1-hsl',
        body = {
            'hsl(227, 35%, 80%)'
        }
    },
    ['Catppuccin Mocha Subtext0 hex'] = {
        prefix = 'cat-mocha-Subtext0-hex',
        body = {
            '#a6adc8'
        }
    },
    ['Catppuccin Mocha Subtext0 rgb'] = {
        prefix = 'cat-mocha-Subtext0-rgb',
        body = {
            'rgb(166, 173, 200)'
        }
    },
    ['Catppuccin Mocha Subtext0 hsl'] = {
        prefix = 'cat-mocha-Subtext0-hsl',
        body = {
            'hsl(228, 24%, 72%)'
        }
    },
    ['Catppuccin Mocha Overlay2 hex'] = {
        prefix = 'cat-mocha-Overlay2-hex',
        body = {
            '#9399b2'
        }
    },
    ['Catppuccin Mocha Overlay2 rgb'] = {
        prefix = 'cat-mocha-Overlay2-rgb',
        body = {
            'rgb(147, 153, 178)'
        }
    },
    ['Catppuccin Mocha Overlay2 hsl'] = {
        prefix = 'cat-mocha-Overlay2-hsl',
        body = {
            'hsl(228, 17%, 64%)'
        }
    },
    ['Catppuccin Mocha Overlay1 hex'] = {
        prefix = 'cat-mocha-Overlay1-hex',
        body = {
            '#7f849c'
        }
    },
    ['Catppuccin Mocha Overlay1 rgb'] = {
        prefix = 'cat-mocha-Overlay1-rgb',
        body = {
            'rgb(127, 132, 156)'
        }
    },
    ['Catppuccin Mocha Overlay1 hsl'] = {
        prefix = 'cat-mocha-Overlay1-hsl',
        body = {
            'hsl(230, 13%, 55%)'
        }
    },
    ['Catppuccin Mocha Overlay0 hex'] = {
        prefix = 'cat-mocha-Overlay0-hex',
        body = {
            '#6c7086'
        }
    },
    ['Catppuccin Mocha Overlay0 rgb'] = {
        prefix = 'cat-mocha-Overlay0-rgb',
        body = {
            'rgb(108, 112, 134)'
        }
    },
    ['Catppuccin Mocha Overlay0 hsl'] = {
        prefix = 'cat-mocha-Overlay0-hsl',
        body = {
            'hsl(231, 11%, 47%)'
        }
    },
    ['Catppuccin Mocha Surface2 hex'] = {
        prefix = 'cat-mocha-Surface2-hex',
        body = {
            '#585b70'
        }
    },
    ['Catppuccin Mocha Surface2 rgb'] = {
        prefix = 'cat-mocha-Surface2-rgb',
        body = {
            'rgb(88, 91, 112)'
        }
    },
    ['Catppuccin Mocha Surface2 hsl'] = {
        prefix = 'cat-mocha-Surface2-hsl',
        body = {
            'hsl(233, 12%, 39%)'
        }
    },
    ['Catppuccin Mocha Surface1 hex'] = {
        prefix = 'cat-mocha-Surface1-hex',
        body = {
            '#45475a'
        }
    },
    ['Catppuccin Mocha Surface1 rgb'] = {
        prefix = 'cat-mocha-Surface1-rgb',
        body = {
            'rgb(69, 71, 90)'
        }
    },
    ['Catppuccin Mocha Surface1 hsl'] = {
        prefix = 'cat-mocha-Surface1-hsl',
        body = {
            'hsl(234, 13%, 31%)'
        }
    },
    ['Catppuccin Mocha Surface0 hex'] = {
        prefix = 'cat-mocha-Surface0-hex',
        body = {
            '#313244'
        }
    },
    ['Catppuccin Mocha Surface0 rgb'] = {
        prefix = 'cat-mocha-Surface0-rgb',
        body = {
            'rgb(49, 50, 68)'
        }
    },
    ['Catppuccin Mocha Surface0 hsl'] = {
        prefix = 'cat-mocha-Surface0-hsl',
        body = {
            'hsl(237, 16%, 23%)'
        }
    },
    ['Catppuccin Mocha Base hex'] = {
        prefix = 'cat-mocha-Base-hex',
        body = {
            '#1e1e2e'
        }
    },
    ['Catppuccin Mocha Base rgb'] = {
        prefix = 'cat-mocha-Base-rgb',
        body = {
            'rgb(30, 30, 46)'
        }
    },
    ['Catppuccin Mocha Base hsl'] = {
        prefix = 'cat-mocha-Base-hsl',
        body = {
            'hsl(240, 21%, 15%)'
        }
    },
    ['Catppuccin Mocha Mantle hex'] = {
        prefix = 'cat-mocha-Mantle-hex',
        body = {
            '#181825'
        }
    },
    ['Catppuccin Mocha Mantle rgb'] = {
        prefix = 'cat-mocha-Mantle-rgb',
        body = {
            'rgb(24, 24, 37)'
        }
    },
    ['Catppuccin Mocha Mantle hsl'] = {
        prefix = 'cat-mocha-Mantle-hsl',
        body = {
            'hsl(240, 21%, 12%)'
        }
    },
    ['Catppuccin Mocha Crust hex'] = {
        prefix = 'cat-mocha-Crust-hex',
        body = {
            '#11111b'
        }
    },
    ['Catppuccin Mocha Crust rgb'] = {
        prefix = 'cat-mocha-Crust-rgb',
        body = {
            'rgb(17, 17, 27)'
        }
    },
    ['Catppuccin Mocha Crust hsl'] = {
        prefix = 'cat-mocha-Crust-hsl',
        body = {
            'hsl(240, 23%, 9%)'
        }
    },
    ['Catppuccin Macchiato Rosewater hex'] = {
        prefix = 'cat-macchiato-Rosewater-hex',
        body = {
            '#f4dbd6'
        }
    },
    ['Catppuccin Macchiato Rosewater rgb'] = {
        prefix = 'cat-macchiato-Rosewater-rgb',
        body = {
            'rgb(244, 219, 214)'
        }
    },
    ['Catppuccin Macchiato Rosewater hsl'] = {
        prefix = 'cat-macchiato-Rosewater-hsl',
        body = {
            'hsl(10, 58%, 90%)'
        }
    },
    ['Catppuccin Macchiato Flamingo hex'] = {
        prefix = 'cat-macchiato-Flamingo-hex',
        body = {
            '#f0c6c6'
        }
    },
    ['Catppuccin Macchiato Flamingo rgb'] = {
        prefix = 'cat-macchiato-Flamingo-rgb',
        body = {
            'rgb(240, 198, 198)'
        }
    },
    ['Catppuccin Macchiato Flamingo hsl'] = {
        prefix = 'cat-macchiato-Flamingo-hsl',
        body = {
            'hsl(0, 58%, 86%)'
        }
    },
    ['Catppuccin Macchiato Pink hex'] = {
        prefix = 'cat-macchiato-Pink-hex',
        body = {
            '#f5bde6'
        }
    },
    ['Catppuccin Macchiato Pink rgb'] = {
        prefix = 'cat-macchiato-Pink-rgb',
        body = {
            'rgb(245, 189, 230)'
        }
    },
    ['Catppuccin Macchiato Pink hsl'] = {
        prefix = 'cat-macchiato-Pink-hsl',
        body = {
            'hsl(316, 74%, 85%)'
        }
    },
    ['Catppuccin Macchiato Mauve hex'] = {
        prefix = 'cat-macchiato-Mauve-hex',
        body = {
            '#c6a0f6'
        }
    },
    ['Catppuccin Macchiato Mauve rgb'] = {
        prefix = 'cat-macchiato-Mauve-rgb',
        body = {
            'rgb(198, 160, 246)'
        }
    },
    ['Catppuccin Macchiato Mauve hsl'] = {
        prefix = 'cat-macchiato-Mauve-hsl',
        body = {
            'hsl(267, 83%, 80%)'
        }
    },
    ['Catppuccin Macchiato Red hex'] = {
        prefix = 'cat-macchiato-Red-hex',
        body = {
            '#ed8796'
        }
    },
    ['Catppuccin Macchiato Red rgb'] = {
        prefix = 'cat-macchiato-Red-rgb',
        body = {
            'rgb(237, 135, 150)'
        }
    },
    ['Catppuccin Macchiato Red hsl'] = {
        prefix = 'cat-macchiato-Red-hsl',
        body = {
            'hsl(351, 74%, 73%)'
        }
    },
    ['Catppuccin Macchiato Maroon hex'] = {
        prefix = 'cat-macchiato-Maroon-hex',
        body = {
            '#ee99a0'
        }
    },
    ['Catppuccin Macchiato Maroon rgb'] = {
        prefix = 'cat-macchiato-Maroon-rgb',
        body = {
            'rgb(238, 153, 160)'
        }
    },
    ['Catppuccin Macchiato Maroon hsl'] = {
        prefix = 'cat-macchiato-Maroon-hsl',
        body = {
            'hsl(355, 71%, 77%)'
        }
    },
    ['Catppuccin Macchiato Peach hex'] = {
        prefix = 'cat-macchiato-Peach-hex',
        body = {
            '#f5a97f'
        }
    },
    ['Catppuccin Macchiato Peach rgb'] = {
        prefix = 'cat-macchiato-Peach-rgb',
        body = {
            'rgb(245, 169, 127)'
        }
    },
    ['Catppuccin Macchiato Peach hsl'] = {
        prefix = 'cat-macchiato-Peach-hsl',
        body = {
            'hsl(21, 86%, 73%)'
        }
    },
    ['Catppuccin Macchiato Yellow hex'] = {
        prefix = 'cat-macchiato-Yellow-hex',
        body = {
            '#eed49f'
        }
    },
    ['Catppuccin Macchiato Yellow rgb'] = {
        prefix = 'cat-macchiato-Yellow-rgb',
        body = {
            'rgb(238, 212, 159)'
        }
    },
    ['Catppuccin Macchiato Yellow hsl'] = {
        prefix = 'cat-macchiato-Yellow-hsl',
        body = {
            'hsl(40, 70%, 78%)'
        }
    },
    ['Catppuccin Macchiato Green hex'] = {
        prefix = 'cat-macchiato-Green-hex',
        body = {
            '#a6da95'
        }
    },
    ['Catppuccin Macchiato Green rgb'] = {
        prefix = 'cat-macchiato-Green-rgb',
        body = {
            'rgb(166, 218, 149)'
        }
    },
    ['Catppuccin Macchiato Green hsl'] = {
        prefix = 'cat-macchiato-Green-hsl',
        body = {
            'hsl(105, 48%, 72%)'
        }
    },
    ['Catppuccin Macchiato Teal hex'] = {
        prefix = 'cat-macchiato-Teal-hex',
        body = {
            '#8bd5ca'
        }
    },
    ['Catppuccin Macchiato Teal rgb'] = {
        prefix = 'cat-macchiato-Teal-rgb',
        body = {
            'rgb(139, 213, 202)'
        }
    },
    ['Catppuccin Macchiato Teal hsl'] = {
        prefix = 'cat-macchiato-Teal-hsl',
        body = {
            'hsl(171, 47%, 69%)'
        }
    },
    ['Catppuccin Macchiato Sky hex'] = {
        prefix = 'cat-macchiato-Sky-hex',
        body = {
            '#91d7e3'
        }
    },
    ['Catppuccin Macchiato Sky rgb'] = {
        prefix = 'cat-macchiato-Sky-rgb',
        body = {
            'rgb(145, 215, 227)'
        }
    },
    ['Catppuccin Macchiato Sky hsl'] = {
        prefix = 'cat-macchiato-Sky-hsl',
        body = {
            'hsl(189, 59%, 73%)'
        }
    },
    ['Catppuccin Macchiato Sapphire hex'] = {
        prefix = 'cat-macchiato-Sapphire-hex',
        body = {
            '#7dc4e4'
        }
    },
    ['Catppuccin Macchiato Sapphire rgb'] = {
        prefix = 'cat-macchiato-Sapphire-rgb',
        body = {
            'rgb(125, 196, 228)'
        }
    },
    ['Catppuccin Macchiato Sapphire hsl'] = {
        prefix = 'cat-macchiato-Sapphire-hsl',
        body = {
            'hsl(199, 66%, 69%)'
        }
    },
    ['Catppuccin Macchiato Blue hex'] = {
        prefix = 'cat-macchiato-Blue-hex',
        body = {
            '#8aadf4'
        }
    },
    ['Catppuccin Macchiato Blue rgb'] = {
        prefix = 'cat-macchiato-Blue-rgb',
        body = {
            'rgb(138, 173, 244)'
        }
    },
    ['Catppuccin Macchiato Blue hsl'] = {
        prefix = 'cat-macchiato-Blue-hsl',
        body = {
            'hsl(220, 83%, 75%)'
        }
    },
    ['Catppuccin Macchiato Lavender hex'] = {
        prefix = 'cat-macchiato-Lavender-hex',
        body = {
            '#b7bdf8'
        }
    },
    ['Catppuccin Macchiato Lavender rgb'] = {
        prefix = 'cat-macchiato-Lavender-rgb',
        body = {
            'rgb(183, 189, 248)'
        }
    },
    ['Catppuccin Macchiato Lavender hsl'] = {
        prefix = 'cat-macchiato-Lavender-hsl',
        body = {
            'hsl(234, 82%, 85%)'
        }
    },
    ['Catppuccin Macchiato Text hex'] = {
        prefix = 'cat-macchiato-Text-hex',
        body = {
            '#cad3f5'
        }
    },
    ['Catppuccin Macchiato Text rgb'] = {
        prefix = 'cat-macchiato-Text-rgb',
        body = {
            'rgb(202, 211, 245)'
        }
    },
    ['Catppuccin Macchiato Text hsl'] = {
        prefix = 'cat-macchiato-Text-hsl',
        body = {
            'hsl(227, 68%, 88%)'
        }
    },
    ['Catppuccin Macchiato Subtext1 hex'] = {
        prefix = 'cat-macchiato-Subtext1-hex',
        body = {
            '#b8c0e0'
        }
    },
    ['Catppuccin Macchiato Subtext1 rgb'] = {
        prefix = 'cat-macchiato-Subtext1-rgb',
        body = {
            'rgb(184, 192, 224)'
        }
    },
    ['Catppuccin Macchiato Subtext1 hsl'] = {
        prefix = 'cat-macchiato-Subtext1-hsl',
        body = {
            'hsl(228, 39%, 80%)'
        }
    },
    ['Catppuccin Macchiato Subtext0 hex'] = {
        prefix = 'cat-macchiato-Subtext0-hex',
        body = {
            '#a5adcb'
        }
    },
    ['Catppuccin Macchiato Subtext0 rgb'] = {
        prefix = 'cat-macchiato-Subtext0-rgb',
        body = {
            'rgb(165, 173, 203)'
        }
    },
    ['Catppuccin Macchiato Subtext0 hsl'] = {
        prefix = 'cat-macchiato-Subtext0-hsl',
        body = {
            'hsl(227, 27%, 72%)'
        }
    },
    ['Catppuccin Macchiato Overlay2 hex'] = {
        prefix = 'cat-macchiato-Overlay2-hex',
        body = {
            '#939ab7'
        }
    },
    ['Catppuccin Macchiato Overlay2 rgb'] = {
        prefix = 'cat-macchiato-Overlay2-rgb',
        body = {
            'rgb(147, 154, 183)'
        }
    },
    ['Catppuccin Macchiato Overlay2 hsl'] = {
        prefix = 'cat-macchiato-Overlay2-hsl',
        body = {
            'hsl(228, 20%, 65%)'
        }
    },
    ['Catppuccin Macchiato Overlay1 hex'] = {
        prefix = 'cat-macchiato-Overlay1-hex',
        body = {
            '#8087a2'
        }
    },
    ['Catppuccin Macchiato Overlay1 rgb'] = {
        prefix = 'cat-macchiato-Overlay1-rgb',
        body = {
            'rgb(128, 135, 162)'
        }
    },
    ['Catppuccin Macchiato Overlay1 hsl'] = {
        prefix = 'cat-macchiato-Overlay1-hsl',
        body = {
            'hsl(228, 15%, 57%)'
        }
    },
    ['Catppuccin Macchiato Overlay0 hex'] = {
        prefix = 'cat-macchiato-Overlay0-hex',
        body = {
            '#6e738d'
        }
    },
    ['Catppuccin Macchiato Overlay0 rgb'] = {
        prefix = 'cat-macchiato-Overlay0-rgb',
        body = {
            'rgb(110, 115, 141)'
        }
    },
    ['Catppuccin Macchiato Overlay0 hsl'] = {
        prefix = 'cat-macchiato-Overlay0-hsl',
        body = {
            'hsl(230, 12%, 49%)'
        }
    },
    ['Catppuccin Macchiato Surface2 hex'] = {
        prefix = 'cat-macchiato-Surface2-hex',
        body = {
            '#5b6078'
        }
    },
    ['Catppuccin Macchiato Surface2 rgb'] = {
        prefix = 'cat-macchiato-Surface2-rgb',
        body = {
            'rgb(91, 96, 120)'
        }
    },
    ['Catppuccin Macchiato Surface2 hsl'] = {
        prefix = 'cat-macchiato-Surface2-hsl',
        body = {
            'hsl(230, 14%, 41%)'
        }
    },
    ['Catppuccin Macchiato Surface1 hex'] = {
        prefix = 'cat-macchiato-Surface1-hex',
        body = {
            '#494d64'
        }
    },
    ['Catppuccin Macchiato Surface1 rgb'] = {
        prefix = 'cat-macchiato-Surface1-rgb',
        body = {
            'rgb(73, 77, 100)'
        }
    },
    ['Catppuccin Macchiato Surface1 hsl'] = {
        prefix = 'cat-macchiato-Surface1-hsl',
        body = {
            'hsl(231, 16%, 34%)'
        }
    },
    ['Catppuccin Macchiato Surface0 hex'] = {
        prefix = 'cat-macchiato-Surface0-hex',
        body = {
            '#363a4f'
        }
    },
    ['Catppuccin Macchiato Surface0 rgb'] = {
        prefix = 'cat-macchiato-Surface0-rgb',
        body = {
            'rgb(54, 58, 79)'
        }
    },
    ['Catppuccin Macchiato Surface0 hsl'] = {
        prefix = 'cat-macchiato-Surface0-hsl',
        body = {
            'hsl(230, 19%, 26%)'
        }
    },
    ['Catppuccin Macchiato Base hex'] = {
        prefix = 'cat-macchiato-Base-hex',
        body = {
            '#24273a'
        }
    },
    ['Catppuccin Macchiato Base rgb'] = {
        prefix = 'cat-macchiato-Base-rgb',
        body = {
            'rgb(36, 39, 58)'
        }
    },
    ['Catppuccin Macchiato Base hsl'] = {
        prefix = 'cat-macchiato-Base-hsl',
        body = {
            'hsl(232, 23%, 18%)'
        }
    },
    ['Catppuccin Macchiato Mantle hex'] = {
        prefix = 'cat-macchiato-Mantle-hex',
        body = {
            '#1e2030'
        }
    },
    ['Catppuccin Macchiato Mantle rgb'] = {
        prefix = 'cat-macchiato-Mantle-rgb',
        body = {
            'rgb(30, 32, 48)'
        }
    },
    ['Catppuccin Macchiato Mantle hsl'] = {
        prefix = 'cat-macchiato-Mantle-hsl',
        body = {
            'hsl(233, 23%, 15%)'
        }
    },
    ['Catppuccin Macchiato Crust hex'] = {
        prefix = 'cat-macchiato-Crust-hex',
        body = {
            '#181926'
        }
    },
    ['Catppuccin Macchiato Crust rgb'] = {
        prefix = 'cat-macchiato-Crust-rgb',
        body = {
            'rgb(24, 25, 38)'
        }
    },
    ['Catppuccin Macchiato Crust hsl'] = {
        prefix = 'cat-macchiato-Crust-hsl',
        body = {
            'hsl(236, 23%, 12%)'
        }
    },
    ['Catppuccin Mocha background'] = {
        prefix = 'cat-mocha-bg',
        body = {
            '#1E1E2E'
        }
    },
    ['Catppuccin Mocha foreground'] = {
        prefix = 'cat-mocha-fg',
        body = {
            '#CDD6F4'
        }
    },
    ['Catppuccin Mocha color0'] = {
        prefix = 'cat-mocha-color0',
        body = {
            '#45475A'
        }
    },
    ['Catppuccin Mocha color8'] = {
        prefix = 'cat-mocha-color8',
        body = {
            '#585B70'
        }
    },
    ['Catppuccin Mocha color1'] = {
        prefix = 'cat-mocha-color1',
        body = {
            '#F38BA8'
        }
    },
    ['Catppuccin Mocha color9'] = {
        prefix = 'cat-mocha-color9',
        body = {
            '#F38BA8'
        }
    },
    ['Catppuccin Mocha color2'] = {
        prefix = 'cat-mocha-color2',
        body = {
            '#A6E3A1'
        }
    },
    ['Catppuccin Mocha color10'] = {
        prefix = 'cat-mocha-color10',
        body = {
            '#A6E3A1'
        }
    },
    ['Catppuccin Mocha color3'] = {
        prefix = 'cat-mocha-color3',
        body = {
            '#F9E2AF'
        }
    },
    ['Catppuccin Mocha color11'] = {
        prefix = 'cat-mocha-color11',
        body = {
            '#F9E2AF'
        }
    },
    ['Catppuccin Mocha color4'] = {
        prefix = 'cat-mocha-color4',
        body = {
            '#89B4FA'
        }
    },
    ['Catppuccin Mocha color12'] = {
        prefix = 'cat-mocha-color12',
        body = {
            '#89B4FA'
        }
    },
    ['Catppuccin Mocha color5'] = {
        prefix = 'cat-mocha-color5',
        body = {
            '#F5C2E7'
        }
    },
    ['Catppuccin Mocha color13'] = {
        prefix = 'cat-mocha-color13',
        body = {
            '#F5C2E7'
        }
    },
    ['Catppuccin Mocha color6'] = {
        prefix = 'cat-mocha-color6',
        body = {
            '#94E2D5'
        }
    },
    ['Catppuccin Mocha color14'] = {
        prefix = 'cat-mocha-color14',
        body = {
            '#94E2D5'
        }
    },
    ['Catppuccin Mocha color7'] = {
        prefix = 'cat-mocha-color7',
        body = {
            '#BAC2DE'
        }
    },
    ['Catppuccin Mocha color15'] = {
        prefix = 'cat-mocha-color15',
        body = {
            '#A6ADC8'
        }
    },
    ['Catppuccin Macchiato background'] = {
        prefix = 'cat-macchiato-bg',
        body = {
            '#24273A'
        }
    },
    ['Catppuccin Macchiato foreground'] = {
        prefix = 'cat-macchiato-fg',
        body = {
            '#CAD3F5'
        }
    },
    ['Catppuccin Macchiato color0'] = {
        prefix = 'cat-macchiato-color0',
        body = {
            '#494D64'
        }
    },
    ['Catppuccin Macchiato color8'] = {
        prefix = 'cat-macchiato-color8',
        body = {
            '#5B6078'
        }
    },
    ['Catppuccin Macchiato color1'] = {
        prefix = 'cat-macchiato-color1',
        body = {
            '#ED8796'
        }
    },
    ['Catppuccin Macchiato color9'] = {
        prefix = 'cat-macchiato-color9',
        body = {
            '#ED8796'
        }
    },
    ['Catppuccin Macchiato color2'] = {
        prefix = 'cat-macchiato-color2',
        body = {
            '#A6DA95'
        }
    },
    ['Catppuccin Macchiato color10'] = {
        prefix = 'cat-macchiato-color10',
        body = {
            '#A6DA95'
        }
    },
    ['Catppuccin Macchiato color3'] = {
        prefix = 'cat-macchiato-color3',
        body = {
            '#EED49F'
        }
    },
    ['Catppuccin Macchiato color11'] = {
        prefix = 'cat-macchiato-color11',
        body = {
            '#EED49F'
        }
    },
    ['Catppuccin Macchiato color4'] = {
        prefix = 'cat-macchiato-color4',
        body = {
            '#8AADF4'
        }
    },
    ['Catppuccin Macchiato color12'] = {
        prefix = 'cat-macchiato-color12',
        body = {
            '#8AADF4'
        }
    },
    ['Catppuccin Macchiato color5'] = {
        prefix = 'cat-macchiato-color5',
        body = {
            '#F5BDE6'
        }
    },
    ['Catppuccin Macchiato color13'] = {
        prefix = 'cat-macchiato-color13',
        body = {
            '#F5BDE6'
        }
    },
    ['Catppuccin Macchiato color6'] = {
        prefix = 'cat-macchiato-color6',
        body = {
            '#8BD5CA'
        }
    },
    ['Catppuccin Macchiato color14'] = {
        prefix = 'cat-macchiato-color14',
        body = {
            '#8BD5CA'
        }
    },
    ['Catppuccin Macchiato color7'] = {
        prefix = 'cat-macchiato-color7',
        body = {
            '#B8C0E0'
        }
    },
    ['Catppuccin Macchiato color15'] = {
        prefix = 'cat-macchiato-color15',
        body = {
            '#A5ADCB'
        }
    },
    ['Catppuccin Mocha background'] = {
        prefix = 'cat-mocha-bg',
        body = {
            '#EFF1F5'
        }
    },
    ['Catppuccin Mocha foreground'] = {
        prefix = 'cat-mocha-fg',
        body = {
            '#4C4F69'
        }
    },
    ['Catppuccin Latte color0'] = {
        prefix = 'cat-latte-color0',
        body = {
            '#5C5F77'
        }
    },
    ['Catppuccin Latte color8'] = {
        prefix = 'cat-latte-color8',
        body = {
            '#6C6F85'
        }
    },
    ['Catppuccin Latte color1'] = {
        prefix = 'cat-latte-color1',
        body = {
            '#D20F39'
        }
    },
    ['Catppuccin Latte color9'] = {
        prefix = 'cat-latte-color9',
        body = {
            '#D20F39'
        }
    },
    ['Catppuccin Latte color2'] = {
        prefix = 'cat-latte-color2',
        body = {
            '#40A02B'
        }
    },
    ['Catppuccin Latte color10'] = {
        prefix = 'cat-latte-color10',
        body = {
            '#40A02B'
        }
    },
    ['Catppuccin Latte color3'] = {
        prefix = 'cat-latte-color3',
        body = {
            '#DF8E1D'
        }
    },
    ['Catppuccin Latte color11'] = {
        prefix = 'cat-latte-color11',
        body = {
            '#DF8E1D'
        }
    },
    ['Catppuccin Latte color4'] = {
        prefix = 'cat-latte-color4',
        body = {
            '#1E66F5'
        }
    },
    ['Catppuccin Latte color12'] = {
        prefix = 'cat-latte-color12',
        body = {
            '#1E66F5'
        }
    },
    ['Catppuccin Latte color5'] = {
        prefix = 'cat-latte-color5',
        body = {
            '#EA76CB'
        }
    },
    ['Catppuccin Latte color13'] = {
        prefix = 'cat-latte-color13',
        body = {
            '#EA76CB'
        }
    },
    ['Catppuccin Latte color6'] = {
        prefix = 'cat-latte-color6',
        body = {
            '#179299'
        }
    },
    ['Catppuccin Latte color14'] = {
        prefix = 'cat-latte-color14',
        body = {
            '#179299'
        }
    },
    ['Catppuccin Latte color7'] = {
        prefix = 'cat-latte-color7',
        body = {
            '#ACB0BE'
        }
    },
    ['Catppuccin Latte color15'] = {
        prefix = 'cat-latte-color15',
        body = {
            '#BCC0CC'
        }
    },
    ['Catppuccin Latte background'] = {
        prefix = 'cat-latte-#background',
        body = {
            '#303446'
        }
    },
    ['Catppuccin Latte foreground'] = {
        prefix = 'cat-latte-#foreground',
        body = {
            '#C6D0F5'
        }
    },
    ['Catppuccin Latte color0'] = {
        prefix = 'cat-latte-color0',
        body = {
            '#51576D'
        }
    },
    ['Catppuccin Latte color8'] = {
        prefix = 'cat-latte-color8',
        body = {
            '#626880'
        }
    },
    ['Catppuccin Latte color1'] = {
        prefix = 'cat-latte-color1',
        body = {
            '#E78284'
        }
    },
    ['Catppuccin Latte color9'] = {
        prefix = 'cat-latte-color9',
        body = {
            '#E78284'
        }
    },
    ['Catppuccin Latte color2'] = {
        prefix = 'cat-latte-color2',
        body = {
            '#A6D189'
        }
    },
    ['Catppuccin Latte color10'] = {
        prefix = 'cat-latte-color10',
        body = {
            '#A6D189'
        }
    },
    ['Catppuccin Latte color3'] = {
        prefix = 'cat-latte-color3',
        body = {
            '#E5C890'
        }
    },
    ['Catppuccin Latte color11'] = {
        prefix = 'cat-latte-color11',
        body = {
            '#E5C890'
        }
    },
    ['Catppuccin Latte color4'] = {
        prefix = 'cat-latte-color4',
        body = {
            '#8CAAEE'
        }
    },
    ['Catppuccin Latte color12'] = {
        prefix = 'cat-latte-color12',
        body = {
            '#8CAAEE'
        }
    },
    ['Catppuccin Latte color5'] = {
        prefix = 'cat-latte-color5',
        body = {
            '#F4B8E4'
        }
    },
    ['Catppuccin Latte color13'] = {
        prefix = 'cat-latte-color13',
        body = {
            '#F4B8E4'
        }
    },
    ['Catppuccin Latte color6'] = {
        prefix = 'cat-latte-color6',
        body = {
            '#81C8BE'
        }
    },
    ['Catppuccin Latte color14'] = {
        prefix = 'cat-latte-color14',
        body = {
            '#81C8BE'
        }
    },
    ['Catppuccin Latte color7'] = {
        prefix = 'cat-latte-color7',
        body = {
            '#B5BFE2'
        }
    },
    ['Catppuccin Latte color15'] = {
        prefix = 'cat-latte-color15',
        body = {
            '#A5ADCE'
        }
    }
}
