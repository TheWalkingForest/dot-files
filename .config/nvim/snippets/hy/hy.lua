return {
    Main = {
        prefix = "main",
        body = {
            "(defn main []",
            "  ) ; main",
            "",
            "(if (= __name__ \"__main__\")",
            "  (main)",
            "  None)"
        },
    },
    Define_Function = {
        prefix = "defn",
        body = {
            "(defn $1 []",
            "  ) ; $1"
        },
    }
}
