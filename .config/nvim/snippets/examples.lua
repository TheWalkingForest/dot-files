return {
    { name = 'Basic',        prefix = "ba", body = "T1=$1 T2=$2 T0=$0" },
    { name = 'Placeholders', prefix = "pl", body = "T1=${1:aa}\nT2=${2:<$1>}" },
    { name = 'Choices',      prefix = "ch", body = "T1=${1|a,b|} T2=${2|c,d|}" },
    { name = 'Linked',       prefix = "li", body = "T1=$1\n\tT1=$1" },
    { name = 'Variables',    prefix = "va", body = "Runtime: $VIMRUNTIME\n" },
    {
        name = 'Complex',
        prefix = "co",
        body = { "T1=${1:$RANDOM}", "T3=${3:$1_${2:$1}}", "T2=$2" }
    }
}
