return {
    ['Minify Rust Binary'] = {
        prefix = 'minirust',
        body = {
            '[profile.release-mini]',
            'inherits = "release"',
            'strip = true',
            'opt-level = "s"',
            'lto = true',
            'codegen-units = 1'
        },
    },
    ['Mold setup'] = {
        prefix = 'mold',
        body = {
            '[target.x86_64-unknown-linux-gnu]',
            'linker = "clang"',
            'rustflags = ["-C", "link-arg=-fuse-ld=/usr/bin/mold"]',
        },
    }
}
