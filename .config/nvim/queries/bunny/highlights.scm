"def" @keyword
"end" @keyword
"if" @keyword
"then" @keyword
"else" @keyword
"while" @keyword
"do" @keyword
"import" @keyword
"return" @keyword

(integer) @constant.builtin
(float) @constant.builtin
(string) @string

(type_integer) @type.builtin
(type_float) @type.builtin
(type_string) @type.builtin
(type_boolean) @type.builtin
(type_identifier) @type

(assign
  variable: (identifier) @variable
  ":" @punctuation.delimiter
  "=" @punctuation.delimiter)

(function_definition
  func_name: (identifier) @function)

"(" @punctuation.bracket
")" @punctuation.bracket

"::" @punctuation.delimiter
":" @punctuation.delimiter
"," @punctuation.delimiter
";" @punctuation.delimiter
