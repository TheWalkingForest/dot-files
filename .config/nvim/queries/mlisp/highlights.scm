[
  "lambda"
  "let"
  "do"
  "else"
] @keyword
(identifier) @variable
(number_lit) @number
(float_lit) @number
(string_lit) @string
[
  (boolean_lit)
  (nil_lit)
] @constant.builtin
["(" ")" "[" "]"] @punctuation.bracket
