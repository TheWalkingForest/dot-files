--Remap space as leader key
vim.api.nvim_set_keymap("", "<Space>", "<Nop>", {})
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

vim.opt.termguicolors = true -- set term gui colors (most terminals support this)
require('fwalk.lazy_init')
require('fwalk.keybinds')
require('fwalk.autocmds')

vim.opt.foldenable = false
vim.opt.foldcolumn = '1'
vim.opt.foldlevelstart = 99
vim.opt.foldmethod = 'expr'
vim.opt.foldexpr = 'nvim_treesitter#foldexpr()'
vim.cmd.colorscheme('catppuccin')

if vim.g.neovide then
    vim.opt.guifont = 'ComicShannsMono Nerd Font:h14'
    vim.g.neovide_padding_top = 5
    vim.g.neovide_padding_bottom = 5
    vim.g.neovide_padding_left = 5
    vim.g.neovide_padding_right = 5
    vim.g.neovide_hide_mouse_when_typing = true
    vim.g.neovide_confirm_quit = true
    vim.g.neovide_cursor_animation_length = 0
end
