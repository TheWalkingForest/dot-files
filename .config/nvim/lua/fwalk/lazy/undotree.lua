return {
    {
        'mbbill/undotree',
        keys = {
            {
                '<leader>u',
                function()
                    vim.cmd.UndotreeToggle(); vim.cmd.UndotreeFocus()
                end,
                mode = 'n',
                desc = 'Open Undotree',
            },
        },
        config = function()
            if not vim.api.nvim_get_var('loaded_undotree') then
                if vim.api.nvim_get_var('undotree_WindowLayout') then
                    vim.api.nvim_set_var('undotree_WindowLayout', 1)
                end
                if vim.api.nvim_get_var('undotree_ShortIndicators') then
                    vim.api.nvim_set_var('undotree_ShortIndicators', 0)
                end
            end
        end,
    },
}
