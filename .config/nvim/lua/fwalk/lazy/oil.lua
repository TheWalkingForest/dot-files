return {
    {
        'stevearc/oil.nvim',
        dependencies = { 'nvim-tree/nvim-web-devicons' },
        lazy = false,
        keys = {
            { '<leader>fe', '<cmd>Oil<CR>', mode = 'n', desc = 'Open Oil' },
        },
        opts = {
            columns = {
                "icon",
            },
            skip_confirm_for_simple_edits = true,
            keymaps = {
                ['<C-h>'] = false,
                ['<C-l>'] = false,
            },
            float = {
                padding = 4,
                max_height = 50,
            },
            watch_for_changed = true,
            view_options = {
                show_hidden = true,
            }
        },
    }
}
