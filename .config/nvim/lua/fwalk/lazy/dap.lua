return {
    {
        'mfussenegger/nvim-dap',
        keys = {
            { '<F5>',       '<cmd>DapContinue<CR>',         mode = 'n', desc = 'Continue' },
            { '<F10>',      '<cmd>DapStepOver<CR>',         mode = 'n', desc = 'Step Over' },
            { '<F11>',      '<cmd>DapStepInto<CR>',         mode = 'n', desc = 'Step into' },
            { '<F12>',      '<cmd>DapStepOut<CR>',          mode = 'n', desc = 'Step Out' },
            { '<Leader>dt', '<cmd>DapToggleBreakpoint<CR>', mode = 'n', desc = 'Toggle breakpoint' },
            { '<Leader>dc', '<cmd>DapContinue<CR>',         mode = 'n', desc = 'Continue' },
            { '<Leader>dx', '<cmd>DapTerminate<CR>',        mode = 'n', desc = 'Terminate dap' },
            { '<Leader>dr', '<cmd>DapRestartFrame<CR>',     mode = 'n', desc = 'RestartFrame' },
        },
        config = function()
            local ok, xdg_data_home = pcall(os.getenv, 'XDG_DATA_HOME')
            if not ok then
                return
            end

            local dap = require('dap')
            dap.adapters.cppdbg = {
                id = 'cppdbg',
                type = 'executable',
                command = xdg_data_home .. '/nvim/mason/packages/cpptools/extension/debugAdapters/bin/OpenDebugAD7',
            }
            dap.configurations.cpp = {
                {
                    name = "Launch file",
                    type = "cppdbg",
                    request = "launch",
                    program = function()
                        return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
                    end,
                    cwd = '${workspaceFolder}',
                    stopAtEntry = true,
                },
                {
                    name = 'Attach to gdbserver :1234',
                    type = 'cppdbg',
                    request = 'launch',
                    MIMode = 'gdb',
                    miDebuggerServerAddress = 'localhost:1234',
                    miDebuggerPath = '/usr/bin/gdb',
                    cwd = '${workspaceFolder}',
                    program = function()
                        return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
                    end,
                },
            }
            dap.configurations.c = dap.configurations.cpp
            dap.configurations.rust = dap.configurations.cpp
        end,
    },
    {
        'theHamsta/nvim-dap-virtual-text',
        dependencies = {
            'mfussenegger/nvim-dap',
            'nvim-treesitter/nvim-treesitter',
        },
        opts = {
            virt_text_pos = 'eol',
        },
        config = true,
    },
    -- {
    --     'rcarriga/nvim-dap-ui',
    --     dependencies = {
    --         'mfussenegger/nvim-dap',
    --         'nvim-neotest/nvim-nio',
    --     },
    --     config = function()
    --         require('dapui').setup()
    --         local dap, dapui = require('dap'), require('dapui')
    --         dap.listeners.before.attach.dapui_config = function()
    --             dapui.open()
    --         end
    --         dap.listeners.before.launch.dapui_config = function()
    --             dapui.open()
    --         end
    --         dap.listeners.before.event_terminated.dapui_config = function()
    --             dapui.close()
    --         end
    --         dap.listeners.before.event_exited.dapui_config = function()
    --             dapui.close()
    --         end
    --     end
    -- },
    {
        'leoluz/nvim-dap-go',
        dependencies = {
            'mfussenegger/nvim-dap',
            'rcarriga/nvim-dap-ui',
        },
        name = 'dap-go',
        config = true,
    },
    {
        'mfussenegger/nvim-dap-python',
        dependencies = {
            'mfussenegger/nvim-dap',
            'rcarriga/nvim-dap-ui',
        },
        config = function()
            require('dap-python').setup('python')
        end,
    },
}
