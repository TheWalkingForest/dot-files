return {
    {
        "julienvincent/nvim-paredit",
        opts = {
            ["af"] = false,
            ["if"] = false,
            ["aF"] = false,
            ["iF"] = false,
            ["ae"] = false,
            ["ie"] = false,
        },
        config = true,
    }
}
