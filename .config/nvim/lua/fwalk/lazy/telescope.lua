return {
    {
        -- TODO: fis
        'nvim-telescope/telescope.nvim',
        dependencies = { 'nvim-lua/plenary.nvim', },
        keys = {
            { '<leader>lI', vim.cmd.Mason,                                          mode = 'n', desc = 'Open Mason' },
            { '<leader>zf', require('telescope.builtin').fd,                        mode = 'n', desc = 'Open File' },
            { '<leader>zb', require('telescope.builtin').buffers,                   mode = 'n', desc = 'List Buffers' },
            { '<leader>zh', require('telescope.builtin').help_tags,                 mode = 'n', desc = 'Nvim Help' },
            { '<leader>zt', vim.cmd.TodoTelescope,                                  mode = 'n', desc = 'Todos' },
            -- { '<leader>za', function() vim.cmd.Telescope('aerial') end,             mode = 'n', desc = 'Aerial' },
            { '<leader>zg', require('telescope.builtin').current_buffer_fuzzy_find, mode = 'n', desc = 'Fzf Buffer' },
            { '<leader>zG', require('telescope.builtin').live_grep,                 mode = 'n', desc = 'Fzf Workspace' },
        },
        config = function()
            require('telescope').setup({
                defaults = {
                    prompt_prefix = ' ❯ ',
                    selection_caret = ' ',
                    path_display = { 'smart' },
                    sorting_strategy = 'ascending',
                    layout_strategy = 'horizontal',
                    layout_config = {
                        vertical = {
                            height = 0.5,
                            prompt_position = "top",
                        },
                        horizontal = {
                            height = 0.5,
                            prompt_position = "top",
                        },
                    },
                    file_ignore_patterns = {
                        '.git/',
                        'target/',
                        'docs/',
                        'vendor/*',
                        '%.lock',
                        '__pycache__/*',
                        '%.sqlite3',
                        '%.ipynb',
                        'node_modules/*',
                        '%.jpg',
                        '%.jpeg',
                        '%.jpeg',
                        '%.png',
                        '%.svg',
                        '%.otf',
                        '%.ttf',
                        '%.webp',
                        '.dart_tool/',
                        '.github/',
                        '.gradle/',
                        '.idea/',
                        '.settings/',
                        '.vscode/',
                        '__pycache__/',
                        'build/',
                        'env/',
                        'gradle/',
                        'node_modules/',
                        '%.pdb',
                        '%.dll',
                        '%.class',
                        '%.exe',
                        '%.cache',
                        '%.ico',
                        '%.pdf',
                        '%.dylib',
                        '%.jar',
                        '%.docx',
                        '%.met',
                        'smalljre_*/*',
                        '.vale/',
                        '%.burp',
                        '%.mp4',
                        '%.mkv',
                        '%.rar',
                        '%.zip',
                        '%.7z',
                        '%.tar',
                        '%.bz2',
                        '%.epub',
                        '%.flac',
                        '%.tar.gz',
                    },
                    mappings = {
                        i = {
                            ['<ESC>'] = require('telescope.actions').close,
                            ['<C-n>'] = require('telescope.actions').move_selection_next,
                            ['<C-p>'] = require('telescope.actions').move_selection_previous,
                        },
                    },
                },
                pickers = {
                    current_buffer_fuzzy_find = {
                        previewer = true
                    },
                    live_grep = {
                        previewer = true
                    },
                },
                extensions = {
                    -- aerial = {
                    --     show_nesting = {
                    --         ['_'] = false,
                    --         json = true,
                    --         yaml = true,
                    --     }
                    -- },
                }
            })
            -- require('telescope').load_extension('aerial')
            require('telescope').load_extension('ui-select')

            local previewers = require('telescope.previewers')
            previewers.cat.new = 'bat'
            previewers.vimgrep.new = 'bat'
            previewers.qflist.new = 'bat'
        end
    },
    {
        'nvim-telescope/telescope-ui-select.nvim',
        dependencies = { 'nvim-telescope/telescope.nvim' },
        --config = function() end
    },
}
