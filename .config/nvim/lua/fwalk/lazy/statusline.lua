return {
    {
        'nvim-lualine/lualine.nvim',
        dependencies = { 'nvim-tree/nvim-web-devicons' },
        opts = {
            options = {
                theme = 'catppuccin',
                section_separators = { left = '', right = '' },
                component_separators = { left = ')', right = '(' },
            },
            sections = {
                lualine_a = { 'mode' },
                lualine_b = { { 'filename', path = 4 } },
                lualine_c = { 'branch', 'diff', 'diagnostics' },

                lualine_x = { 'encoding', 'fileformat' },
                lualine_y = { 'filetype' },
                lualine_z = { 'location' },
            },
            inactive_sections = {
                lualine_a = {},
                lualine_b = {},
                lualine_c = { { 'filename', path = 4 } },
                lualine_x = { 'location' },
                lualine_y = {},
                lualine_z = {},
            },
            extensions = {
                'aerial',
                'fzf',
                'oil',
            },
        },
    },
}
