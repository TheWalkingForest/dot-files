return {
    {
        'leath-dub/snipe.nvim',
        branch = 'snipe2',
        keys = {
            { 'gb', function() require('snipe').open_buffer_menu() end, mode = 'n', desc = 'Open Snipe buffer menu' }
        },
        opts = {
            ui = {
                -- position = 'center',
                border = 'rounded',
            },
            hints = {
                dictionary = 'asdfgl;wertyuiop'
            },
            sort = 'default',
        },
    }
}
