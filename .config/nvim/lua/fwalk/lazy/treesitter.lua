return {
    {
        'nvim-treesitter/nvim-treesitter',
        config = function()
            local treesitter = require('nvim-treesitter.configs')
            treesitter.setup({
                sync_install = false,
                auto_install = true,
                ignore_install = { 'org' },
                highlight = {
                    enable = true,
                    additional_vim_regex_highlighting = false,
                },
                indent = {
                    enable = true,
                },
                rainbow = {
                    enable = true,
                },
            })

            local parser_config = require('nvim-treesitter.parsers').get_parser_configs()
            parser_config.hy = {
                install_info = {
                    url = 'https://github.com/kwshi/tree-sitter-hy',
                    files = { 'src/parser.c' },
                    branch = 'main',
                    generate_requires_npm = false,
                    requires_generate_from_grammar = false,
                },
                filetype = 'hy',
            }
            parser_config.mlisp = {
                install_info = {
                    url = 'https://gitlab.com/TheWalkingForest/tree-sitter-mlisp',
                    files = { 'src/parser.c' },
                    branch = 'main',
                    generate_requires_npm = false,
                    requires_generate_from_grammar = true,
                },
                filetype = 'mlisp',
            }
        end
    },
    {
        'nvim-treesitter/nvim-treesitter-context',
        dependencies = {
            'nvim-treesitter/nvim-treesitter'
        },
        main = 'treesitter-context',
        opts = {
            multiline_threshold = 1,
        },
    },
    {
        'hiphish/rainbow-delimiters.nvim',
        dependencies = { 'nvim-treesitter/nvim-treesitter' },
        main = 'rainbow-delimiters.setup',
        config = true,
    },
    {
        'stevearc/aerial.nvim',
        dependencies = {
            'nvim-treesitter/nvim-treesitter',
            'neovim/nvim-lspconfig',
        },
        opts = {
            backends = { "treesitter", "lsp" },
            layout = {
                default_direction = "float",
                placement = "window",
            },
            filter_kind = {
                "Class",
                "Enum",
                "Function",
                "Interface",
                "Module",
                "Method",
                "Struct",
            },
            autojump = true,
            close_on_select = true,
        },
    },
}
