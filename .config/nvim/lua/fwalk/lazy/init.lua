return {
    { 'nvim-tree/nvim-web-devicons', config = true },
    { 'echasnovski/mini.icons',      config = true },
    {
        'j-hui/fidget.nvim',
        opts = {
            notification = {
                window = {
                    winblend = 0,
                },
            },
        },
        config = true
    },
    { 'norcalli/nvim-colorizer.lua', config = true },
}
