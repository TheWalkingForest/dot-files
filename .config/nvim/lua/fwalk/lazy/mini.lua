return {
    {
        'echasnovski/mini.ai',
        opts = {
            mappings = {
                around = '',
                inside = '',
            },
        },
    },
    {
        'echasnovski/mini.comment',
        opts = {
            mappings = {
                comment = '',
            },
        },
    },
    {
        'echasnovski/mini.indentscope',
        opts = {
            symbol = '│',
        }
    },
    { 'echasnovski/mini.cursorword', config = true },
    -- { 'echasnovski/mini.pairs',      config = true },
    { 'echasnovski/mini.align',      config = true },
    { 'echasnovski/mini.starter',    config = true, },
    { 'echasnovski/mini.sessions',   config = true, },
    -- { 'echasnovski/mini.map',        config = true, },
    -- {
    --     'echasnovski/mini.files',
    --     keys = {
    --         -- { '<leader>fe', '<cmd>Oil<CR>', mode = 'n', desc = 'Open Oil' },
    --         { '<leader>fe', '<cmd>lua MiniFiles.open()<CR>', mode = 'n', desc = 'Open Mini.files' }
    --     },
    --     config = true,
    -- },
}
