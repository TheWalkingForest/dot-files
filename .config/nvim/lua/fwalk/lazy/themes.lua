return {
    {
        'catppuccin/nvim',
        priority = 100,
        main = 'catppuccin',
        opts = {
            flavor = "mocha",
            transparent_background = true,
            term_colors = true,
            integrations = {
                aerial = true,
                fidget = true,
                gitsigns = true,
                harpoon = true,
                mason = true,
                neogit = true,
                noice = true,
                cmp = true,
                dap = true,
                dap_ui = true,
                which_key = true,
                markdown = true,
                telescope = true,
            },
        },
        config = true,
    },
    {
        'bluz71/vim-moonfly-colors',
        name = 'moonfly',
        lazy = false,
        priority = 100,
    },
}
