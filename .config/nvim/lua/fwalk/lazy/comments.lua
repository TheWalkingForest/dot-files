return {
    {
        'folke/todo-comments.nvim',
        dependencies = {
            'nvim-lua/plenary.nvim',
            'nvim-telescope/telescope.nvim',
        },
        opts = {
            keywords = {
                FIX = {
                    icon = ' ',
                    color = 'error',
                    alt = { 'FIXME', 'BUG', 'ISSUE' },
                },
                TODO = {
                    icon = ' ',
                    color = 'info'
                },
                HACK = {
                    icon = ' ',
                    color = 'warning'
                },
                WARN = {
                    icon = ' ',
                    color = 'warning',
                    alt = { 'WARNING', 'XXX', 'REMOVE', 'DELETE' },
                },
                PERF = {
                    icon = ' ',
                    alt = { 'OPTIM', 'PERFORMANCE', 'OPTIMIZE' },
                },
                NOTE = {
                    icon = ' ',
                    color = 'hint',
                    alt = { 'INFO', 'SEE' },
                },
            },
            highlight = {
                keyword = 'wide',
                exclude = {
                    'markdown',
                },
            },
        },
    },
}
