return {
    {
        'ThePrimeagen/harpoon',
        keys = {
            { '<leader>hm', function() require('harpoon.mark').add_file() end,        mode = 'n', desc = "Harpoon Mark" },
            { '<leader>hl', function() require('harpoon.ui').toggle_quick_menu() end, mode = 'n', desc = "Harpoon List" },
            { '<M-1>',      function() require('harpoon.ui').nav_file(1) end,         mode = 'n', desc = "Harpoon 1" },
            { '<M-2>',      function() require('harpoon.ui').nav_file(2) end,         mode = 'n', desc = "Harpoon 2" },
            { '<M-3>',      function() require('harpoon.ui').nav_file(3) end,         mode = 'n', desc = "Harpoon 3" },
            { '<M-4>',      function() require('harpoon.ui').nav_file(4) end,         mode = 'n', desc = "Harpoon 4" },
            { '<M-5>',      function() require('harpoon.ui').nav_file(5) end,         mode = 'n', desc = "Harpoon 5" },
        },
        config = true,
    },
}
