return {
    {
        'jubnzv/mdeval.nvim',
        ft = { "markdown", "org" },
        keys = {
            { '<leader>oE', '<cmd>MdEval<CR>', mode = 'n', desc = 'Evaluate block' },
        },
        opts = {
            require_confirmation = false,
            eval_options = {
                fish = {
                    command = { 'fish' },
                    language_code = 'fish',
                    exec_type = 'interpreted',
                    extension = 'fish',
                },
                scheme = {
                    command = { 'guile', '--no-auto-compile', '-s' },
                    language_code = { 'scheme', 'guile' },
                    exec_type = 'interpreted',
                    extension = 'scm',
                },
                -- racket = {
                --     command = { 'racket', '-u' }, -- Command to run interpreter
                --     language_code = 'racket',     -- Markdown language code
                --     exec_type = 'interpreted',    -- compiled or interpreted
                --     extension = 'rkt',            -- File extension for temporary files
                -- },
            },
        },
        -- init = function()
        --     vim.g.markdown_fenced_languages = {
        --         'racket',
        --         'fish',
        --         'guile',
        --     }
        -- end
    },
}
