local servers = {
    -- 'bashls',
    -- 'clangd',
    -- 'cssls',
    -- 'elixirls',
    -- 'elmls',
    -- 'gopls',
    -- 'html',
    -- 'htmx',
    -- 'jsonls',
    -- 'lua_ls',
    -- 'marksman',
    -- 'nim_langserver',
    -- 'ocamllsp',
    -- 'ruff_lsp',
    -- 'rust_analyzer',
    -- 'taplo',
    -- 'yamlls',
    -- 'tailwindcss',
    -- 'eslint',
    -- 'vimls,'
}

return {
    {
        'williamboman/mason.nvim',
        keys = {
            { '<leader>lI', vim.cmd.Mason, mode = 'n', desc = 'Open Mason' }
        },
        opts = {
            ui = {
                icons = {
                    package_installed = '✓',
                    package_pending = '➜',
                    package_uninstalled = '✗',
                },
            },
        },
    },
    {
        'williamboman/mason-lspconfig.nvim',
        dependencies = {
            'neovim/nvim-lspconfig',
            'williamboman/mason.nvim',
        },
        opts = {
            ensure_installed = servers,
            automatic_installation = true
        },
    },
    {
        'WhoIsSethDaniel/mason-tool-installer.nvim',
        dependencies = {
            'williamboman/mason.nvim',
        },
        opts = {
            ensure_installed = servers,
            auto_update = true,
            run_on_start = true,
            debounce_hours = 24, -- wait 24 hours between attempts to install/update
        },
    },
}
