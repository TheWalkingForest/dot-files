---@class Keymap
---@field 1 string
---@field 2 string
---@field mode string
---@field desc string

---@param keybinds Keymap[]
---@param buf integer
local function set_keybinds(keybinds, buf)
    for _i, m in ipairs(keybinds) do
        local opts = { noremap = true, desc = m.desc }
        vim.api.nvim_buf_set_keymap(buf, m['mode'], m[1], m[2], opts)
    end
end

return {
    {
        'nvim-orgmode/orgmode',
        main = 'orgmode',
        lazy = false,
        ft = { 'org' },
        init = function()
            vim.api.nvim_create_autocmd('BufWritePre', {
                pattern = { '*.org' },
                command = 'exec "normal gg=G"'
            })
            vim.api.nvim_create_autocmd('FileType', {
                pattern = { 'org' },
                callback = function(ev)
                    local keymaps = {
                        { '<leader>oC', '<cmd>lua vim.opt.conceallevel = ((vim.opt.conceallevel:get() + 2) % 4)<CR>', mode = 'n', desc = 'Toggle conceal' }
                    }
                    set_keybinds(keymaps, ev.buf)
                end
            })
        end,
        opts = {
            org_agenda_files = { '~/Documents/Org/**/*', '~/Notes/**/*' },
            org_default_notes_file = '~/Notes/refile.org',
        }
    },
    {
        'nvim-orgmode/telescope-orgmode.nvim',
        event = 'VeryLazy',
        dependencies = {
            'nvim-orgmode/orgmode',
            'nvim-telescope/telescope.nvim',
        },
        init = function()
            require('telescope').load_extension('orgmode')
            vim.api.nvim_create_autocmd('FileType', {
                pattern = { 'org' },
                callback = function(ev)
                    local keymaps = {
                        { '<leader>oh', '<cmd>lua require(\'telescope\').extensions.orgmode.search_headings()<CR>', mode = 'n', desc = 'Search headings' },
                        -- { '<leader>r',  '<cmd>lua require(\'telescope\').extensions.orgmode.refile_heading()<CR>',  mode = 'n', desc = 'Refile heading' },
                        -- { '<leader>li', '<cmd>lua require(\'telescope\').extensions.orgmode.insert_link()<CR>',     mode = 'n', desc = 'Insert link' },
                    }
                    set_keybinds(keymaps, ev.buf)
                end
            })
        end,
        config = true,
    },
    {
        'nvim-orgmode/org-bullets.nvim',
        dependencies = { 'nvim-orgmode/orgmode', },
        config = true,
    },
    -- {
    --     'lukas-reineke/headlines.nvim',
    --     dependencies = { 'nvim-orgmode/orgmode', },
    --     config = true,
    -- },
}
