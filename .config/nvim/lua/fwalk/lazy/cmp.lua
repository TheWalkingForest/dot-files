return {
    {
        'L3MON4D3/LuaSnip',
        dependencies = { 'rafamadriz/friendly-snippets' },
        config = function()
            local config_dir = os.getenv("XDG_CONFIG_HOME") or "~/.config"
            require("luasnip").filetype_extend('ruby', { 'rails' })
            require("luasnip.loaders.from_vscode").lazy_load()
            require("luasnip.loaders.from_vscode").lazy_load({ paths = { config_dir .. "/nvim/snippets" } })
        end
    },
    {
        'saecki/crates.nvim',
        requires = 'nvim-lua/plenary.nvim',
        config = function()
            vim.api.nvim_create_autocmd('BufRead', {
                pattern = 'Cargo.toml',
                callback = function(ev)
                    local buf = ev.buf
                    local keymaps = {
                        { '<leader>cr', '<cmd>lua require(\'crates\').reload()<CR>',                   mode = 'n', desc = 'Refresh crates' },
                        { '<leader>cv', '<cmd>lua require(\'crates\').show_versions_popup()<CR>',      mode = 'n', desc = 'Show versions' },
                        { '<leader>cf', '<cmd>lua require(\'crates\').show_features_popup()<CR>',      mode = 'n', desc = 'Show features' },
                        { '<leader>cd', '<cmd>lua require(\'crates\').show_dependencies_popup()<CR>',  mode = 'n', desc = 'Show dependencies' },
                        { '<leader>cu', '<cmd>lua require(\'crates\').update_crate()<CR>',             mode = 'n', desc = 'Update crate' },
                        { '<leader>cu', '<cmd>lua require(\'crates\').update_crates()<CR>',            mode = 'v', desc = 'Update crates' },
                        { '<leader>ca', '<cmd>lua require(\'crates\').update_all_crates()<CR>',        mode = 'n', desc = 'Update all crates' },
                        { '<leader>cU', '<cmd>lua require(\'crates\').upgrade_crate()<CR>',            mode = 'n', desc = 'Upgrade crate' },
                        { '<leader>cU', '<cmd>lua require(\'crates\').upgrade_crates()<CR>',           mode = 'v', desc = 'Upgrade crates' },
                        { '<leader>cA', '<cmd>lua require(\'crates\').upgrade_all_crates()<CR>',       mode = 'n', desc = 'Upgrade all creates' },
                        { '<leader>cX', '<cmd>lua require(\'crates\').extract_crate_into_table()<CR>', mode = 'n', desc = 'Extract crate to table' },
                        { '<leader>cH', '<cmd>lua require(\'crates\').open_homepage()<CR>',            mode = 'n', desc = 'Open homepage' },
                        { '<leader>cR', '<cmd>lua require(\'crates\').open_repository()<CR>',          mode = 'n', desc = 'Open repository' },
                        { '<leader>cD', '<cmd>lua require(\'crates\').open_documentation()<CR>',       mode = 'n', desc = 'Open documentation' },
                        { '<leader>cC', '<cmd>lua require(\'crates\').open_crates_io()<CR>',           mode = 'n', desc = 'Open crates.io' },
                        { '<leader>cL', '<cmd>lua require(\'crates\').open_lib_rs()<CR>',              mode = 'n', desc = 'Open lib.rs' },
                    }

                    for i, m in ipairs(keymaps) do
                        local opts = { noremap = true, desc = m.desc }
                        vim.api.nvim_buf_set_keymap(buf, m['mode'], m[1], m[2], opts)
                    end
                end
            })
            require('crates').setup({})
        end
    },
    {
        'hrsh7th/nvim-cmp',
        lazy         = false,
        dependencies = {
            'L3MON4D3/LuaSnip',
            'hrsh7th/cmp-buffer',
            'hrsh7th/cmp-path',
            'hrsh7th/cmp-cmdline',
            'saadparwaiz1/cmp_luasnip',
            'hrsh7th/cmp-nvim-lsp',
            'hrsh7th/cmp-nvim-lua',
            'mtoohey31/cmp-fish',
            'amarakon/nvim-cmp-fonts',
            'chrisgrieser/cmp-nerdfont',
            'saecki/crates.nvim',
        },
        config       = function()
            local kind_icons = {
                Text = "",
                Method = "m",
                Function = "",
                Constructor = "",
                Field = "",
                Variable = "",
                Class = "",
                Interface = "",
                Module = "",
                Property = "",
                Unit = "",
                Value = "",
                Enum = "",
                Keyword = "",
                Snippet = "",
                Color = "",
                File = "",
                Reference = "",
                Folder = "",
                EnumMember = "",
                Constant = "",
                Struct = "",
                Event = "",
                Operator = "",
                TypeParameter = "",
            }
            local check_backspace = function()
                local col = vim.fn.col "." - 1
                return col == 0 or vim.fn.getline("."):sub(col, col):match "%s"
            end
            local luasnip = require('luasnip')
            local cmp = require('cmp')
            cmp.setup {
                snippet = {
                    expand = function(args)
                        luasnip.lsp_expand(args.body) -- For `luasnip` users.
                    end,
                },
                mapping = {
                    ["<C-p>"] = cmp.mapping.select_prev_item(),
                    ["<C-n>"] = cmp.mapping.select_next_item(),
                    ["<C-b>"] = cmp.mapping(cmp.mapping.scroll_docs(-1), { "i", "c" }),
                    ["<C-f>"] = cmp.mapping(cmp.mapping.scroll_docs(1), { "i", "c" }),
                    ["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),
                    ["<C-y>"] = cmp.mapping.confirm { select = true },
                    ["<C-e>"] = cmp.mapping {
                        i = cmp.mapping.abort(),
                        c = cmp.mapping.close(),
                    },
                },
                formatting = {
                    fields = { "kind", "abbr", "menu" },
                    format = function(entry, vim_item)
                        -- Kind icons
                        vim_item.kind = string.format("%s", kind_icons[vim_item.kind])
                        -- vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind) -- This concatonates the icons with the name of the item kind
                        vim_item.menu = ({
                            nvim_lsp = "[Lsp]",
                            nvim_lua = "[Nvim Lua]",
                            luasnip  = "[luasnip]",
                            crates   = "[Crates]",
                            fish     = "[Fish]",
                            buffer   = "[Buffer]",
                            path     = "[Path]",
                            neorg    = "[Norg]",
                            fonts    = "[Fonts]",
                            nerdfont = "[NerdFont]",
                        })[entry.source.name]
                        return vim_item
                    end,
                },
                sources = {
                    { name = "nvim_lsp" },
                    { name = "nvim_lua" },
                    { name = "crates" },
                    { name = "luasnip" },
                    { name = "buffer" },
                    { name = "fish" },
                    { name = "path" },
                    { name = "orgmode" },
                    { name = "neorg" },
                    { name = "nerdfont" },
                    { name = "fonts",   option = { space_filler = '-' } },
                },
                confirm_opts = {
                    behavior = cmp.ConfirmBehavior.Replace,
                    select = false,
                },
                window = {
                    documentation = cmp.config.window.bordered(),
                },
                experimental = {
                    ghost_text = true,
                    native_menu = false,
                },
            }
        end
    },
}
