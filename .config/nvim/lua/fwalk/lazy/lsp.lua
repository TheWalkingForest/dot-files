local servers = {
    'bashls',
    'biome',
    'cmake',
    'cssls',
    'elixirls',
    'elmls',
    'eslint',
    'fennel_ls',
    'gleam',
    'gopls',
    'hls',
    'html',
    'htmx',
    'jsonls',
    'lua_ls',
    'marksman',
    'nimls',
    'ocamllsp',
    'ols',
    'ruff',
    'rust_analyzer',
    'serve_d',
    'solargraph',
    'tailwindcss',
    'taplo',
    'ts_ls',
    'yamlls',
    -- 'clangd',
    -- 'nim_langserver',
    -- 'vimls,'
}

return {
    {
        'neovim/nvim-lspconfig',
        dependencies = { 'nvim-telescope/telescope.nvim', },
        config = function()
            local lspconfig = require('lspconfig')
            local cmp_nvim_lsp = require('cmp_nvim_lsp')

            local lsp_autocmds = vim.api.nvim_create_augroup('LspAutocmds', {})
            vim.api.nvim_create_autocmd('BufWritePre', {
                group = lsp_autocmds,
                callback = function(ev)
                    vim.lsp.buf.format({ buffner = ev.buf })
                end

            })
            vim.api.nvim_create_autocmd('LspAttach', {
                group = lsp_autocmds,
                callback = function(ev)
                    local buf = ev.buf
                    local keymaps = {
                        { 'gd',         '<cmd>lua vim.lsp.buf.definition()<CR>',                        mode = 'n', desc = 'Goto definition' },
                        { 'gI',         '<cmd>lua vim.lsp.buf.implementation()<CR>',                    mode = 'n', desc = 'Goto implementation' },
                        { 'gr',         '<cmd>lua require(\'telescope.builtin\').lsp_references()<CR>', mode = 'n', desc = 'Open references', },
                        { 'K',          '<cmd>lua vim.lsp.buf.hover()<CR>',                             mode = 'n', desc = 'Show info', },
                        { '<leader>li', '<cmd>lua vim.cmd.LspInfo()<CR>',                               mode = 'n', desc = 'Lsp Info', },
                        { '<leader>la', '<cmd>lua vim.lsp.buf.code_action()<CR>',                       mode = 'n', desc = 'Code actions', },
                        { '<leader>lf', '<cmd>lua vim.lsp.buf.format()<CR>',                            mode = 'n', desc = 'Format buffer', },
                        { '<leader>lj', '<cmd>lua vim.diagnostic.goto_next({buffer=0})<CR>',            mode = 'n', desc = 'Next diagnostic message', },
                        { '<leader>lk', '<cmd>lua vim.diagnostic.goto_prev({buffer=0})<CR>',            mode = 'n', desc = 'Previous diagnostic message', },
                        { '<leader>lr', '<cmd>lua vim.lsp.buf.rename()<CR>',                            mode = 'n', desc = 'Rename', },
                        { '<leader>lR', '<cmd>lua vim.cmd.LspRestart()<CR>',                            mode = 'n', desc = 'Restart LSP', },
                        { '<leader>ls', '<cmd>lua vim.lsp.buf.signature_help()<CR>',                    mode = 'n', desc = 'Signature help', },
                        { '<leader>lq', '<cmd>lua vim.diagnostic.setloclist()<CR>',                     mode = 'n', desc = 'Set loclist', },
                        { '<leader>ld', '<cmd>lua vim.lsp.buf.type_definition()<CR>',                   mode = 'n', desc = 'Goto type definition', },
                        { '<leader>ll', '<cmd>lua vim.diagnostic.open_float()<CR>',                     mode = 'n', desc = 'Open diagnostics', },
                    }

                    for i, m in ipairs(keymaps) do
                        local opts = { noremap = true, desc = m.desc }
                        vim.api.nvim_buf_set_keymap(buf, m['mode'], m[1], m[2], opts)
                    end
                end
            })
            local M = {}
            M.capabilities = vim.lsp.protocol.make_client_capabilities()
            M.capabilities.textDocument.completion.completionItem.snippetSupport = true
            M.capabilities = cmp_nvim_lsp.default_capabilities()

            -- for servers not installable by Mason
            for _, server in pairs({ 'nixd' }) do
                lspconfig[server].setup({
                    on_attach = M.on_attach,
                    capabilities = M.capabilities,
                    settings = {
                        codeActionOnSave = {
                            enable = true,
                            mode = 'all'
                        },
                    }
                })
            end
            for _, server in pairs(servers) do
                local opts = {
                    on_attach = M.on_attach,
                    capabilities = M.capabilities,
                    settings = {
                        codeActionOnSave = {
                            enable = true,
                            mode = 'all'
                        },
                    }
                }

                if server == 'ts_ls' then
                    local function organize_imports()
                        local params = {
                            command = "_typescript.organizeImports",
                            arguments = { vim.api.nvim_buf_get_name(0) }
                        }
                        vim.lsp.buf.execute_command(params)
                    end
                    opts.commands = {
                        OrganizeImports = {
                            organize_imports,
                            description = "Organize Imports"
                        },
                    }
                    opts.init_options = {
                        preferences = {
                            desableSuggestions = true,
                        }
                    }
                elseif server == 'lua_ls' then
                    opts.settings = {
                        Lua = {
                            diagnostics = {
                                globals = { 'vim', 'love' }
                            }
                        }
                    }
                elseif server == 'elixirls' then
                    opts.cmd = { os.getenv("HOME") ..
                    "/.local/share/nvim/mason/packages/elixir-ls/language_server.sh" }
                elseif server == 'html' or server == 'htmx' then
                    opts.filetypes = opts.filetypes or {}
                    table.insert(opts.filetypes, "html")
                    table.insert(opts.filetypes, "heex")
                end

                server = vim.split(server, '@')[1]

                lspconfig[server].setup(opts)
            end
        end
    },
}
