return {
    {
        'folke/which-key.nvim',
        lazy = false,
        main = 'which-key',
        opts = {
            win = {
                border = 'single',
            },
        },
    },
}
