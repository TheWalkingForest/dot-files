return {
    {
        'f-person/git-blame.nvim',
        config = function()
            vim.g.gitblame_enabled = 1
            vim.g.gitblame_message_template = "<summary> • <date> • <author>"
            vim.g.gitblame_highlight_group = "LineNr"
        end
    },
    {
        'lewis6991/gitsigns.nvim',
        lazy = false,
        opts = {
            signs = {
                add          = { text = '│' },
                change       = { text = '│' },
                delete       = { text = '│' },
                topdelete    = { text = '‾' },
                untracked    = { text = '│' },
                changedelete = { text = '┆' },
            },
        },
        keys = {
            { '<leader>gp', '<cmd>:Gitsigns preview_hunk<CR>',      mode = 'n', desc = 'Preview chunk', },
            { '<leader>gs', '<cmd>Gitsigns stage_hunk<CR>',         mode = 'n', desc = 'Stage chunk', },
            { '<leader>gr', '<cmd>Gitsigns reset_hunk<CR>',         mode = 'n', desc = 'Reset chunk', },
            { '<leader>gu', '<cmd>Gitsigns undo_stage_hunk<CR>',    mode = 'n', desc = 'Undo staged chunk', },
            { '<leader>gS', '<cmd>Gitsigns stage_buffer<CR>',       mode = 'n', desc = 'Stage buffer', },
            { '<leader>gR', '<cmd>Gitsigns reset_buffer<CR>',       mode = 'n', desc = 'Reset buffer', },
            { '<leader>gj', '<cmd>Gitsigns next_hunk<CR>',          mode = 'n', desc = 'Next chunk', },
            { '<leader>gk', '<cmd>Gitsigns prev_hunk<CR>',          mode = 'n', desc = 'Prev chunk', },
            { '<leader>gU', '<cmd>Gitsigns reset_buffer_index<CR>', mode = 'n', desc = 'Reset buffer index', },
        }
    },
    {
        'NeogitOrg/neogit',
        dependencies = {
            "nvim-lua/plenary.nvim",
            "nvim-telescope/telescope.nvim",
        },
        keys = {
            { '<leader>gg', '<cmd>Neogit<CR>',                 desc = "Neogit ui" },
            { '<leader>gc', '<cmd>Neogit commit<CR>',          desc = "Neogit commit" },
            { '<leader>gb', '<cmd>Telescope git_branches<CR>', desc = "Git Branches" },
        },
        config = true,
    },
}
