local opts = { noremap = true, silent = true }

-- Shorten function name
local keymap = vim.api.nvim_set_keymap

keymap('n', 'gc', '<Nop>', {})

-- Better window navigation
keymap('n', '<C-h>', '<C-w>h', opts)
keymap('n', '<C-j>', '<C-w>j', opts)
keymap('n', '<C-k>', '<C-w>k', opts)
keymap('n', '<C-l>', '<C-w>l', opts)

-- Navigate buffers
keymap('n', '<S-l>', ':bnext<CR>', opts)
keymap('n', '<S-h>', ':bprevious<CR>', opts)

-- Better page jumps
keymap('n', '<C-d>', '<C-d>zz', opts)
keymap('n', '<C-u>', '<C-u>zz', opts)

-- Better search jumps
keymap('n', 'n', 'nzz', opts)
keymap('n', 'N', 'Nzz', opts)

-- Remove highlighting after search
keymap('n', '<CR>', ':noh<CR>', opts)

-- Stay in indent mode
keymap('v', '<', '<gv', opts)
keymap('v', '>', '>gv', opts)

-- Move text up and down
keymap('v', 'p', '"_dP', opts)

keymap('n', ';', ':', { noremap = true })

keymap('t', '<ESC><ESC>', '<C-\\><C-n>', { noremap = true })

-- When text is wrapped, move by terminal rows, not lines, unless a count is provided
vim.cmd([[
nnoremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')
nnoremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')
]])

-- replace with whichkey
keymap('n', '<leader>sv', '<cmd>vsplit<CR>', { noremap = true, desc = 'Vertical split' })
keymap('n', '<leader>sh', '<cmd>split<CR>', { noremap = true, desc = 'Horizontal split' })

keymap('n', '<leader>bc', '<C-w>c', { noremap = true, desc = 'Close split' })
keymap('n', '<leader>bo', '<C-w>o', { noremap = true, desc = 'Close all other splits' })
keymap('n', '<leader>be', '<cmd>enew<CR>', { noremap = true, desc = 'Open empty buffer' })
keymap('n', '<leader>bq', '<cmd>bd<CR>', { noremap = true, desc = 'Close buffer' })
keymap('n', '<leader>bd', '<cmd>ls<CR>:delete<Space>', { noremap = true, desc = 'Delete buffer' })
keymap('n', '<leader>fs', '<cmd>up<CR>', { noremap = true, desc = 'Write buffer' })
keymap('n', '<leader><leader>', '<cmd>up<CR>', { noremap = true, desc = 'Write buffer' })
keymap('n', '<leader>fq', '<cmd>q!<CR>', { noremap = true, desc = 'Quit without saving' })
--keymap('n', '<leader>fg', '<cmd>edit <cfile><CR>', opts)
keymap('n', '<leader>fa', ':saveas<Space>', { noremap = true, silent = false, desc = 'Save buffer as' })
keymap('n', '<leader>fo', ':e<Space>', { noremap = true, silent = false, desc = 'Open file' })
-- keymap('n', '<leader>mm', '<cmd>BBuildTerm<CR>', opts)

local ok, _ = pcall(require, 'lazy')
if ok then
    keymap('n', '<leader>lL', '<cmd>Lazy<CR>', { noremap = true, desc = 'Open Lazy' })
end
