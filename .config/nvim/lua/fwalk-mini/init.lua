-- Clone 'mini.deps' manually in a way that it gets managed by 'mini.deps'
local path_package = vim.fn.stdpath('data') .. '/site/'
local mini_path = path_package .. 'pack/deps/start/mini.deps'
if not vim.loop.fs_stat(mini_path) then
    vim.cmd('echo "Installing `mini.deps`" | redraw')
    local clone_cmd = {
        'git', 'clone', '--filter=blob:none',
        'https://github.com/echasnovski/mini.deps', mini_path
    }
    vim.fn.system(clone_cmd)
    vim.cmd('packadd mini.deps | helptags ALL')
    vim.cmd('echo "Installed `mini.deps`" | redraw')
end

-- Set up 'mini.deps' (customize to your liking)
require('mini.deps').setup({ path = { package = path_package } })

vim.api.nvim_set_keymap("", "<Space>", "<Nop>", {})
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '


require("fwalk-mini.keybinds")
require("fwalk-mini.autocmds")
if vim.g.neovide then
    require("fwalk-mini.neovide")
end

local add = MiniDeps.add
local now = MiniDeps.later
local later = MiniDeps.later

add({ source = 'nvim-lua/plenary.nvim' })

add({ source = 'echasnovski/mini.nvim' })
later(function()
    require('mini.icons').setup()
    later(function() MiniIcons.tweak_lsp_kind('append') end)
    MiniIcons.mock_nvim_web_devicons()
end)

add({
    source = 'nvim-treesitter/nvim-treesitter',
    hooks = { post_checkout = function() vim.cmd('TSUpdate') end, }
})
add({
    source = 'nvim-treesitter/nvim-treesitter-context',
    depends = { 'nvim-treesitter/nvim-treesitter' }
})
add({
    source = 'https://gitlab.com/HiPhish/rainbow-delimiters.nvim',
    depends = { 'nvim-treesitter/nvim-treesitter' }
})
now(function()
    require('nvim-treesitter.configs').setup({
        auto_install = true,
        ignore_install = { 'org' },
        highlight = {
            enable = true,
            additional_vim_regex_highlighting = false,
        },
        rainbow = {
            enable = true,
        }
    })

    require('treesitter-context').setup()
    require('rainbow-delimiters.setup').setup()
end)

add({ source = 'catppuccin/nvim', })
later(function()
    require('catppuccin').setup({
        flavor = "mocha",
        transparent_background = false,
        show_end_of_buffer = true,
        term_colors = true,
    })
    vim.cmd.colorscheme('catppuccin')
end)

add({
    source = 'stevearc/oil.nvim',
    depeneds = { 'echasnovski/mini.nvim' }
})
later(function()
    require('oil').setup({
        columns = {
            "icon",
        },
        skip_confirm_for_simple_edits = true,
        keymaps = {
            ['<C-h>'] = false,
            ['<C-l>'] = false,
        },
        watch_for_changed = true,
        constrain_cursor = "name",
        view_options = {
            show_hidden = true,
        },
    })
    vim.keymap.set('n', '<leader>fe', '<cmd>Oil<CR>',
        { noremap = true, desc = 'Open Oil' })
end)

add({
    source = 'nvim-lualine/lualine.nvim',
    depeneds = {
        'echasnovski/mini.nvim',
        'catppuccin/nvim',
        'stevearc/aerial.nvim',
    }
})
-- TODO: Hello
later(function()
    require('lualine').setup({
        options = {
            theme = 'catppuccin',
            section_separators = { left = '', right = '' },
            component_separators = { left = ')', right = '(' },
        },
        sections = {
            lualine_c = { 'filename', 'aerial' },
        }
    })
end)

add({
    source = 'folke/todo-comments.nvim',
    depends = { 'nvim-lua/plenary.nvim', }
})

later(function() require('todo-comments').setup() end)

later(function()
    require('mini.ai').setup({
        mappings = {
            around = '',
            inside = '',
        },
    })
end)

later(function()
    require('mini.comment').setup({
        mappings = {
            comment = '',
        },
    })
end)

later(function()
    require('mini.indentscope').setup({
        symbol = '│',
    })
end)

later(function() require('mini.cursorword').setup() end)

later(function() require('mini.align').setup() end)

later(function() require('mini.jump').setup() end)

later(function()
    local gl = require('mini.snippets').gen_loader
    local snippets = vim.fn.stdpath('config') .. '/snippets/'
    require('mini.snippets').setup({
        mappings = {
            expand    = '<C-a>',
            jump_next = '<C-s>',
            jump_prev = '<C-d>',
            stop      = '<C-f>',
        },
        snippets = {
            gl.from_file(snippets .. 'global.lua'),
            gl.from_file(snippets .. 'license.lua'),
            gl.from_file(snippets .. 'examples.lua'),
            gl.from_lang(),
        }
    })
end)

add({ source = 'folke/which-key.nvim' })
later(function()
    require('which-key').setup({
        win = { border = 'single', },
    })
end)

add({ source = 'nvim-orgmode/orgmode' })
add({
    source = 'nvim-orgmode/org-bullets.nvim',
    depends = { 'nvim-orgmode/orgmode' }
})
later(function()
    require('orgmode').setup({
        org_agenda_files = { '~/Documents/Org/**/*', '~/Notes/**/*' },
        org_default_notes_file = '~/Notes/refile.org',
    })
    vim.api.nvim_create_autocmd('BufWritePre', {
        pattern = { '*.org' },
        command = 'exec "normal gg=G"'
    })
    require('org-bullets').setup()
end)

add({ source = 'f-person/git-blame.nvim' })
now(function()
    vim.g.gitblame_enabled = 1
    vim.g.gitblame_message_template = "<summary> • <date> • <author>"
    vim.g.gitblame_highlight_group = "LineNr"
end)

now(function()
    require('mini.diff').setup({
        view = {
            style = 'sign',
        },
        mappings = {
            goto_first = '<leader>gf',
            goto_prev = '<leader>gk',
            goto_next = '<leader>gj',
            goto_last = '<leader>gl',
        }
    })
    local keymap = vim.api.nvim_set_keymap
    keymap('n', '<leader>gp', '<cmd>lua MiniDiff.toggle_overlay()<CR>', { noremap = true, desc = 'Preview diff', })
end)

add({
    source = 'NeogitOrg/neogit',
    depends = { 'nvim-lua/plenary.nvim' }
})
later(function()
    require('neogit').setup()
    vim.api.nvim_set_keymap('n', '<leader>gg', '<cmd>Neogit<CR>', { noremap = true, desc = "Neogit ui" })
end)

add({ source = 'ThePrimeagen/harpoon' })
later(function()
    require('harpoon').setup()
    require('which-key').add({ { '<leader>h', group = 'Harpoon', mode = 'n' } })
    local keymap = vim.api.nvim_set_keymap
    keymap('n', '<leader>hm', '<cmd>lua require("harpoon.mark").add_file()<CR>',
        { noremap = true, desc = "Harpoon Mark" })
    keymap('n', '<leader>hl', '<cmd>lua require("harpoon.ui").toggle_quick_menu()<CR>',
        { noremap = true, desc = "Harpoon List" })
    keymap('n', '<M-1>', '<cmd>lua require("harpoon.ui").nav_file(1)<CR>', { noremap = true, desc = "Harpoon 1" })
    keymap('n', '<M-2>', '<cmd>lua require("harpoon.ui").nav_file(2)<CR>', { noremap = true, desc = "Harpoon 2" })
    keymap('n', '<M-3>', '<cmd>lua require("harpoon.ui").nav_file(3)<CR>', { noremap = true, desc = "Harpoon 3" })
    keymap('n', '<M-4>', '<cmd>lua require("harpoon.ui").nav_file(4)<CR>', { noremap = true, desc = "Harpoon 4" })
    keymap('n', '<M-5>', '<cmd>lua require("harpoon.ui").nav_file(5)<CR>', { noremap = true, desc = "Harpoon 5" })
end)

add({
    source = 'hrsh7th/nvim-cmp',
    depends = {
        'hrsh7th/cmp-nvim-lsp',
        -- 'hrsh7th/cmp-nvim-lua',
        'hrsh7th/cmp-buffer',
        'hrsh7th/cmp-path',
        'hrsh7th/cmp-cmdline',
        'saadparwaiz1/cmp_luasnip',
        -- 'mtoohey31/cmp-fish',
        'amarakon/nvim-cmp-fonts',
        'chrisgrieser/cmp-nerdfont',
        -- 'saecki/crates.nvim',
    }
})
later(function()
    local cmp = require('cmp')
    cmp.setup({
        mapping = cmp.mapping.preset.insert({
            ["<C-p>"] = cmp.mapping.select_prev_item(),
            ["<C-n>"] = cmp.mapping.select_next_item(),
            -- ["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),
            ["<C-y>"] = cmp.mapping.confirm { select = true },
        }),
        sources = cmp.config.sources({
            { name = "nvim_lsp" },
            -- { name = "nvim_lua" },
            -- { name = "crates" },
            -- { name = "luasnip" },
            -- { name = "fish" },
            { name = "path" },
            -- { name = "orgmode" },
            { name = "nerdfont" },
            { name = "fonts",   option = { space_filler = '-' } },
        }, {
            { name = "buffer" },
        }),
    })
end)

add({ source = 'williamboman/mason.nvim' })
add({
    source = 'williamboman/mason-lspconfig.nvim',
    depends = { 'neovim/nvim-lspconfig',
        'williamboman/mason.nvim' }
})
add({
    source = 'WhoIsSethDaniel/mason-tool-installer.nvim',
    depends = { 'williamboman/mason.nvim' }
})
add({
    source = 'neovim/nvim-lspconfig',
    depends = {
        'williamboman/mason.nvim',
        'hrsh7th/nvim-cmp'
    }
})
now(function()
    local lspconfig = require('lspconfig')
    local cmp_lsp = require("cmp_nvim_lsp")
    local capabilities = vim.tbl_deep_extend(
        "force",
        {},
        vim.lsp.protocol.make_client_capabilities(),
        cmp_lsp.default_capabilities())
    require('mason').setup()
    require('mason-lspconfig').setup({
        ensure_installed = {
            'lua_ls',
            'rust_analyzer',
        },
        handlers = {
            function(server_name)
                lspconfig[server_name].setup({
                    capabilities = capabilities,
                })
            end,
            ["lua_ls"] = function()
                lspconfig.lua_ls.setup({
                    capabilities = capabilities,
                    on_init = function(client)
                        if client.workspace_folders then
                            local path = client.workspace_folders[1].name
                            if vim.loop.fs_stat(path .. '/.luarc.json') or vim.loop.fs_stat(path .. '/.luarc.jsonc') then
                                return
                            end
                        end
                        client.config.settings.Lua = vim.tbl_deep_extend('force', client.config.settings.Lua, {
                            workspace = {
                                checkThirdParty = true,
                                library = {
                                    vim.env.VIMRUNTIME,
                                    '${3rd}/love2d/library',
                                },
                            },
                        })
                    end,
                    settings = {
                        Lua = {
                            diagnostics = {
                                globals = { 'MiniDeps', 'MiniIcons' }
                            },
                        }
                    }
                })
            end
        }
    })
    require('mason-tool-installer').setup({
        auto_update = true,
        run_on_start = true,
        debounce_hours = 24, -- wait 24 hours between attempts to install/update
    })

    require('which-key').add({ { '<leader>l', group = 'Lsp', mode = 'n' } })
    vim.api.nvim_set_keymap('n', '<leader>ls', '<cmd>LspStart<CR>', { noremap = true, desc = 'Start Lsp' })

    local lsp_autocmds = vim.api.nvim_create_augroup('LspAutocmds', {})
    vim.api.nvim_set_keymap('n', '<leader>lI', '<cmd>Mason<CR>', { noremap = true, desc = 'Open Mason' })
    vim.api.nvim_create_autocmd('BufWritePre', {
        group = lsp_autocmds,
        callback = function(ev)
            vim.lsp.buf.format({ buffner = ev.buf })
        end

    })
    vim.api.nvim_create_autocmd('LspAttach', {
        group = lsp_autocmds,
        callback = function(ev)
            print("LSP Starting")
            local buf = ev.buf
            local keymap = vim.keymap.set
            -- keymap(buf, 'n', 'gd',         '<cmd>lua vim.lsp.buf.definition()<CR>',                      { noremap = true, desc = 'Goto definition' })
            -- keymap(buf, 'n', 'gI',         '<cmd>lua vim.lsp.buf.implementation()<CR>',                  { noremap = true, desc = 'Goto implementation' })
            -- keymap(buf, 'n', '<leader>lj', '<cmd>lua vim.diagnostic.goto_next({buffer=0})<CR>',          { noremap = true, desc = 'Next diagnostic message' })
            -- keymap(buf, 'n', '<leader>lk', '<cmd>lua vim.diagnostic.goto_prev({buffer=0})<CR>',          { noremap = true, desc = 'Previous diagnostic message' })
            -- keymap(buf, 'n', '<leader>ls', '<cmd>lua vim.lsp.buf.signature_help()<CR>',                  { noremap = true, desc = 'Signature help' })
            -- keymap(buf, 'n', '<leader>lq', '<cmd>lua vim.diagnostic.setloclist()<CR>',                   { noremap = true, desc = 'Set loclist' })
            -- keymap(buf, 'n', '<leader>ld', '<cmd>lua vim.lsp.buf.type_definition()<CR>',                 { noremap = true, desc = 'Goto type definition' })
            keymap('n', 'grr', vim.lsp.buf.references, { noremap = true, buffer = buf, desc = 'Open references' })
            keymap('n', '<leader>li', vim.cmd.LspInfo, { noremap = true, buffer = buf, desc = 'Lsp Info' })
            keymap('n', '<leader>la', vim.lsp.buf.code_action, { noremap = true, buffer = buf, desc = 'Code actions' })
            keymap('n', '<leader>lf', vim.lsp.buf.format, { noremap = true, buffer = buf, desc = 'Format buffer' })
            keymap('n', '<leader>lr', vim.lsp.buf.rename, { noremap = true, buffer = buf, desc = 'Rename' })
            keymap('n', '<leader>lR', '<cmd>LspRestart<CR>', { noremap = true, buffer = buf, desc = 'Restart LSP' })
            keymap('n', '<leader>ll', vim.diagnostic.open_float,
                { noremap = true, buffer = buf, desc = 'Open diagnostics' })
        end
    })
end)

add({ source = 'nvim-telescope/telescope-fzf-native.nvim', })
add({ source = 'nvim-telescope/telescope-ui-select.nvim', })
add({ source = 'scottmckendry/telescope-resession.nvim', })
add({
    source = 'stevearc/aerial.nvim',
    depends = {
        'neovim/nvim-lspconfig',
    }
})
add({
    source = 'nvim-telescope/telescope.nvim',
    depends = {
        'nvim-lua/plenary.nvim',
        'nvim-telescope/telescope-fzf-native.nvim',
        'stevearc/aerial.nvim',
        'nvim-telescope/telescope-ui-select.nvim',
        'scottmckendry/telescope-resession.nvim',
    }
})
later(function()
    require('telescope').setup({
        defaults = {
            mappings = {
                i = {
                    ['<ESC>'] = require('telescope.actions').close,
                    ['<C-n>'] = require('telescope.actions').move_selection_next,
                    ['<C-p>'] = require('telescope.actions').move_selection_previous,
                },
            },
        },
        extensions = {
            fzf = {},
            aerial = {
                show_nesting = {
                    ['_'] = false,
                    json = true,
                    yaml = true,
                }
            },
            resession = {},
        },
    })
    require('aerial').setup({
        backends = { "treesitter", "lsp" },
        filter_kind = {
            "Class",
            "Enum",
            "Function",
            "Interface",
            "Module",
            "Method",
            "Struct",
        },
        autojump = true,
        close_on_select = true,
    })

    require('telescope').load_extension('fzf')
    require('telescope').load_extension('aerial')
    require('telescope').load_extension('resession')

    require('which-key').add({
        { '<leader>t', group = 'Telescope', mode = 'n' },
        { '<leader>e', group = 'Edit',      mode = 'n' },
    })
    local keymap = vim.keymap.set
    keymap('n', '<leader>tf', "<cmd>Telescope find_files theme=ivy<CR>", { noremap = true, desc = 'Find files' })
    keymap('n', '<leader>tb', "<cmd>Telescope buffers theme=ivy<CR>", { noremap = true, desc = 'List buffers' })
    keymap('n', '<leader>th', "<cmd>Telescope help_tags theme=ivy<CR>", { noremap = true, desc = 'Nvim help' })
    keymap('n', '<leader>tm', "<cmd>Telescope man_pages theme=ivy<CR>", { noremap = true, desc = 'Man pages' })
    keymap('n', '<leader>tk', "<cmd>Telescope keymaps theme=ivy<CR>", { noremap = true, desc = 'List keymaps' })
    keymap('n', '<leader>tg', "<cmd>Telescope live_grep theme=ivy<CR>", { noremap = true, desc = 'Directory grep' })
    keymap('n', '<leader>ta', "<cmd>Telescope aerial theme=ivy<CR>", { noremap = true, desc = 'Aerial' })
    keymap('n', '<leader>ts', "<cmd>Telescope resession theme=ivy<CR>", { noremap = true, desc = 'Nvim sessions' })
    keymap('n', '<leader>tG', "<cmd>Telescope current_buffer_fuzzy_find theme=ivy<CR>",
        { noremap = true, desc = 'Grep current buffer' })

    keymap('n', '<leader>en', function()
        require('telescope.builtin').find_files { cwd = vim.fn.stdpath('config') }
    end, { noremap = true, desc = 'Edit nvim config' })
    -- keymap('n', 'ec', function() require('telescope.builtin').fd { cwd = vim.fn.stdpath('config_dirs') } end, { noremap = true, desc = 'Edit configs' })
end)

add({ source = 'stevearc/resession.nvim' })
later(function()
    require('resession').setup({
        autosave = {
            enabled = true,
            interval = 60,
            notify = true,
        },
    })
    local keymap = vim.keymap.set
    keymap('n', '<leader>ss', require('resession').save, { noremap = true, desc = 'Save session' })
    keymap('n', '<leader>sd', require('resession').delete, { noremap = true, desc = 'Delete session' })
    vim.api.nvim_create_autocmd("VimLeavePre", {
        callback = function()
            -- Always save a special session named "last"
            require('resession').save("last")
        end,
    })
end)

add({ source = 'Canop/nvim-bacon' })
later(function()
    require('bacon').setup()
    local keymap = vim.keymap.set
    keymap('n', '<leader>bb', ':BaconLoad<CR>:w<CR>:BaconNext<CR>',
        { noremap = true, desc = 'Navigate to next bacon location' })
    keymap('n', '<leader>bl', ':BaconList<CR>', { noremap = true, desc = 'Open bacon locations list' })
    keymap('n', '<leader>bn', ':BaconNext<CR>', { noremap = true, desc = 'Next bacon locations' })
    keymap('n', '<leader>bp', ':BaconPrevious<CR>', { noremap = true, desc = 'Previous bacon locations' })
end)

add({ source = 'https://git.sr.ht/~sircmpwn/hare.vim' })

add({
    source = 'https://github.com/folke/trouble.nvim',
    depends = { 'echasnovski/mini.nvim' },
})
later(function()
    require('trouble').setup()
    local keymap = vim.keymap.set
    keymap('n', '<leader>xx', '<cmd>Trouble diagnostics toggle<CR>', { noremap = true, desc = 'Diagnostics (Trouble)' })
    keymap('n', '<leader>xl', '<cmd>Trouble loclist toggle<CR>', { noremap = true, desc = 'Location List (Trouble)' })
    keymap('n', '<leader>xq', '<cmd>Trouble qflist toggle<CR>', { noremap = true, desc = 'Quickfix List (Trouble)' })
end)
