vim.opt.guifont = 'ComicShannsMono Nerd Font:h14'
vim.g.neovide_padding_top = 5
vim.g.neovide_padding_bottom = 5
vim.g.neovide_padding_left = 5
vim.g.neovide_padding_right = 5
vim.g.neovide_hide_mouse_when_typing = true
vim.g.neovide_confirm_quit = true
vim.g.neovide_cursor_animation_length = 0
