local user_autocmds = vim.api.nvim_create_augroup('MyAutocmds', { clear = true })
local create_autocmd = vim.api.nvim_create_autocmd

create_autocmd('TextYankPost', {
    group = user_autocmds,
    pattern = "*",
    callback = function()
        vim.highlight.on_yank({ timeout = 200 })
    end
})

-- create_autocmd({ 'BufRead', 'BufNewFile' }, {
--     group = user_autocmds,
--     pattern = { '*.lpm' },
--     callback = function()
--         vim.cmd([[setfiletype racket]])
--     end
-- })

create_autocmd({ 'BufWritePre' }, {
    group = user_autocmds,
    pattern = '*',
    command = [[%s/\s\+$//e]],
})

create_autocmd('TermOpen', {
    group = user_autocmds,
    callback = function()
        vim.opt.number = false
        vim.opt.relativenumber = false
    end
})
