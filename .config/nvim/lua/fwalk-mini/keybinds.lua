vim.api.nvim_set_keymap("", "<Space>", "<Nop>", {})
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

local opts = { noremap = true, silent = true }

-- Shorten function name
local keymap = vim.keymap.set

keymap('n', 'gc', '<Nop>', {})

-- Better window navigation
keymap('n', '<C-h>', '<C-w>h', opts)
keymap('n', '<C-j>', '<C-w>j', opts)
keymap('n', '<C-k>', '<C-w>k', opts)
keymap('n', '<C-l>', '<C-w>l', opts)

-- Navigate buffers
keymap('n', '<S-l>', ':bnext<CR>', opts)
keymap('n', '<S-h>', ':bprevious<CR>', opts)

-- Better page jumps
keymap('n', '<C-d>', '<C-d>zz', opts)
keymap('n', '<C-u>', '<C-u>zz', opts)

-- Better search jumps
keymap('n', 'n', 'nzz', opts)
keymap('n', 'N', 'Nzz', opts)

-- Remove highlighting after search
keymap('n', '<CR>', ':noh<CR>', opts)

-- Stay in indent mode
keymap('v', '<', '<gv', opts)
keymap('v', '>', '>gv', opts)

-- Move text up and down
keymap('v', 'p', '"_dP', opts)

keymap('n', ';', ':', { noremap = true })

keymap('t', '<ESC><ESC>', '<C-\\><C-n>', { noremap = true })

-- When text is wrapped, move by terminal rows, not lines, unless a count is provided
vim.cmd([[
nnoremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')
nnoremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')
]])

MiniDeps.later(function()
    require('which-key').add({
        { '<leader>s', group = 'Split', mode = 'n' }
    })
end)
keymap('n', '<leader>sv', '<cmd>vsplit<CR>', { noremap = true, desc = 'Vertical split' })
keymap('n', '<leader>sh', '<cmd>split<CR>', { noremap = true, desc = 'Horizontal split' })

MiniDeps.later(function()
    require('which-key').add({
        { '<leader>b', group = 'Buffer',   mode = 'n' },
        { '<leader>f', group = 'Files',    mode = 'n' },
        { '<leader>c', group = 'Quickfix', mode = 'n' },
    })
end)
keymap('n', '<leader>bc', '<C-w>c', { noremap = true, desc = 'Close split' })
keymap('n', '<leader>bo', '<C-w>o', { noremap = true, desc = 'Close all other splits' })
keymap('n', '<leader>be', '<cmd>enew<CR>', { noremap = true, desc = 'Open empty buffer' })
keymap('n', '<leader>bq', '<cmd>bd<CR>', { noremap = true, desc = 'Close buffer' })
keymap('n', '<leader>bd', '<cmd>ls<CR>:delete<Space>', { noremap = true, desc = 'Delete buffer' })
keymap('n', '<leader>fs', '<cmd>up<CR>', { noremap = true, desc = 'Write buffer' })
keymap('n', '<leader><leader>', '<cmd>up<CR>', { noremap = true, desc = 'Write buffer' })
keymap('n', '<leader>fq', '<cmd>q!<CR>', { noremap = true, desc = 'Quit without saving' })
--keymap('n', '<leader>fg', '<cmd>edit <cfile><CR>', opts)
keymap('n', '<leader>fa', '<cmd>saveas<Space>', { noremap = true, desc = 'Save buffer as' })
keymap('n', '<leader>fo', '<cmd>e<Space>', { noremap = true, desc = 'Open file' })
keymap('n', '<leader>cc', '<cmd>copen<CR>', { noremap = true, desc = 'Open quickfix list' })
keymap('n', '<leader>cn', '<cmd>cnext<CR>', { noremap = true, desc = 'Next quickfix' })
keymap('n', '<leader>cp', '<cmd>cprev<CR>', { noremap = true, desc = 'Previous quickfix' })
keymap('n', '<leader>cl', vim.diagnostic.setqflist, { noremap = true, desc = 'Fill quickfix list' })
keymap('n', '<leader>cq', '<cmd>cclose<CR>', { noremap = true, desc = 'Close quickfix list' })

keymap('n', '<leader>Op', '<cmd>e ~/PROMPT<CR>', { noremap = true, desc = 'Open ~/PROMPT' })
-- keymap('n', '<leader>mm', '<cmd>BBuildTerm<CR>', opts)
