vim.opt.conceallevel = 2
vim.opt.concealcursor = 'nc'

vim.keymap.set(
    'n',
    '<leader>oC',
    function() vim.opt.conceallevel = ((vim.opt.conceallevel:get() + 2) % 4) end,
    { noremap = true, desc = 'Toggle conceal' })
