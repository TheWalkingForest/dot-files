vim.filetype.add({
    extension = {
        hy = 'hy',
        lpm = 'mlisp',
        bny = 'bunny',
    },
})
