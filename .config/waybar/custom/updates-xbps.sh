#!/usr/bin/env bash

updates_xbps=$(xbps-install -Mun 2> /dev/null | wc -l)
flatpak=$(flatpak update | wc -l)

if [ $((flatpak - 3)) -eq 0 ]; then
    updates_flatpak=0
else
    updates_flatpak=$(echo "$flatpak" | tail -n +4 | head -n -2 | wc -l)
fi
updates=$((updates_xbps + updates_nix + updates_flatpak))

if [ -n "$updates" ] && [ "$updates" -ge 0 ]; then
    printf \
        '{"text": "%s", "alt": "has-updates", "tooltip": "xbps: %s, nix: %s, flatpak: %s", "class": "updates" }' \
        "$updates" "$updates_xbps" "$updates_flatpak"
else
    printf \
        '{"text": "%s", "alt": "updated", "tooltip": "All up to date", "class": "updates" }' \
        "$updates"
fi
