#!/usr/bin/env bash

CONFIG_DP1_FILE="config-DP-1.jsonc"
CONFIG_DP2_FILE="config-DP-2.jsonc"
STYLE_FILE="style.css"

get_hash() {
    sha256sum "$1" | awk '{print $1}'
}

CONFIG_HASH="$(get_hash $CONFIG_DP1_FILE)$(get_hash $CONFIG_DP2_FILE)"
STYLE_HASH=$(get_hash $STYLE_FILE)

waybar 2>/dev/null &
while [ 1 -eq 1 ]; do
    if [ "$CONFIG_HASH" != "$(get_hash $CONFIG_DP1_FILE)$(get_hash $CONFIG_DP2_FILE)" ] || \
       [ "$STYLE_HASH" != "$(get_hash $STYLE_FILE)" ]
    then
        pkill waybar
        CONFIG_HASH="$(get_hash $CONFIG_DP1_FILE)$(get_hash $CONFIG_DP2_FILE)"
        STYLE_HASH=$(get_hash $STYLE_FILE)
        waybar -b 0 -c $CONFIG_DP1_FILE 2>/dev/null &
        waybar -b 1 -c $CONFIG_DP2_FILE 2>/dev/null &
    fi
    sleep 1s
done
