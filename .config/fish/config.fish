#
#  ███████╗██╗███████╗██╗  ██╗
#  ██╔════╝██║██╔════╝██║  ██║
#  █████╗  ██║███████╗███████║
#  ██╔══╝  ██║╚════██║██╔══██║
#  ██║     ██║███████║██║  ██║
#  ╚═╝     ╚═╝╚══════╝╚═╝  ╚═╝
#

# if not running interactively, dont do anything
if not status is-interactive
    return
end
