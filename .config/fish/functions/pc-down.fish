function pc-down --wraps='podman-compose down' --description 'alias pc-down=podman-compose down'
  podman-compose down $argv
        
end
