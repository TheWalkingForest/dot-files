function bw --description 'alias bw=bacon --watch src -e' --argument job
	bacon -e -s -W --watch ./src -j "$job"
end
