function usvstatus --wraps='SVDIR=~/.service sv status' --description 'alias usvstatus=SVDIR=~/.service sv status'
  if ! test -d "$SV_SERVICE_HOME"/"$argv"
    echo "Error: service $argv does not exist"
    return
  end
    SVDIR="$SV_SERVICE_HOME" sv status $argv
end
