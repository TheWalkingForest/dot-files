function tsup --wraps='tailscale up' --description 'alias tsup=tailscale up'
  tailscale up $argv
        
end
