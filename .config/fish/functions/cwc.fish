function cwc --wraps='cargo watch -q -x clippy' --description 'alias cwc cargo watch -q -x clippy'
  cargo watch -q -x clippy $argv
end
