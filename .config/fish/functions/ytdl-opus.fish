function ytdl-opus --wraps='yt-dlp --extract-audio --audio-format opus ' --description 'alias ytdl-opus=yt-dlp --extract-audio --audio-format opus '
  yt-dlp --extract-audio --audio-format opus  $argv
end
