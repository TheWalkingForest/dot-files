function ytdl-vorbis --wraps='yt-dlp --extract-audio --audio-format vorbis ' --description 'alias ytdl-vorbis=yt-dlp --extract-audio --audio-format vorbis '
  yt-dlp --extract-audio --audio-format vorbis  $argv
end
