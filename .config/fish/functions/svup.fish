function svup --wraps='sv up' --description 'alias svup=sv up'
  if ! test -d "/var/service/$argv"
    echo "Error: service $argv does not exist"
    return
  end
  if command -v  doas &> /dev/null
    doas sv up $argv
  else
    sudo sv up $argv
  end
end
