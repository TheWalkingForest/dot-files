function ytdl-best --wraps='yt-dlp --extract-audio --audio-format best ' --description 'alias ytdl-best=yt-dlp --extract-audio --audio-format best '
  yt-dlp --extract-audio --audio-format best  $argv
end
