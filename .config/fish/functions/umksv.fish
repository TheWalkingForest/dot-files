function umksv --description "create new user service template"
    if not test -d "$USV_USER_DIR"
        echo "User runnit service directory does not exist"
        return
    end

    for i in $argv
        set NEW_SERVICE_DIR "$USV_USER_DIR"/"$i"

        if test -d "$NEW_SERVICE_DIR"
            echo "Service with the name '$i' already exists"
            return
        end

        mkdir -p "$NEW_SERVICE_DIR/log"

        printf "#!/bin/sh\n\n[ -r ./conf ] && . ./conf\n\nexec $i \${OPTS:-} 2>&1" > "$NEW_SERVICE_DIR"/run
        printf "#!/bin/sh\n\nexec logger -t $i" > "$NEW_SERVICE_DIR"/log/run
        printf "# -*- mode: sh; -*-\n\nOPTS=" > "$NEW_SERVICE_DIR"/conf

        chmod +x "$NEW_SERVICE_DIR"/run
        chmod +x "$NEW_SERVICE_DIR"/log/run

        touch "$NEW_SERVICE_DIR"/run
        touch "$NEW_SERVICE_DIR"/conf
        touch "$NEW_SERVICE_DIR"/log/run
    end
end
