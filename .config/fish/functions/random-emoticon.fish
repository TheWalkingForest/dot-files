function random-emoticon --description "prints random emoticon"
    set emoticons "$(set_color red)V=(° °)=V$(set_color normal)" \
                  "ಠ_ಠ" \
                  "(◕‿◕$(set_color blue)✿$(set_color normal))" \
                  "⎛⎝(•ⱅ•)⎠⎞" \
                  "$(set_color brown)ʕ •ᴥ•ʔ$(set_color normal)" \
                  "(/◕ヮ◕)/"

    set len (count $emoticons)
    set rand (random)
    set idx (math "($rand % $len) + 1")

    echo "$emoticons[$idx]"
end
