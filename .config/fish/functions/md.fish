function md --wraps=mdcat --description 'alias md=mdcat'
  if command -v glow &> /dev/null
    glow $argv
  else
    cat $argv
  end
end
