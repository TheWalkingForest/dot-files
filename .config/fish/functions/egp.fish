function egp --wraps='egrep --color=always' --description 'alias egp=egrep --color=always'
  egrep --color=always $argv
        
end
