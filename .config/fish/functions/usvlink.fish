function usvlink --description 'Enable service' --argument service
  if ! test -d "$SV_USER_HOME"/"$argv"
    echo "Error: service $argv does not exist"
    return
  end
    ln -sf "$SV_USER_HOME"/"$service" "$SV_SERVICE_HOME"/"$service"
end
