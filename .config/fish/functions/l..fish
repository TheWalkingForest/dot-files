function l. --wraps=eza\ -labF\ --git\ --group-directories-first\ --icons\ \|\ rg\ \"^\\.\" --description alias\ l.=eza\ -labF\ --git\ --group-directories-first\ --icons\ \|\ rg\ \"^\\.\"
  eza -labF --git --group-directories-first --icons | rg "^\." $argv
end
