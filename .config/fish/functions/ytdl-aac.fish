function ytdl-aac --wraps='yt-dlp --extract-audio --audio-format aac ' --description 'alias ytdl-aac=yt-dlp --extract-audio --audio-format aac '
  yt-dlp --extract-audio --audio-format aac  $argv
end
