function svdown --wraps='sv down' --description 'alias svdown=sv down'
  if ! test -d "/var/service/$argv"
    echo "Error: service $argv does not exist"
    return
  end
  if command -v  doas &> /dev/null
    doas sv down $argv
  else
    sudo sv down $argv
  end
end
