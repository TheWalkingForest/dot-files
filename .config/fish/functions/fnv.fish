function fnv --description 'nvim with fzf' --argument-names dir
    if set -q dir; and not test -n "$dir"
        set dir '.'
    else
        echo "'$dir'"
    end
    set res (find . -type f | fzf --preview 'bat -p --color always {}')
    if set -q res; and not test -n "$res"
        echo "$res"
    else
        nv $res
    end
end
