function tsget --wraps='tailscale file get ~/Downloads' --description 'alias tsget=tailscale file get ~/Downloads'
  tailscale file get ~/Downloads $argv
        
end
