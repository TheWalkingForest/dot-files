function ytdl-wav --wraps='yt-dlp --extract-audio --audio-format wav ' --description 'alias ytdl-wav=yt-dlp --extract-audio --audio-format wav '
  yt-dlp --extract-audio --audio-format wav  $argv
end
