function ytdl-bestav --wraps='yt-dlp -f bestvideo+bestaudio ' --description 'alias ytdl-bestav=yt-dlp -f bestvideo+bestaudio '
  yt-dlp -f bestvideo+bestaudio  $argv
end
