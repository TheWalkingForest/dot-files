function ytdl-mkv --wraps='yt-dlp --extract-audio --audio-format mkv ' --description 'alias ytdl-mkv=yt-dlp -f mkv '
  yt-dlp -f mkv $argv
end
