function llt --wraps='eza -lbF --tree --git-ignore --git --color=always --group-directories-first --icons' --description 'alias lst=eza -lbF --tree --git-ignore --git --color=always --group-directories-first --icons'
  eza -lbF --tree --git-ignore --git --color=always --group-directories-first --icons $argv
end
