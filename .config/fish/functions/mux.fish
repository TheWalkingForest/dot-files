function mux --wraps=tmuxinator --description 'alias mux=tmuxinator'
  if command -v tmuxinator &> /dev/null
    tmuxinator $argv
  else
    tmux new-session -A -t "$(basename $PWD)" $argv
  end
end
