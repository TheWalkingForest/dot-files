function usvdown --wraps='SVDIR=~/.service sv down' --description 'alias usvdown=SVDIR=~/.service sv down'
  if ! test -d "$SV_SERVICE_HOME"/"$argv"
    echo "Error: service $argv does not exist"
    return
  end
  SVDIR="$SV_SERVICE_HOME" sv down $argv
end
