function svlist --wraps='ls /etc/sv' --description 'alias svlist=ls /etc/sv'
  command ls /etc/sv $argv
end
