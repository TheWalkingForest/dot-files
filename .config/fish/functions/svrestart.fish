function svrestart --wraps='sv restart' --description 'alias svrestart=sv restart'
  if ! test -d "/var/service/$argv"
    echo "Error: service $argv does not exist"
    return
  end
  if command -v  doas &> /dev/null
    doas sv restart $argv
  else
    sudo sv restart $argv
  end
end
