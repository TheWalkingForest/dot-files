function ytdl-mp3 --wraps='yt-dlp --extract-audio --audio-format mp3 ' --description 'alias ytdl-mp3=yt-dlp --extract-audio --audio-format mp3 '
  yt-dlp --extract-audio --audio-format mp3  $argv
end
