function init_project --argument lang --description "Skeleton for projects"
    switch $lang
    case "c"
        mkdir include src
        curl "https://gitlab.com/-/snippets/3603639/raw/main/C%20Makefile" > Makefile
        git init
        echo "obj/" >> .gitignore
        printf "#include <stdio.h>\n\nint main() {\n    printf(\"Hello world\");\n    return 0;\n}" > src/main.c
    case "cpp" "cc"
        mkdir include src
        touch src/main.cpp
        curl "https://gitlab.com/-/snippets/3603639/raw/main/C++%20Makefile" > Makefile
        echo "obj/" >> .gitignore
        printf "#include <iostream>\n\nint main() {\n    std::cout << \"Hello world\" << std::endl;\n    return 0;\n}" > src/main.cpp
    case "*"
        echo "$lang" not supported
    end
end
