function cwr "--wraps=cargo watch -q -B 1 -E 'RUST_LOG=debug' -x run" --description "alias cwr cargo watch -q -B 1 -E 'RUST_LOG=debug' -x run"
  cargo watch -q -B 1 -E 'RUST_LOG=debug' -x run $argv
end
