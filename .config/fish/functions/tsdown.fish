function tsdown --wraps='tailscale down' --description 'alias tsdown=tailscale down'
  tailscale down $argv
        
end
