function bk --argument src --description 'makes backup of the given file/dir'
    cp -r "$src" "$src.bk"
end
