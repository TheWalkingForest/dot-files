function muxo --wraps='tmuxinator open --suppress-tmux-version-warning' --wraps='tmuxinator start --suppress-tmux-version-warning' --description 'alias muxo=tmuxinator start --suppress-tmux-version-warning'
  tmuxinator start --suppress-tmux-version-warning $argv
end
