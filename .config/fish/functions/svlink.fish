function svlink --description 'Enable service' --argument service
  if ! test -d "/var/service/$argv"
    echo "Error: service $argv does not exist"
    return
  end
  if command -v  doas &> /dev/null
    doas ln -sf /etc/sv/"$service" /var/service/"$service"
  else
    sudo ln -sf /etc/sv/"$service" /var/service/"$service"
  end
end
