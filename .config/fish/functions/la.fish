function la --wraps='eza -labF --git --color=always --group-directories-first --icons' --description 'alias la=eza -labF --git --color=always --group-directories-first --icons'
  eza -labF --git --color=always --group-directories-first --icons $argv
end
