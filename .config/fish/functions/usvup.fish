function usvup --wraps='SVDIR=~/.service sv up' --description 'alias usvup=SVDIR=~/.service sv up'
  if ! test -d "$SV_SERVICE_HOME"/"$argv"
    echo "Error: service $argv does not exist"
    return
  end
    SVDIR="$SV_SERVICE_HOME" sv up $argv
end
