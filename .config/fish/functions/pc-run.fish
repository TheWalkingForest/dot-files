function pc-run --wraps='podman-compose run' --description 'alias pc-run=podman-compose run'
  podman-compose run $argv
        
end
