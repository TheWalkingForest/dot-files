function svstatus --wraps='sv status' --description 'alias svstatus=sv status'
  if ! test -d "/var/service/$argv"
    echo "Error: service $argv does not exist"
    return
  end
  if command -v  doas &> /dev/null
    doas sv status $argv
  else
    sudo sv status $argv
  end
end
