function usvunlink --description 'Disable service' --argument service
  if ! test -d "$SV_SERVICE_HOME"/"$argv"
    echo "Error: service $argv does not exist"
    return
  end
    rm -f "$SV_SERVICE_HOME"/"$service"
end
