function muxo --wraps='tmuxinator open --suppress-tmux-version-warning' --wraps='tmuxinator start --suppress-tmux-version-warning' --description 'alias muxo=tmuxinator start --suppress-tmux-version-warning'
  tmuxinator new --local $argv
end
