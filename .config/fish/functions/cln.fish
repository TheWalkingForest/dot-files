function cln --wraps='git clone' --description 'alias cln git clone'
  git clone $argv
        
end
