function show-path --wraps=echo\ \$PATH\ \|\ sed\ \"s/\ /\\n/g\" --description alias\ show-path=echo\ \$PATH\ \|\ sed\ \"s/\ /\\n/g\"
  echo $PATH | sed "s/ /\n/g" $argv
        
end
