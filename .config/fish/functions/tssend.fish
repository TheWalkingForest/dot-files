function tssend --wraps='tailscale file cp' --description 'alias tssend=tailscale file cp'
  tailscale file cp $argv
        
end
