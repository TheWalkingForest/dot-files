function svunlink --description 'Disable service' --argument service
  if ! test -d "/var/service/$argv"
    echo "Error: service $argv does not exist"
    return
  end
  if command -v  doas &> /dev/null
    doas rm -f /var/service/"$service"
  else
    sudo rm -f /var/service/"$service"
  end
end
