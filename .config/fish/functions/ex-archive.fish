function  ex-archive --description 'extract various archives' --argument-names filename
    if test -f "$filename"
        switch "$filename"
        case '*.tar.bz2' '*.tbz2' '*.tar.bz' '*.tbz'
            tar xjf "$filename"
        case '*.tar.gz' '*.tgz'
            tar xzf "$filename"
        case '*.tar.xz'
            tar xf "$filename"
        case '*.tar.zst'
            unzstd "$filename"
        case '*.tar.lz' '*.tar.lzip'
            tar --lzip -xf "$filename"  
        case '*.tar'
            tar xf  "$filename"
        case '*.bz2' '*.bz'
            bunzip2 "$filename"
        case '*.rar'
            unrar x "$filename"
        case '*.gz'
            gunzip  "$filename"
        case '*.zip'
            unzip   "$filename"
        case '*.Z'
            uncompress "$filename"
        case '*.7z'
            7z x   "$filename"
        case '*.deb'
            ar x   "$filename"
        case '*'
            echo "'$filename' cannot be extracted via ex-archive"
        end
    else
        echo "'$filename' is not a valid file"
    end
end

