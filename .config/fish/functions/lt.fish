function lt --wraps='eza --tree --git-ignore --color=always --group-directories-first --icons' --description 'alias lt=eza --tree --git-ignore --color=always --group-directories-first --icons'
  eza --tree --git-ignore --color=always --group-directories-first --icons $argv
end
