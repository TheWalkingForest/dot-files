function ytdl-m4a --wraps='yt-dlp --extract-audio --audio-format m4a ' --description 'alias ytdl-m4a=yt-dlp --extract-audio --audio-format m4a '
  yt-dlp --extract-audio --audio-format m4a  $argv
end
