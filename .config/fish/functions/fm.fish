function fm --wraps=vifm --description 'alias fm=vifm'
  if set -q argv; and not test -n "$argv"
    vifm . .
  else
    vifm $argv
  end
end
