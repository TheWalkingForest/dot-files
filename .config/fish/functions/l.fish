function l --wraps='eza --color=always --group-directories-first --icons' --description 'alias l=eza --color=always --group-directories-first --icons'
  eza --color=always --group-directories-first --icons $argv
end
