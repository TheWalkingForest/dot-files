function tsstat --wraps='tailscale status' --description 'alias tsstat=tailscale status'
  tailscale status $argv
        
end
