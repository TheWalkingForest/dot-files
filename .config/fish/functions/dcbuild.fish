function dcbuild --wraps='docker-compose build' --description 'alias dcbuild=docker-compose build'
  docker-compose build $argv
        
end
