function fgp --wraps='fgrep --color=always' --description 'alias fgp=fgrep --color=always'
  fgrep --color=always $argv
        
end
