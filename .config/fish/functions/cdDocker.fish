function cdDocker --wraps='cd ~/Projects/Docker' --description 'alias cdDocker=cd ~/Projects/Docker'
  cd ~/Projects/Docker $argv
        
end
