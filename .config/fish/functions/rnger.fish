function rnger --wraps='TERM=xterm-kitty ranger' --description 'alias rnger=TERM=xterm-kitty ranger'
  TERM=xterm-kitty ranger $argv
        
end
