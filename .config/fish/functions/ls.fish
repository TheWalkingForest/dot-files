function ls --wraps='eza -lbF --git --color=always --group-directories-first --icons' --description 'alias ls=eza -lbF --git --color=always --group-directories-first --icons'
  eza -lbF --git --color=always --group-directories-first --icons $argv
end
