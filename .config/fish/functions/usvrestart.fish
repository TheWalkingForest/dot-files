function usvrestart --wraps='SVDIR=~/.service sv restart' --description 'alias usvrestart=SVDIR=~/.service sv restart'
  if ! test -d "$SV_SERVICE_HOME"/"$argv"
    echo "Error: service $argv does not exist"
    return
  end
    SVDIR="$SV_SERVICE_HOME" sv restart $argv
end
