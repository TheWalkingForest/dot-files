function usvlist --wraps='ls ~/.config/sv' --description 'alias usvlist=ls ~/.config/sv'
    command ls "$SV_USER_HOME" $argv
end
