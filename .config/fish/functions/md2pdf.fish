function md2pdf --description 'converts md to pdf' --argument file
    set pdf $(echo $file | sed s/.md/.pfd/)
    pandoc "$file" -s -o "$pdf"
end
