function ll --wraps='eza -lmhgbF --git --color=always --group-directories-first --icons' --description 'alias ll=eza -lmhgbF --git --color=always --group-directories-first --icons'
  eza -lmhgbF --git --color=always --group-directories-first --icons $argv
end
