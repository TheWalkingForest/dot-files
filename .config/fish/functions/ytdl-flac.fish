function ytdl-flac --wraps='yt-dlp --extract-audio --audio-format flac ' --description 'alias ytdl-flac=yt-dlp --extract-audio --audio-format flac '
  yt-dlp   --extract-audio --audio-format flac  $argv
end
