function svrunning --wraps='ls /var/service' --description 'alias svruning=ls /var/service'
  command ls /var/service $argv
end
