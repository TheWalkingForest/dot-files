set ZIG_BIN "$HOME/opt/zig"

if test -d "$ZIG_BIN"; and not contains -- "$ZIG_BIN" $PATH
    fish_add_path --path --append "$ZIG_BIN"
end
