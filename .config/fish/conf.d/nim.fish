set -x NIMBLE_DIR "$XDG_DATA_HOME"/nimble
set NIMBLE_BIN "$NIMBLE_DIR/bin"

if test -d $NIMBLE_BIN; and not contains -- $NIMBLE_BIN $PATH
    fish_add_path --path --append $NIMBLE_BIN
end
