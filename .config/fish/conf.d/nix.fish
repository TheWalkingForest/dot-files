# set -x NIX_USER_PROFILE_DIR "/nix/var/nix/profiles/per-user/$USER"
# set -x NIX_PROFILES "/nix/var/nix/profiles/default $HOME/.nix-profile"
#
# mkdir -m 0755 -p $NIX_USER_PROFILE_DIR
# if ! test -O "$NIX_USER_PROFILE_DIR"
#     echo "WARNING: bad ownership on $NIX_USER_PROFILE_DIR" >&2
# end
#
# if test -w $HOME
#     if not test -L $HOME/.nix-profile
#         if test "$USER" != root
#             ln -s $NIX_USER_PROFILE_DIR/profile $HOME/.nix-profile
#         else
#             # Root installs in the system-wide profile by default.
#             ln -s /nix/var/nix/profiles/default $HOME/.nix-profile
#         end
#     end
#
#     # Subscribe the root user to the NixOS channel by default.
#     if test "$USER" = root; and not test -e $HOME/.nix-channels
#         echo "https://nixos.org/channels/nixpkgs-unstable nixpkgs" > $HOME/.nix-channels
#     end
#
#     # Create the per-user garbage collector roots directory.
#     set NIX_USER_GCROOTS_DIR /nix/var/nix/gcroots/per-user/$USER
#     mkdir -m 0755 -p $NIX_USER_GCROOTS_DIR
#     if not test -O "$NIX_USER_GCROOTS_DIR"
#         echo "WARNING: bad ownership on $NIX_USER_GCROOTS_DIR" >&2
#     end
#
#     # Set up a default Nix expression from which to install stuff.
#     if not test -e $HOME/.nix-defexpr -o -L $HOME/.nix-defexpr
#         rm -f $HOME/.nix-defexpr
#         mkdir -p $HOME/.nix-defexpr
#         if "$USER" != root
#             ln -s /nix/var/nix/profiles/per-user/root/channels $HOME/.nix-defexpr/channels_root
#         end
#     end
# end
#
# if test -d /nix/var/nix/profiles/default/bin; and not contains -- /nix/var/nix/profiles/default/bin $PATH
#     # set -x PATH /nix/var/nix/profiles/default/bin:$PATH
#     fish_add_path --path --append "/nix/var/nix/profiles/default/bin"
# end
#
# if test -d $HOME/.nix-profile/bin; and not contains -- $HOME/.nix-profile/bin $PATH
#     fish_add_path --path --append "$HOME/.nix-profile/bin"
#     # set -x PATH
# end
#
# set -x NIX_PATH $HOME/.nix-defexpr/channels
#
# set -x NIX_REMOTE daemon
