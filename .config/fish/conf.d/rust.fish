set -x RUSTUP_HOME "$XDG_DATA_HOME/rustup"
set -x CARGO_HOME "$XDG_DATA_HOME/cargo"
set CARGO_BIN "$CARGO_HOME/bin"

if test -d "$CARGO_BIN"; and not contains -- "$CARGO_BIN" $PATH
    fish_add_path --path --append "$CARGO_BIN"
end
