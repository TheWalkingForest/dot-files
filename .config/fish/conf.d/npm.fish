set -x NPM_CONFIG_USERCONFIG "$XDG_CONFIG_HOME"/npm/npmrc
set -x NPM_CONFIG_INIT_MODULE "$XDG_CONFIG_HOME"/npm/config/npm-init.js
set -x NPM_CONFIG_CACHE "$XDG_CACHE_HOME"/npm
set -x NPM_CONFIG_TMP "$XDG_RUNTIME_DIR"/npm

set -x NODE_REPL_HISTORY "$XDG_DATA_HOME"/node_repl_history
