set fish_greeting                      # Supresses fish's intro message
fish_vi_key_bindings

set -xU XDG_CONFIG_HOME "$HOME"/.config
set -xU XDG_CACHE_HOME "$HOME"/.cache
set -xU XDG_STATE_HOME "$HOME"/.local/state
set -xU XDG_DATA_HOME "$HOME"/.local/share
set -xU XDG_SCREENSHOTS_DIR "$HOME"/Pictures/Screenshots

set -x SV_USER_HOME "$XDG_CONFIG_HOME"/sv
set -x SV_SERVICE_HOME "$HOME"/.service
# set -x TERM "screen-256color"

if command -v nvim &> /dev/null
    set -x EDITOR "nvim"
else if command -v vim &> /dev/null
    set -x EDITOR "vim"
else
    set -x EDITOR "vi"
end

if command -v emacs &> /dev/null
    set -x VISUAL "emacs"
end

if command -v moar &> /dev/null
    set -x MANPAGER "moar -no-linenumbers"
end

set -x SXHKD_SHELL /usr/bin/bash

# AUTOCOMPLETE AND HIGHLIGHT COLORS
set fish_color_normal brcyan
set fish_color_autosuggestion '#7d7d7d'
set fish_color_command brcyan
set fish_color_error '#ff6c6b'
set fish_color_param brcyan
