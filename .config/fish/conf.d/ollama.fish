set OLLAMA_DIR "$HOME/opt/ollama"

if test -d "$OLLAMA_DIR"; and not contains -- "$OLLAMA_DIR/bin" $PATH
    fish_add_path --path --append "$OLLAMA_DIR/bin"
end
