# Only source this once.
if [ -n "$__HM_SESS_VARS_SOURCED" ]
  return
end
set -gx __HM_SESS_VARS_SOURCED '1'
set -gx LOCALE_ARCHIVE_2_27 '/nix/store/9kjxsvc6ja2iwcj85ki815n94557nd06-glibc-locales-2.38-44/lib/locale/locale-archive'
