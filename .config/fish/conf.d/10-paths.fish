# Add "$HOME/.bin to PATH
if test -d "$HOME/.bin"; and  not contains -- "$HOME/.bin" $PATH
    fish_add_path --path --append "$HOME/.bin"
end

# Add "$HOME/.local/bin to PATH
if test -d "$HOME/.local/bin"; and not contains -- "$HOME/.local/bin" $PATH
    fish_add_path --path --append "$HOME/.local/bin"
end

# Add "$HOME/.scripts to PATH
if test -d "$HOME/.scripts"; and not contains -- "$HOME/.scripts" $PATH
    fish_add_path --path --append "$HOME/.scripts"
end

if test -d "$HOME/Applications"; and not contains -- "$HOME/Applications" $PATH
    fish_add_path --path --append "$HOME/Applications"
end
