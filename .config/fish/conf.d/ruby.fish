set -x GEM_HOME "$XDG_DATA_HOME"/gem
set -x GEM_SPEC_CACHE "$XDG_CACHE_HOME"/gem
set -x GEM_BIN $GEM_HOME/bin

if test -d "$GEM_BIN"; and not contains -- "$GEM_BIN" $PATH
    fish_add_path --path --append "$GEM_BIN"
end

set -x BUNDLE_USER_CONFIG "$XDG_CONFIG_HOME"/bundle
set -x BUNDLE_USER_CACHE "$XDG_CACHE_HOME"/bundle
set -x BUNDLE_USER_PLUGIN "$XDG_DATA_HOME"/bundle

if status is-interactive
  if command -v rbenv &> /dev/null
    rbenv init - fish | source
  end
end
