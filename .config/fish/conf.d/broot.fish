if command -v broot &> /dev/null && ! test -e "$XDG_CONFIG_HOME/fish/functions/br.fish"
    broot --install
    if ! test -e "$XDG_CONFIG_HOME/broot/conf.hjson"
        mkdir -p "$XDG_CONFIG_HOME/broot"
        broot --write-default-conf "$XDG_CONFIG_HOME/broot"

    end
end
