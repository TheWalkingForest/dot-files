# Ocaml
set -x OPAMROOT "$XDG_DATA_HOME/opam"

test -r "$OPAMROOT/opam-init/init.fish" && source "$OPAMROOT/opam-init/init.fish" > /dev/null 2> /dev/null; or true
