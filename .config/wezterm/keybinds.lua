local wezterm = require('wezterm')
-- local a = wezterm.action

local M = {}

M.disable_default_key_bindings = true

-- M.leader = { mods = 'CTRL', key = 'Space', timeout_milliseconds = 1000 }
--
-- M.keys = {
--     { mods = 'LEADER',       key = '1', action = a.ActivateTab(0) },
--     { mods = 'LEADER',       key = '2', action = a.ActivateTab(1) },
--     { mods = 'LEADER',       key = '3', action = a.ActivateTab(2) },
--     { mods = 'LEADER',       key = '4', action = a.ActivateTab(3) },
--     { mods = 'LEADER',       key = '5', action = a.ActivateTab(4) },
--     { mods = 'LEADER',       key = '6', action = a.ActivateTab(5) },
--     { mods = 'LEADER',       key = '7', action = a.ActivateTab(6) },
--     { mods = 'LEADER',       key = '8', action = a.ActivateTab(7) },
--     { mods = 'LEADER',       key = '9', action = a.ActivateTab(8) },
--     { mods = 'LEADER',       key = '0', action = a.ActivateTab(9) },
--     -- { mods = 'LEADER',       key = 'd', action = a.DetachDomain('CurrentPaneDomain') },
--     { mods = 'LEADER|SHIFT', key = '"', action = a.SplitVertical({ domain = 'CurrentPaneDomain' }), },
--     { mods = 'LEADER|SHIFT', key = '%', action = a.SplitHorizontal({ domain = 'CurrentPaneDomain' }) },
--     { mods = 'LEADER',       key = 'z', action = a.TogglePaneZoomState },
--     { mods = 'LEADER',       key = 'x', action = a.CloseCurrentPane({ confirm = true }) },
--     { mods = 'LEADER',       key = 'p', action = a.ActivateCommandPalette, },
--     { mods = 'ALT',          key = 'h', action = a.ActivatePaneDirection('Left'), },
--     { mods = 'ALT',          key = 'l', action = a.ActivatePaneDirection('Right'), },
--     { mods = 'ALT',          key = 'k', action = a.ActivatePaneDirection('Up'), },
--     { mods = 'ALT',          key = 'j', action = a.ActivatePaneDirection('Down'), },
--     { mods = 'CTRL',         key = '=', action = a.IncreaseFontSize, },
--     { mods = 'CTRL',         key = '-', action = a.DecreaseFontSize, },
--     { mods = 'CTRL|SHIFT',   key = 'v', action = a.PasteFrom('Clipboard'), },
--     { mods = 'LEADER',       key = 'c', action = a.SpawnTab('CurrentPaneDomain'), },
--     { mods = 'LEADER|SHIFT', key = 'f', action = a.ShowTabNavigator, },
--     { mods = 'LEADER|CTRL',  key = 'f', action = a.ShowLauncherArgs({ flags = 'FUZZY|WORKSPACES' }) },
--     {
--         mods = 'LEADER|SHIFT',
--         key = '$',
--         action = a.PromptInputLine {
--             description = 'Enter new name for tab',
--             action = wezterm.action_callback(function(window, pane, line)
--                 if line then
--                     window:active_tab():set_title(line)
--                 end
--             end),
--         }
--     },
--     {
--         mods = 'LEADER',
--         key = 'b',
--         action = wezterm.action_callback(function(window, pane, line)
--             wezterm.set_workspace('hello')
--         end),
--     },
--     -- { key = 'L',             mods = 'CTRL', action = wezterm.action.ShowDebugOverlay, },
-- }

return M
