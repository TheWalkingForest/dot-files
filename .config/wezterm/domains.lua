local wezterm = require('wezterm')

local M = {}

M.uniz_domains = {
    {
        name = 'unix',
    },
}

return M
