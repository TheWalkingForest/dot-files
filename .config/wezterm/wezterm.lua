local wezterm = require('wezterm')
local theme = require('theme')
local keybinds = require('keybinds')
local font = require('font.comicshanns')
local domains = require('domains')

local config = {}

if wezterm.config_builder then
    config = wezterm.config_builder()
end

config.color_scheme = 'Catppuccin Mocha'

config.font_size = font.font_size or 12.0
config.freetype_load_target = "Normal"
config.font = font.font
config.font_rules = font.font_rules
config.warn_about_missing_glyphs = false
config.hide_tab_bar_if_only_one_tab = true

config.use_fancy_tab_bar = false

config.tab_bar_at_bottom = true

config.colors = {
    tab_bar = theme.tab_bar
}

config.term = 'wezterm'

config.disable_default_key_bindings = keybinds.disable_default_key_bindings
config.leader = keybinds.leader
config.keys = keybinds.keys
config.show_new_tab_button_in_tab_bar = false
-- config.hide_tab_bar_if_only_one_tab = true

config.unix_domains = domains.unix_domains

config.audible_bell = "Disabled"

return config
