local wezterm = require('wezterm')
M = {}

M.tab_bar = {
    background = '#1E1E2E',
    active_tab = {
        bg_color = '#3A3A3A',
        fg_color = '#C0C0C0',
    },
    inactive_tab = {
        bg_color = '#313244',
        fg_color = '#808080',
    },
    inactive_tab_hover = {
        bg_color = '#313244',
        fg_color = '#808080',
    },
    -- new_tab = {
    --     bg_color = '#1E1E2E',
    --     fg_color = '#1E1E2E',
    -- },
    -- new_tab_hover = {
    --     bg_color = '#1E1E2E',
    --     fg_color = '#1E1E2E',
    -- },
}

M.status_right = function(window, pane)
    local active_workspace = window:active_workspace()
    local colors = {
        tab_background = '#11111B',
        background = '#313244',
        foreground = '#',
        green = '#A6E3A1',
    }

    local elements = {
        { Foreground = { Color = colors.green } },
        { Background = { Color = colors.tab_background } },
        { Text = '' },
        { Foreground = { Color = colors.background } },
        { Background = { Color = colors.green } },
        { Text = ' ' },
        { Foreground = { Color = colors.foreground } },
        { Background = { Color = colors.background } },
        { Text = ' ' .. active_workspace .. ' ' },
    }

    window:set_right_status(wezterm.format(elements))
end

-- M.command_palette_bg_color = '#'
-- M.command_palette_fg_color = '#'
-- M.command_palette_font_size = '#'

wezterm.on('update-right-status', M.status_right)

return M
