local wezterm = require('wezterm')
M = {}

M.harfbuzz_features = { 'ss02', 'ss03', 'ss05', 'ss09', 'cv30', 'cv28', 'zero', 'onum', }
M.font = wezterm.font({
    family = 'FiraCode Nerd Font',
    harfbuzz_features = M.harfbuzz_features
})

M.font_rules = {
    {
        intensity = 'Bold',
        italic = false,
        font = wezterm.font({
            family = 'FiraCode Nerd Font',
            weight = 'Bold',
            harfbuzz_features = M.harfbuzz_features
        })
    },
    {
        intensity = 'Half',
        italic = false,
        font = wezterm.font({
            family = 'FiraCode Nerd Font',
            weight = 'Light',
            harfbuzz_features = M.harfbuzz_features
        })
    },
}

return M
