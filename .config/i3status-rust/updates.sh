#!/usr/bin/env bash

updates_xbps=$(xbps-install -Mun 2> /dev/null | wc -l)
updates_nix=$(nix-env -q --installed --compare-versions | grep -c "<")

flatpak=$(flatpak update | wc -l)

if [ $((flatpak - 3)) -eq 0 ]; then
    updates_flatpak=0
else
    updates_flatpak=$(echo "$flatpak" | tail -n +4 | head -n -2 | wc -l)
fi
updates=$((updates_xbps + updates_nix + updates_flatpak))

if [ -n "$updates" ] && [ "$updates" -ge 0 ]; then
    printf '{"text": "  %s"}\n' \
           "$updates"
else
    printf '{"text": "Unable to get updates", "state": "Warning" }\n'
fi
