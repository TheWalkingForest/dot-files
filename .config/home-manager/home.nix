{ config, pkgs, ... }:

{
  targets.genericLinux.enable = true;

  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "fwalk";
  home.homeDirectory = "/home/fwalk";
  home.stateVersion = "23.11"; # Please read the comment before changing.

  home.file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    # ".screenrc".source = dotfiles/screenrc;

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';
  };

  home.sessionVariables = {
    EDITOR = "nvim";
  };

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = [
    #pkgs.amfora
    #pkgs.bun
    #pkgs.deno
    #pkgs.dmd
    #pkgs.elixir
    #pkgs.elvish
    #pkgs.erlang
    #pkgs.gleam
    #pkgs.hypridle
    #pkgs.irssi
    #pkgs.lfe
    #pkgs.luajitPackages.fennel
    #pkgs.rebar3
    #pkgs.ruff
    #pkgs.stack
    #pkgs.swift
    #pkgs.uv
    #pkgs.zellij
    #pkgs.zlib
    #pkgs.zola
    pkgs.cmatrix
    pkgs.delve
    pkgs.entr
    pkgs.eza
    pkgs.fd
    pkgs.glow
    pkgs.go
    pkgs.htop
    pkgs.jdk
    pkgs.jre
    pkgs.julia
    pkgs.just
    pkgs.moar
    pkgs.mpc-cli
    pkgs.ncdu
    pkgs.nixd
    pkgs.nodejs
    pkgs.onefetch
    pkgs.pandoc
    pkgs.racket
    pkgs.ranger
    pkgs.ripgrep
    pkgs.ruby_3_3
    pkgs.tmux
    pkgs.tree-sitter
    pkgs.zoxide
  ];

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  #programs.bat = {
  #  enable = true;
  #  config = {
  #      theme = "base16";
  #      italic-text = "always";
  #      color = "always";
  #      decorations = "always";
  #      map-syntax = ".ignore:Git Ignore";
  #  };
  #};

  #programs.fastfetch = {
  #  enable = true;
  #  settings = {
  #    general= {
  #        thread= true;
  #    };
  #    modules= [
  #        "title"
  #        "separator"
  #        "cpu"
  #        "gpu"
  #        "disk"
  #        "memory"
  #        "swap"
  #        "os"
  #        "kernel"
  #        "wm"
  #        "shell"
  #        "packages"
  #        "uptime"
  #        "break"
  #        {
  #            type= "colors";
  #            symbol= "circle";
  #            paddingLeft= 4;
  #        }
  #        "break"
  #        "break"
  #        "break"
  #        "break"
  #    ];
  #  };
  #};

  #programs.fzf = {
  #  enable = true;
  #  colors = {
  #    "bg+" = "#313244";
  #    bg = "#1e1e2e";
  #    fg = "#cdd6f4,";
  #    "fg+" = "#cdd6f4,";
  #    hl = "#f38ba8";
  #    "hl+" = "#f38ba8";
  #    spinner = "#f5e0dc";
  #    header = "#f38ba8";
  #    info = "#cba6f7";
  #    pointer = "#f5e0dc";
  #    marker = "#f5e0dc";
  #    prompt = "#cba6f7";
  #  };
  #};
}
