export XDG_DATA_HOME := x'${XDG_DATA_HOME:-"$HOME/.local/share"}'
export XDG_CONFIG_HOME := x'${XDG_CONFIG_HOME:-"$HOME/.config"}'

[private]
default:
    @echo "XDG_DATA_HOME: " {{XDG_DATA_HOME}}
    @echo "XDG_CONFIG_HOME: " {{XDG_CONFIG_HOME}}
    @echo {{os()}}
    @just --list

[private]
msg *args:
    @printf "=> %s\n" "{{args}}"

setup: rust-install tmp-install configs

rust-install: (msg "Installing Rust")
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

tmp-install: (msg "Installing Tmux Package Manager")
    mkdir -p {{XDG_DATA_HOME}}/tmux/plugins
    git clone --depth=1 https://github.com/tmux-plugins/tpm {{XDG_DATA_HOME}}/tmux/plugins/tpm

home-manager-install: (msg "Installing home-manager")
    nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
    nix-channel --update
    nix-shell '<home-manager>' -A install

configs:
    @just config/install
