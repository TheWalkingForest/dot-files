#!/usr/bin/env bash
#
#  ██████╗  █████╗ ███████╗██╗  ██╗██████╗  ██████╗
#  ██╔══██╗██╔══██╗██╔════╝██║  ██║██╔══██╗██╔════╝
#  ██████╔╝███████║███████╗███████║██████╔╝██║
#  ██╔══██╗██╔══██║╚════██║██╔══██║██╔══██╗██║
#  ██████╔╝██║  ██║███████║██║  ██║██║  ██║╚██████╗
#  ╚═════╝ ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝
#


# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

export XDG_CONFIG_HOME="${XDG_CONFIG_HOME:$HOME/.config}"

### Configs
for file in "${XDG_CONFIG_HOME}/bash/conf.d"/*; do
    echo "$file"
    source "$file"
done

for file in "${XDG_CONFIG_HOME}/bash/functions"/*; do
    source "$file"
done

if [ -f "${XDG_CONFIG_HOME}/bash/prompt.sh" ]; then
    source "${XDG_CONFIG_HOME}/bash/prompt.sh"
fi

### ALIASES ###
if [ -f "${XDG_CONFIG_HOME}/bash/aliases.sh" ]; then
    source "${XDG_CONFIG_HOME}/bash/aliases.sh"
fi
