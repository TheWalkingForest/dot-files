* Dot Files

  Repostitory for my dotfiles

** Requirements

   Ensure git and GNU stow is installed

   ~xbps-install -S stow git~

** Installation

   Clone this repostitory

   #+BEGIN_SRC bash
   git clone https://gitlab.com/TheWalkingForest/dot-files.git
   cd dot-files
   #+END_SRC

   Use GNU stow to setup symlinks

   #+BEGIN_SRC bash
   stow .
   #+END_SRC
