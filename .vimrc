map <Space> <Leader>

set modelines=0

" Line Numbering
set number
set relativenumber

" Line settings
set wrap
set scrolloff=5

" Search
set ignorecase
set smartcase

" Tab configs
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set smarttab
set autoindent

set cursorline
"set cursorcolumn
set colorcolumn=80,100

set encoding=utf-8

set scrolloff=5

set showcmd


syntax enable
filetype plugin indent on

:let g:netrw_dirhistmax = 0
